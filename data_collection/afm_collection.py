import numpy as np
import matplotlib.pyplot as plt
import h5py
import glob
import os
import json
import pandas as pd
import seaborn as sb
from tqdm import tqdm
from PIL import Image
import xmltodict


def get_files(path,type='hdf5',show=False):
    '''
    :param path: path
    :param type: file ending
    :param show: show the list
    :return: list of file names
    :return: dictionary so that user use it to find single files
    '''
    fnames = glob.glob(path+'/*.'+type)
    fnames.sort()
    keys_fnames = {}
    for i in range(len(fnames)):
        keys_fnames[os.path.basename(fnames[i])] = i
        if show:
            print(os.path.basename(fnames[i]))
    return fnames, keys_fnames

def get_measurement_parameter_jb(path):
    '''
    used to extract measurement parameters like cells, measurements, and data types
    ------------------------------
    fnames : txt file list
    fname : parameter file names
    parm : parameter dictionary
    '''
    fnames, _ = get_files(path,'txt')
    fname = [fname for fname in fnames if 'measurement' in os.path.basename(fname)]
    with open(fname[0]) as json_file:
        parm = json.load(json_file)
    return parm

# get the time codes from the file names and translate into seconds
def get_times(fnames):
    '''
    takes files and extracts the time data in seconds
    time : array
    time_code : string sorted from the file names
    '''
    time = np.full([len(fnames),4],np.nan)  # first axis fnames, then // h / min / s / ms
    for i in range(len(fnames)):
        time_code = os.path.basename(fnames[i])[22:-25]
        time[i,0] = float(time_code[:2])
        time[i,1] = float(time_code[3:5])
        time[i,2] = float(time_code[6:8])
        time[i,3] = float(time_code[8:])
    time = time[:,0]*60*60 + time[:,1]*60 + time[:,2] + time[:,3]  # time in seconds
    return time

def cell_code(parm):
    '''
    makes a code in order to recognize the same cells from the segemation program
    '''
    cells = parm['cells']
    image_path = parm['images_path']
    if 'm2' in parm['measurement_info']:
        meas_name = os.path.dirname(image_path)[-8:] + 'd' + os.path.basename(image_path)[-3]
        meas_name = meas_name + 'm2'
    else:
        meas_name = os.path.dirname(image_path)[-8:] + 'd' + os.path.basename(image_path)[-1]
    return [meas_name + cell for cell in cells.keys()] * len(parm['measurement_type'])


## cellpose part ###########################
def get_img_path(path,parm):
    img_path =os.path.join(os.path.dirname(os.path.dirname(path)),parm['images_path'][2:])
    return img_path

def get_canti_positions(path):
    '''
    extract the cantilever positions that where set by image set
    '''
    fnames, _ = get_files(path,'txt')
    for fname in fnames:
        if 'canti_locations' in fname:
            with open(fname) as json_file:
                canti_locs = json.load(json_file)
    return canti_locs


def get_canti_inames(canti_loc):
    '''
    provides a list of canti image names sorted stage wise
    '''
    d = {}
    for stage in canti_loc:
        _d = []
        for cell in canti_loc[stage]:
            l = list(canti_loc[stage][cell].keys())
            if len(l) > 0:
                _d.append(l[0])
            else:
                continue
        d[stage] = _d
    return d

def get_image_time(path,cname):
    '''
    path : path to image folder
    cname : name of the image
    '''
    with Image.open(os.path.join(path,cname)) as _img:
        img_datetime = xmltodict.parse(_img.tag[270][0])['OME']['OME:Image']['OME:AcquisitionDate']
    time_str = img_datetime[img_datetime.find('T')+1:-1]
    hours = int(time_str[:2])
    mins = int(time_str[3:5])
    seconds = int(time_str[6:])

    img_time = hours * 60**2 + mins * 60 + seconds # time in seconds
    return img_time

def get_canti_times(canti_loc,img_path):
    '''
    gets the time of the first images in the each stage for each cell,
    time coresponds to the first afm curve usually taken max 5 seconds after
    '''
    d = {}
    for stage in canti_loc:
        _d =[]
        for cell in canti_loc[stage]:
            inames = list(canti_loc[stage][cell].keys())
            if len(inames) > 0:
                iname = inames[0]
                time = get_image_time(img_path,iname)
                _d.append(time)
            else:
                continue
        _d.sort()
        d[stage] = _d
    return d

################################################
def _error_propogation(errors1,errors2):
    '''
    simplified error propogation for the
    '''
    propogated_error = np.sqrt(errors1**2+errors2**2)
    return propogated_error

def error_progogation(cell_data_ad):
    prog_error = np.copy(cell_data_ad)
    for index in range(np.size(cell_data_ad,0)):
        prog_error[index,:] = _error_propogation(cell_data_ad[0,:],cell_data_ad[index,:])
    return prog_error

def _per_error_propogation(x,y,error1,error2):
    prop_error = np.sqrt(error1**2 / y**2 + x**2 * error2**2 / y**4) * 100
    return prop_error


def per_error_propogation(cell_data_ad):
    prog_error = np.copy(cell_data_ad)
    for index in range(np.size(prog_error,0)):
        prog_error = _per_error_propogation(cell_data[:,:5],cell_data[0,:5],cell_data[:,:5],cell_data[0,:5])


def calculate_percent_difference_data(mean_data,cells):
    per_data = np.full([np.size(mean_data,0) - len(cells),np.size(mean_data,1)],np.nan)
    for cindex in range(len(cells)):
        cell_data = mean_data[cindex::len(cells),:]
        _per_data = ((cell_data[:,:5] - cell_data[0,:5]) / cell_data[0,:5]) * 100
        per_data[cindex::len(cells),:5] = _per_data[1:,:]
        crop = mean_data[cindex::len(cells),10]
        per_data[cindex::len(cells),10] = crop[1:]
    return per_data

def calculate_difference_data(mean_data,cells):
    '''
    this calucaletes the differnce between before and after or meansurement 1 and the rest
    from the mean dataset
    '''
    difference_data = np.full([np.size(mean_data,0) - len(cells),np.size(mean_data,1)],np.nan)
    for cindex in range(len(cells)):
        cell_data = mean_data[cindex::len(cells),:]
        _diff_data = cell_data[:,:5] - cell_data[0,:5]
        _error = error_progogation(cell_data[:,5:10])
        difference_data[cindex::len(cells),:5] = _diff_data[1:,:]
        difference_data[cindex::len(cells),5:10] = _error[1:,:]
        crop = mean_data[cindex::len(cells),10]
        difference_data[cindex::len(cells),10] = crop[1:]
    return difference_data

### max force stuff
def read_contact_point(file):
    return file.attrs['contact_point']

def read_baseline_corr(file):
    time = file['time'][:]
    force = file['force'] - np.poly1d(file.attrs['baseline_parameter'])(time)
    return np.array(time), np.array(force)

def get_data(file):
    '''
    get all the required data and the fit range
    from the hdf5 file
    '''
    time, force = read_baseline_corr(file)
    contact_point = read_contact_point(file)
    return time, force, contact_point

def get_cell_dataset(fnames,times,cell,meas_step, cell_num):
    '''
    fnames : list of file names of the current measurement
    times : list of cell times in seconds, first curve is at t=0
    cell : index on the cell c2 = 3 for example
    meas_step : index of the measurement step, after is often 18 for example
    cell_num : global count of the cells
    ---------------------------------------
    returns : dataset of that cell (3 curves) (tension, ka, beta, zabs, time)
    & the the mean aswell as std of the cell
    '''
    data = np.full([3,8],np.nan)
    # only choose curves of the cell
    cell_fnames = fnames[meas_step + cell : meas_step + cell+3]
    # as well as only the times for the cell
    cell_times = times[meas_step + cell : meas_step + cell + 3]
    for index, fname in enumerate(cell_fnames):
        # get single curve data
        file = h5py.File(fname,'r')
        if 'trace_ok' in file.attrs:
            if file.attrs['trace_ok'] == 'no':
                fit_parameter = np.array([-999,-999,-999])
                zabs = np.nan
                max_force = np.nan
                max_fit_force = np.nan
            else:
                fit_parameter = file['fit_parameter'][:]
                zabs = file.attrs['fit_zabs']
                time, force, cp = get_data(file)
                fit_force = np.array(file['fit_force'])
                max_force = np.max(force)
                max_fit_force = np.max(fit_force)
        else:
            fit_parameter = file['fit_parameter'][:]
            zabs = file.attrs['fit_zabs']
            time, force, cp = get_data(file)
            fit_force = np.array(file['fit_force'])
            max_force = np.max(force)
            max_fit_force = np.max(fit_force)
        file.close()
        # sort out bad fits
        if (fit_parameter[0]<0) or (fit_parameter[1]<0) or (fit_parameter[2]>=0) or (fit_parameter[2]<(-0.80)):# or (zabs >= 3.31)): # zabs works?
            data[index,:] = np.nan
        else:
            data[index,:3] = fit_parameter
            data[index,3] = zabs
            data[index,4] = cell_times[index]
            data[index,5] = max_force
            data[index,6] = max_fit_force
    data[:,7] = cell_num
    data[:,0] = data[:,0] * 1000 # now in mN
    data[:,2] = data[:,2] * (-1) # beta now positive

    return data, np.nanmean(data[:,:7],axis=0), np.nanstd(data[:,:7],axis=0)

def get_measurement_step_dataset(fnames,times,cells,meas_step,cell_number,meas_code):
    '''
    the following function will provide an array of the single measurement
    fnames : list of file names
    times : list of cell times
    cells : dictionary cell names and cell indexs, provided by parm
    meas_setp : index for the measurement step
    cell_number : global count of all cells from all measurements
    ---------------------------------------------------------
    returns : data array & mean data with std
    (tension, Ka, beta, zabs, time,cell_number, cell counter)
    cell counter  = measurement based cell number
    cell number = global cell count
    '''
    data = np.full([len(cells)*3,9],np.nan)
    mean_data = np.full([len(cells),16],np.nan)
    for index, cell in enumerate(cells):
        cell_num = cell_number + index
        cell_index = cells[cell]
        if (meas_code =='20210205d1after_2') & (index >= 4):
            cell_index = cell_index - 3
            if cell == 'c5':
                print(index)
                _d = np.nan
                _m = np.nan
                _s = np.nan
            else:
                _d, _m, _s = get_cell_dataset(fnames,times,cell_index,meas_step,cell_num)
        else:
            _d, _m, _s = get_cell_dataset(fnames,times,cell_index,meas_step,cell_num)
        data[index*3:index*3+3,:8] = _d
        data[index*3:index*3+3,8] = index
        mean_data[index,:7] = _m
        mean_data[index,7:14] = _s
        mean_data[index,14] = cell_num
        mean_data[index,15] = index
    return data, mean_data

def get_measurement_dataset(path,cell_number):
    '''
    path : path to the files and the parm file
    cell_number : global cell counter
    ----------------------------------
    return
    data : complete dataset with cell counter / cell number
    data_mean : mean & std dataset, with cell counter
    cell_number : new cell number
    all_info: contains the lists below
    info : list of measurement steps for the pandas
    mean_info : list of measurements for the mean dataset
    meas_info : list of measurement type example: 'stretch control' or 'control control'
    mean_meas_info : same as meas_info just shorter list
    '''
    parm = get_measurement_parameter_jb(path)
    fnames, _ = get_files(path)
    times = get_times(fnames)
    cells = parm['cells']
    _i = len(cells) * 3
    _l = _i * len(parm['measurement_type'])
    data = np.full([_l,10],np.nan)
    mean_data = np.full([_l//3,17],np.nan)
    measurement_step = []
    first_count = True
    # time shift stuff
    img_path = get_img_path(path, parm)
    canti_loc = get_canti_positions(img_path)
    canti_inames = get_canti_inames(canti_loc)
    canti_times = get_canti_times(canti_loc,img_path)
    time_shift = np.full([len(cells)*len(canti_times.keys())],np.nan)
    # cell code for cell area connection
    mean_cell_codes = cell_code(parm)
    print(mean_cell_codes[0][:-2])
    diff_cell_codes = mean_cell_codes[len(cells):]

    for index, meas_step in enumerate(parm['measurement_type']):
        measurement_step.append(meas_step)
        if 'during' in meas_step:
            path_during = os.path.join(path,os.path.basename(parm['path_during']))
            fnames_during, _ = get_files(path_during)
            times_during = get_times(fnames_during)
            _data, _mean_data = get_measurement_step_dataset(fnames_during,
                                                             times_during,
                                                             cells,parm['measurement_type'][meas_step],
                                                             cell_number,
                                                             mean_cell_codes[0][:-2]+meas_step)
            time_shift[len(cells):len(cells)*2] = times_during[::3][:len(cells)] - canti_times['during']
            # get a reference time
            if first_count:
                ref_time = times_during[0]
                ref_time_during = _mean_data[0,[4,9]]
                first_count=False
        else:
            _data, _mean_data= get_measurement_step_dataset(fnames,
                                                            times,
                                                            cells,
                                                            parm['measurement_type'][meas_step],
                                                            cell_number,
                                                            mean_cell_codes[0][:-2])
            if (meas_step == 'before') or (meas_step == 'after'):
                stage = meas_step
                _start = parm['measurement_type'][stage]
                if stage == 'after':
                    _s = _start + len(cells)*3
                    # for that one case..
                    if 'time' in parm['measurement_info']:
                        _s = len(cells)*2*3
                else:
                    _s = _start
                _times = times[_start:_start+len(cells)*3]
                time_shift[_s//3:_s//3+len(cells)] = _times[::3] - canti_times[stage]
            elif meas_step == 'measurement_1':
                time_shift = times[:len(cells)*3][::3] - canti_times['measurement']

        data[index*_i:index*_i+_i,:9] = _data
        mean_data[index*len(cells):index*len(cells)+len(cells),:16] = _mean_data
    # calculate time shift between img and afm
    time_correction = np.nanmedian(time_shift)
    # calculate difference data
    diff_data = calculate_difference_data(mean_data,cells)
    per_data = calculate_percent_difference_data(mean_data,cells)
    # add temperature info
    if 'measurement_temp' in parm:
        temperature = parm['measurement_temp']
    else:
        temperature = 37
    temperature = str(temperature)
    data[:,9] = temperature
    mean_data[:,16] = temperature
    diff_data[:,16] = temperature
    per_data[:,16] = temperature
    # cell number
    cell_number = cell_number + len(cells)
    # lists for pandas
    info = []
    mean_info = info
    meas_info = info
    mean_meas_info = info
    for l in list(parm['measurement_type']):
        info = info + [l]*_i
        mean_info = mean_info + [l]*len(cells)
    # check for parameter not all files have them
    if 'measurement_info' in parm:
        measurement_info = parm['measurement_info']
        # check which strain in mm is begin used
        if 'strain' in measurement_info:
            # set measurement type
            if 'LatA' in measurement_info:
                m_type = 'stretch LatA'
            else:
                m_type = 'stretch control'
            if '5mm' in measurement_info:
                if '15mm' in measurement_info:
                    strain = 15
                else:
                    strain = 5
            elif '10mm' in measurement_info:
                strain = 10
        elif 'control' in parm['measurement_info']:
            if 'LatA' in measurement_info:
                m_type = 'control LatA'
            else:
                m_type = 'control control'
            strain = 0
    else:
        # these are all the same
        m_type = 'stretch control'
        strain = 10
    strain = str(strain)
    meas_info = _i*len(parm['measurement_type']) * [m_type]
    if 'who' in parm:
        who_p = parm['who']
    else:
        who_p = 'jonathan'
    who_info =  _i*len(parm['measurement_type']) * [who_p]
    mean_meas_info = len(cells) * len(parm['measurement_type']) * [m_type]
    mean_who_info = len(cells) * len(parm['measurement_type']) * [who_p]
    diff_info = mean_info[len(cells):]
    diff_meas_info = mean_meas_info[len(cells):]
    diff_who_info = mean_who_info[len(cells):]
    # set time to zero at during stage start
    lb_time =  parm['time_since_strain_lb'] * 60
    if not(first_count):
        data[:,4] = data[:,4] - ref_time + lb_time
        mean_data[:,4] = mean_data[:,4] - ref_time_during[0] + ref_time_during[1] + lb_time
    per_data[:,4] = per_data[:,4] - per_data[0,4]
    # fill list of list for the names for pandas
    all_info = [info, meas_info, who_info,
                mean_info, mean_meas_info, mean_who_info,
                diff_info, diff_meas_info, diff_who_info,
                mean_cell_codes,diff_cell_codes]
    return data, mean_data, diff_data, per_data, cell_number, all_info, strain, [mean_cell_codes[0][:-2],time_correction]

def make_single_df(p_index,path,cell_number):
    '''
    calls get_measurement_dataset
    p_index : index of the current path for a measurement number
    path : path to file and parm file
    cell_number : global cell number for all measurements
    -----------------------------
    return
    df : dataframe of all curves
    mean_df : dataframe of mean data
    cell_number : global cell number counter
    if needed...:
    data : data array of measurement
    mean_data : mean_data array of measurement
    '''
    data, mean_data, diff_data, per_data, cell_number, all_info, strain, time_correction = get_measurement_dataset(path,cell_number)
    info = all_info[0]
    meas_info = all_info[1]
    who_info = all_info[2]
    mean_info = all_info[3]
    mean_meas_info = all_info[4]
    mean_who_info = all_info[5]
    diff_info = all_info[6]
    diff_meas_info = all_info[7]
    diff_who_info = all_info[8]
    mean_cell_codes = all_info[9]
    diff_cell_codes = all_info[10]
    df = pd.DataFrame({'measurement_number' : f'measurement_{p_index}',
                       'tension' : data[:,0],
                       'Ka' : data[:,1],
                       'beta' : data[:,2],
                       'zabs' : data[:,3],
                       'time' : data[:,4],
                       'max_force' : data[:,5],
                       'max_fit_force' : data[:,6],
                       'cell_number' : data[:,7],
                       'cell_counter' : data[:,8],
                       'temperature' : data[:,9],
                       'measurement_step' : info,
                       'measurement_type' : meas_info,
                       'person' : who_info,
                       'strain' : strain})
    mean_df = pd.DataFrame({'measurement_number' : f'measurement_{p_index}',
                            'tension' : mean_data[:,0],
                            'Ka' : mean_data[:,1],
                            'beta' : mean_data[:,2],
                            'zabs' : mean_data[:,3],
                            'time' : mean_data[:,4],
                            'max_force' : mean_data[:,5],
                            'max_fit_force' : mean_data[:,6],
                            'err tension' : mean_data[:,7],
                            'err Ka' : mean_data[:,8],
                            'err beta' : mean_data[:,9],
                            'err zabs' : mean_data[:,10],
                            'err time' : mean_data[:,11],
                            'err max_force' : mean_data[:,12],
                            'err max_fit_force' : mean_data[:,13],
                            'cell_number' : mean_data[:,14],
                            'cell_counter' : mean_data[:,15],
                            'temperature' : mean_data[:,16],
                            'measurement_step' : mean_info,
                            'measurement_type' : mean_meas_info,
                            'person' : mean_who_info,
                            'strain' : strain,
                            'cell_code' : mean_cell_codes})

    diff_df = pd.DataFrame({'measurement_number' : f'measurement_{p_index}',
                            'tension' : diff_data[:,0],
                            'Ka' : diff_data[:,1],
                            'beta' : diff_data[:,2],
                            'zabs' : diff_data[:,3],
                            'time' : diff_data[:,4],
                            'max_force' : diff_data[:,5],
                            'max_fit_force' : diff_data[:,6],
                            'err tension' : diff_data[:,7],
                            'err Ka' : diff_data[:,8],
                            'err beta' : diff_data[:,9],
                            'err zabs' : diff_data[:,10],
                            'err time' : diff_data[:,11],
                            'max_force' : diff_data[:,12],
                            'max_fit_force' : diff_data[:,13],
                            'cell_number' : diff_data[:,14],
                            'cell_counter' : diff_data[:,15],
                            'temperature' : diff_data[:,16],
                            'measurement_step' : diff_info,
                            'measurement_type' : diff_meas_info,
                            'person' : diff_who_info,
                            'strain' : strain,
                            'cell_code' : diff_cell_codes})

    per_df = pd.DataFrame({'measurement_number' : f'measurement_{p_index}',
                            'tension' : per_data[:,0],
                            'Ka' : per_data[:,1],
                            'beta' : per_data[:,2],
                            'zabs' : per_data[:,3],
                            'time' : per_data[:,4],
                           'max_force' : per_data[:,5],
                            'max_fit_force' : per_data[:,6],
                            'err tension' : per_data[:,7],
                            'err Ka' : per_data[:,8],
                            'err beta' : per_data[:,9],
                            'err zabs' : per_data[:,10],
                            'err time' : per_data[:,11],
                           'max_force' : per_data[:,12],
                           'max_fit_force' : per_data[:,13],
                            'cell_number' : per_data[:,14],
                           'cell_counter' : per_data[:,15],
                            'temperature' : per_data[:,16],
                            'measurement_step' : diff_info,
                            'measurement_type' : diff_meas_info,
                            'person' : diff_who_info,
                            'strain' : strain,
                           'cell_code' : diff_cell_codes})

    return df, mean_df, diff_df, per_df, cell_number, data, mean_data, diff_data, time_correction


def make_complete_df(paths):
    cell_number = 0
    dfs = []
    mean_dfs = []
    diff_dfs = []
    per_dfs = []
    datas = []
    mean_datas = []
    diff_datas = []
    time_corrections = []
    for index,path in enumerate(tqdm(paths)):
        _df, _mean_df, _diff_df, _per_df, cell_number, _data, _mean_data, _diff_data, time_correction = make_single_df(index,path,cell_number)

        dfs.append(_df)
        mean_dfs.append(_mean_df)
        diff_dfs.append(_diff_df)
        per_dfs.append(_per_df)
        datas.append(_data)
        mean_datas.append(_mean_data)
        diff_datas.append(_diff_df)
        time_corrections.append(time_correction)
    return pd.concat(dfs), pd.concat(mean_dfs), pd.concat(diff_dfs), pd.concat(per_dfs), time_corrections


def run_it(paths):
    df, mean_df, diff_df, per_df, time_correction = make_complete_df(paths)
    df.to_csv('data.csv', index=False)
    mean_df.to_csv('mean_data.csv', index=False)
    diff_df.to_csv('diff_data.csv', index=False)
    per_df.to_csv('per_data.csv', index=False)
    # save time shift / time correcion
    time_correction = dict(time_correction)
    with open('./time_correction.txt','w+') as outfile:
        json.dump(time_correction,outfile)


#========== run part===============================

paths = []
#paths.append('../../../../')
#paths.append('../../../../strain_measurements/191004/device1/eval_2019_10_06_16_45')
paths.append('../../../../strain_measurements/191009/device1/eval_2019_11_06_17_12')
paths.append('../../../../strain_measurements/191010/device1/eval_2019_11_06_17_45')
paths.append('../../../../strain_measurements/191010/device2/eval_2019_11_06_17_55')
paths.append('../../../../strain_measurements/191016/device2/eval_2019_11_05_10_28')
paths.append('../../../../strain_measurements/191018/device1/eval_2019_11_06_12_19')
"""
# data from Karim
paths.append('../../../../strain_measurements/karim/BA_data/200509/KA032/eval_2020_06_24_11_29')
#paths.append('../../../../strain_measurements/karim/BA_data/200510/KA033/eval_2020_06_24_11_26')
paths.append('../../../../strain_measurements/karim/BA_data/200510/KA034/eval_2020_06_24_11_19')
#paths.append('../../../../strain_measurements/karim/BA_data/200610/KA037/eval_2020_06_24_11_03')
paths.append('../../../../strain_measurements/karim/BA_data/200610/KA038/eval_2020_06_24_10_56')
paths.append('../../../../strain_measurements/karim/BA_data/200611/KA039/eval_2020_06_24_10_47')
paths.append('../../../../strain_measurements/karim/BA_data/200611/KA040/eval_2020_06_24_10_35')
paths.append('../../../../strain_measurements/karim/BA_data/200612/KA041/eval_2020_06_24_10_28')
paths.append('../../../../strain_measurements/karim/BA_data/200612/KA042/eval_2020_06_24_10_22')
#paths.append('../../../../strain_measurements/karim/BA_data/200613/KA043/eval_2020_06_24_10_17')
paths.append('../../../../strain_measurements/karim/BA_data/200613/KA044/eval_2020_06_24_10_10')
paths.append('../../../../strain_measurements/karim/BA_data/200614/KA045/eval_2020_06_24_09_51')
# recorrected
paths.append('../../../../strain_measurements/karim/BA_data/200510/KA033/eval_2020_10_28_12_05')
paths.append('../../../../strain_measurements/karim/BA_data/200610/KA037/eval_2020_10_27_16_17')
paths.append('../../../../strain_measurements/karim/BA_data/200613/KA043/eval_2020_10_27_12_06')
"""
# new measurements only 10mm strain
paths.append('../../../../strain_measurements/200704/device1/eval_2020_07_21_14_41')
paths.append('../../../../strain_measurements/200704/device2/eval_2020_07_21_15_14')
paths.append('../../../../strain_measurements/200704/device3/eval_2020_07_21_15_45')
paths.append('../../../../strain_measurements/200801/device1/eval_2020_08_17_21_23')
paths.append('../../../../strain_measurements/200801/device2/eval_2020_08_17_23_02')
paths.append('../../../../strain_measurements/200802/device1/eval_2020_08_18_00_07')   # some issues here..
#paths.append('../../../../strain_measurements/200802/device2/eval_2020_08_18_00_28')
#paths.append('../../../../strain_measurements')

# measurements 5mm
paths.append('../../../../strain_measurements/200729/device1/eval_2020_08_17_16_31')
paths.append('../../../../strain_measurements/200730/device2/eval_2020_08_17_17_06')

# measurements 15mm
paths.append('../../../../strain_measurements/200731/device1/eval_2020_08_17_17_38')

# new stuff
paths.append('../../../../strain_measurements/210122/device2/eval_2021_01_27_16_49')
paths.append('../../../../strain_measurements/210203/device1/eval_2021_02_03_18_31')
paths.append('../../../../strain_measurements/210204/device1/eval_2021_02_04_16_53')
paths.append('../../../../strain_measurements/210204/device2/eval_2021_02_08_17_20')
paths.append('../../../../strain_measurements/210204/device2m2/eval')
paths.append('../../../../strain_measurements/210205/device1/eval_2021_02_08_17_59')
paths.append('../../../../strain_measurements/210205/device1m2/eval_2021_02_08_18_08')
paths.append('../../../../strain_measurements/210205/device2/eval_2021_02_09_18_20')
paths.append('../../../../strain_measurements/210206/device1/eval_2021_02_09_10_13')
paths.append('../../../../strain_measurements/210206/device1m2/eval_2021_02_09_10_45')
paths.append('../../../../strain_measurements/210206/device2/eval_2021_02_09_11_18')
#paths.append('../../../../strain_measurements/210207/device1/eval_2021_02_09_11_40') --> airbubble
paths.append('../../../../strain_measurements/210207/device2/eval_2021_02_09_11_50')

#kparm = get_measurement_parameter_karim('../../../../strain_measurements/karim/force_curves_jb/all_new')

test_missing_cells_curves = False
if test_missing_cells_curves:
    path = paths[24]
    parm = get_measurement_parameter_jb(path)
    fnames, _ = get_files(path)
    times = get_times(fnames)
    cells = parm['cells']
    _i = len(cells) * 3
    _l = _i * len(parm['measurement_type'])
    data = np.full([_l,8],np.nan)
    mean_data = np.full([_l//3,13],np.nan)
    measurement_step = []
    first_count = True
    # time shift stuff
    img_path = get_img_path(path, parm)
    canti_loc = get_canti_positions(img_path)
    canti_inames = get_canti_inames(canti_loc)
    canti_times = get_canti_times(canti_loc,img_path)
    time_shift = np.full([len(cells)*len(canti_times.keys())],np.nan)
    # cell code for cell area connection
    mean_cell_codes = cell_code(parm)
    print(mean_cell_codes[0][:-2])
    diff_cell_codes = mean_cell_codes[len(cells):]

    meas_steps = list(parm['measurement_type'].keys())
    index = 2
    meas_step = meas_steps[index]
    cell_number = 1
    meas_code = mean_cell_codes[0][:-2] + meas_step
    p = parm['measurement_type'][meas_step]
    # get_measurement_step_stuff
    data = np.full([len(cells)*3,7],np.nan)
    mean_data = np.full([len(cells),12],np.nan)
    index = 0
    cell = list(cells.keys())[index]
    print(cell)
    cell_num = cell_number + index

    # get_cell_dataset stuff
    meas_step = parm['measurement_type'][meas_step]
    cell = cells[cell]



    data = np.full([3,6],np.nan)
    # only choose curves of the cell
    cell_fnames = fnames[meas_step + cell : meas_step + cell+3]
    # as well as only the times for the cell
    cell_times = times[meas_step + cell : meas_step + cell + 3]
    for index, fname in enumerate(cell_fnames):
        # get single curve data
        file = h5py.File(fname,'r')
        if 'trace_ok' in file.attrs:
            if file.attrs['trace_ok'] == 'no':
                fit_parameter = np.array([-999,-999,-999])
                zabs = np.nan
            else:
                fit_parameter = file['fit_parameter'][:]
                zabs = file.attrs['fit_zabs']
        else:
            fit_parameter = file['fit_parameter'][:]
            zabs = file.attrs['fit_zabs']
        file.close()
        # sort out bad fits
        if (fit_parameter[0]<0) or (fit_parameter[1]<0) or (fit_parameter[2]>=0) or (fit_parameter[2]<(-0.90) or (zabs >= 3.31)):
            data[index,:] = np.nan
        else:
            data[index,:3] = fit_parameter
            data[index,3] = zabs
            data[index,4] = cell_times[index]
    data[:,5] = cell_num
    data[:,0] = data[:,0] * 1000 # now in mN
    data[:,2] = data[:,2] * (-1) # beta now positive

test_cellpose_time = False

if test_cellpose_time:
    # normal afm stuff
    path = paths[24]
    parm = get_measurement_parameter_jb(path)
    cells = parm['cells']
    fnames, _ = get_files(path)
    times = get_times(fnames)
    # new stuff to find time shift between image and afm
    img_path = get_img_path(path, parm)
    canti_loc = get_canti_positions(img_path)
    canti_inames = get_canti_inames(canti_loc)
    canti_times = get_canti_times(canti_loc,img_path)
    # during stuff difference between afm and cells
    #dif = times_during[::3][:len(cells)] - canti_times['during']
    # now for everything
    time_shift = np.full([len(cells)*len(canti_times.keys())],np.nan)
    if 'measurement' in list(parm['measurement_type'].keys())[0]:
        time_shift = times[:len(cells)*3][::3] - canti_times['measurement']
    else:
        for stage in canti_times:
            if stage == 'during':
                path_during = os.path.join(path,os.path.basename(parm['path_during']))
                fnames_during, _ = get_files(path_during)
                times_during = get_times(fnames_during)

                time_shift[len(cells):len(cells)*2] = times_during[::3][:len(cells)] - canti_times['during']
            else:
                if stage in parm['measurement_type']:
                    _start = parm['measurement_type'][stage]
                    if stage == 'after':
                        _s = _start + len(cells)*3
                        # for that one case..
                        if 'time' in parm['measurement_info']:
                            _s = len(cells)*2*3
                    else:
                        _s = _start
                    _times = times[_start:_start+len(cells)*3]
                    time_shift[_s//3:_s//3+len(cells)] = _times[::3] - canti_times[stage]
    time_correction = np.nanmedian(time_shift)







test_it = False

if test_it:
    path = paths[1]

    if 'karim' in path:
        parm = get_measurement_parameter_jb(path)
        fnames, _ = get_files(path)
        times = get_times(fnames)

        path_during = os.path.join(path,os.path.basename(parm['path_during']))
        fnames_during, _ = get_files(path_during)
        times_during = get_times(fnames_during)

        fname = fnames[2]
        file = h5py.File(fname,'r')
    else:
        parm = get_measurement_parameter_jb(path)
        cells = parm['cells']
        fnames, _ = get_files(path)
        times = get_times(fnames)

        path_during = os.path.join(path,os.path.basename(parm['path_during']))
        fnames_during, _ = get_files(path_during)
        times_during = get_times(fnames_during)

        fname = fnames[2]
        file = h5py.File(fname,'r')
        file.close()
