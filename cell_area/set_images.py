import numpy as np
import glob
import os
import datetime
import h5py
import sys
import json
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.widgets import RectangleSelector

import skimage.io

def get_files(path,type='force.sdf',show=False):
    '''
    :param path: path
    :param type: file ending
    :param show: show the list
    :return: list of file names
    :return: dictionary so that user use it to find single files
    and key_names
    '''
    fnames = glob.glob(path+'/*.'+type)
    fnames.sort()
    keys_fnames = {}
    for i in range(len(fnames)):
        keys_fnames[os.path.basename(fnames[i])] = i
        if show:
            print(os.path.basename(fnames[i]))
    return fnames, keys_fnames

class image_viewer():
    '''
    used to correct the curves
    fnames : list of file names
    path : path to folder
    segments : which segments should be shown 0,1,2, 3 = all
    '''
    def __init__(self,fnames,img_path,parm):
        self.fnames = fnames
        self.img_path = img_path
        self.parm = parm
        print(parm['measurement_info'])
        self.cell_list = list(parm['cells'].keys())
        self.index = 0
        self.img_index = 0
        self.cell_index = 0
        self.check_parms()
        #self.stage_name = ['before', 'during', 'after']
        self.data = {}
        self.stage_data = {}
        self.cell_data = {}
        # canti stuff
        self.canti_data = {}
        self.canti_stage = {}
        self.canti_cell = {}
        # size of crop
        self.length = 200
        # Setup the figure
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        # print instructions
        instru = {'change image left' : 'a',
                  'change image right' : 'd'}
        print('='*50)
        print('#\tchange image left : a\n#\tchange image right : d')
        print('-'*50)
        print('#\taccept point : w')
        print('#\tremove point : c')
        print('-'*50)
        print('#\tAFM measurement point : v')
        print('-'*50)
        print('#\tchange cell number left : e\n#\tchange cell number right : r')
        print('-'*50)
        print('#\tchange measurement stage left : t\n#\tchange measurement stage right : g')
        print('-'*50)
        print('#\tsave and close : x')
        print('='*50)

        #self.line_hl, = self.ax.plot([], [], 'g-', lw=2)
        #self.line_bl, = self.ax.plot([], [], 'm--', lw=2)
        #self.line_cp, = self.ax.plot([], [], 'mo', lw=4)

        #self.ax.set_xlabel("Time / s", size=16)
        #self.ax.set_ylabel("Force / nN", size=16)
        #self.ax.tick_params("both", labelsize=14)
        #self.ax.grid(True)

    def check_parms(self):
        meas_info = self.parm['measurement_info']
        meas_temp = self.parm['measurement_temp']
        if 'strain' in meas_info:
            self.stage_name = ['before', 'during','after']
        if 'control' in meas_info:
            self.stage_name = ['measurement']

    def save_data(self):
        file_name = os.path.join(self.img_path,'cell_locations.txt')
        file_name2 = os.path.join(self.img_path,'canti_locations.txt')
        with open(file_name,'w+') as outfile:
            json.dump(self.data,outfile)
        with open(file_name2,'w+') as outfile:
            json.dump(self.canti_data,outfile)

    def close_curve_next_curve(self):
        self.ax.relim(visible_only=True)
        self.ax.cla()
        self.draw_image()

    def close_image_next_stage(self):
        self.ax.cla()
        self.prepare_data()
        self.draw_image()

    def onrelease(self, event):
        # reverse the x and y because they are plotted differently with imshow
        self.point_data = (int(event.xdata),int(event.ydata))
        adj_point = [self.point_data[0]-self.length,self.point_data[1]-self.length]
        if len(self.ax.patches) == 0:
            self.rect = patches.Rectangle(adj_point,self.length*2,self.length*2,
                                          linewidth=1,edgecolor='r',facecolor='none')
            self.ax.add_patch(self.rect)
        else:
            self.rect.set_xy(adj_point)
        #print(f'patches: {len(self.ax.patches)}')
        self.point.set_data(self.point_data[0],self.point_data[1])
        plt.draw()

    def delete_point(self):
        if self.iname[self.img_index] in self.cell_data:
            del self.cell_data[self.iname[self.img_index]]
            self.close_curve_next_curve()
            self.draw_point()

        if self.iname[self.img_index] in self.canti_cell:
            del self.canti_cell[self.iname[self.img_index]]
            self.close_curve_next_curve()
            self.draw_point()


    def draw_point(self):
        if self.stage_name[self.index] in self.data:
            self.stage_data = self.data[self.stage_name[self.index]]
            if self.cell_list[self.cell_index] in self.stage_data:
                self.cell_data = self.stage_data[self.cell_list[self.cell_index]]

        if self.stage_name[self.index] in self.canti_data:
            self.canti_stage = self.canti_data[self.stage_name[self.index]]
            if self.cell_list[self.cell_index] in self.canti_stage:
                self.canti_cell = self.canti_stage[self.cell_list[self.cell_index]]

        if self.iname[self.img_index] in self.cell_data:
            self.point_data = self.cell_data[self.iname[self.img_index]]
            self.point_saved.set_data(self.point_data[0],self.point_data[1])
            plt.draw()

        if self.iname[self.img_index] in self.canti_cell:
            self.point_data = self.canti_cell[self.iname[self.img_index]]
            self.canti_point_saved.set_data(self.point_data[0],self.point_data[1])
            plt.draw()

    def step_cell_data(self):
        # save the cell data into the stage data
        # reset the cell data
        self.stage_data[self.cell_list[self.cell_index]] = self.cell_data
        self.cell_data = {}
        self.canti_stage[self.cell_list[self.cell_index]] = self.canti_cell
        self.canti_cell = {}

    def step_stage_data(self):
        # for the step between stages save the points
        # and reset the data
        self.step_cell_data()
        self.data[self.stage_name[self.index]] = self.stage_data
        self.stage_data = {}
        self.canti_data[self.stage_name[self.index]] = self.canti_stage
        self.canti_stage = {}

    def key_v(self):
        self.canti_cell[self.iname[self.img_index]] = self.point_data
        if self.img_index == (len(self.imgs)-1):
            self.img_index = 0
        else:
            self.img_index += 1
        self.close_curve_next_curve()
        self.draw_point()
        # move left in the mea


    def toggle_selector(self, event):
        # move right in the image list
        if event.key in ['a', 'A']:
            if self.img_index == 0:
                self.img_index = len(self.imgs) -1
            else:
                self.img_index -= 1
            self.close_curve_next_curve()
            self.draw_point()
        # move left in the image list
        elif event.key in ['d','D']:
            if self.img_index == (len(self.imgs)-1):
                self.img_index = 0
            else:
                self.img_index += 1
            self.close_curve_next_curve()
            self.draw_point()
        # save the point
        elif event.key in ['w','W']:
            self.cell_data[self.iname[self.img_index]] = self.point_data
            # automatically moves when it is accepted
            if self.img_index == (len(self.imgs)-1):
                self.img_index = 0
            else:
                self.img_index += 1
            self.close_curve_next_curve()
            self.draw_point()
        # move left in the measurement stages
        elif event.key in ['t','T']:
            self.step_stage_data()
            if self.index ==2:
                self.index = 0
            else:
                self.index +=1
            self.img_index = 0
            self.cell_index = 0
            self.close_image_next_stage()
            self.draw_point()
        # move right in the measurement stages
        elif event.key in ['g','G']:
            self.step_stage_data()
            if self.index == 0:
                self.index = 2
            else:
                self.index -=1
            self.img_index = 0
            self.cell_index = 0
            self.close_image_next_stage()
            self.draw_point()
        # move right in the cell list
        elif event.key in ['r', 'R']:
            self.step_stage_data()
            if self.cell_index == (len(self.cell_list) -1):
                self.cell_index = 0
            else:
                self.cell_index += 1

            self.close_curve_next_curve()
            self.draw_point()
        # move left in the cell list
        elif event.key in ['e', 'E']:
            self.step_stage_data()
            if self.cell_index == 0:
                self.cell_index = len(self.cell_list) -1
            else:
                self.cell_index -= 1

            self.close_curve_next_curve()
            self.draw_point()

        elif event.key in ['c', 'C']:
            self.delete_point()

        elif event.key in ['v', 'V']:
            self.key_v()

        elif event.key in ['x','X']:
            self.step_stage_data()
            self.save_data()
            print('data saved')
            print('script closed')
            sys.exit(0)

    def prepare_data(self):
        '''
        prepares the measurement stage images for evaluation
        '''
        #print(self.index)
        #self.imgs = [skimage.io.imread(f) for f in self.fnames[self.index]]
        self.imgs = []
        self.iname = []
        for f in self.fnames[self.index]:
            self.iname.append(os.path.basename(f))
            self.imgs.append(skimage.io.imread(f))

    def setting_stage(self):
        """
        function to choose which cells are used
        """
        if self.index == 0:
            self.title_str = 'stage: before'
        elif self.index == 1:
            self.title_str = 'stage: during'
        elif self.index == 2:
            self.title_str = 'stage: after'

        self.title_str = f'stage: {self.stage_name[self.index]}, cell: {self.cell_index}, {self.iname[self.img_index]}'

    def draw_image(self):
        '''
        draws the force curve
        :return:
        '''
        self.setting_stage()
        self.ax.imshow(self.imgs[self.img_index])
        self.fig.suptitle(self.title_str)
        self.point, = self.ax.plot([], [], 'rx', ms=5)
        self.point_saved, = self.ax.plot([], [], 'mx', ms=10)
        self.canti_point_saved, = self.ax.plot([], [], 'gx', ms=10)
        #rescale the view and draw
        plt.draw()

    def run(self):
        '''
        oh wow
        '''
        self.prepare_data()
        self.draw_image()
        # event manager
        plt.connect("key_press_event", self.toggle_selector)
        plt.connect("button_release_event", self.onrelease)
        # plt.connect("scroll_event", self.zoom_fun) This doesnt work that well yet
        # Show everything
        plt.show()
