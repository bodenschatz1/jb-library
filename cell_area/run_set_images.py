import numpy as np
import matplotlib.pyplot as plt
import h5py
import glob
import os
import json
from tqdm import tqdm

from cell_area_functions import sort_images, get_measurement_parameter, get_files, get_img_path


from set_images import image_viewer

#############################################################################
paths = []
paths.append(r'../../../../../strain_measurements/191009/device1/eval_2019_11_06_17_12') # 0
paths.append(r'../../../../../strain_measurements/191010/device1/eval_2019_11_06_17_45') # 1
paths.append(r'../../../../../strain_measurements/191010/device2/eval_2019_11_06_17_55') # 2
paths.append(r'../../../../../strain_measurements/191016/device2/eval_2019_11_05_10_28') # 3
paths.append(r'../../../../../strain_measurements/191018/device1/eval_2019_11_06_12_19') # 4
paths.append(r'../../../../../strain_measurements/200704/device1/eval_2020_07_21_14_41') # 5
paths.append(r'../../../../../strain_measurements/200704/device2/eval_2020_07_21_15_14') # 6
paths.append(r'../../../../../strain_measurements/200704/device3/eval_2020_07_21_15_45') # 7 6
paths.append(r'../../../../../strain_measurements/200729/device1/eval_2020_08_17_16_31') # 8 7
#paths.append(r'../../../../../strain_measurements/200730/device1/eval_2020_08_17_17_00') # 9  only before
paths.append(r'../../../../../strain_measurements/200730/device2/eval_2020_08_17_17_06') #10 8
paths.append(r'../../../../../strain_measurements/200731/device1/eval_2020_08_17_17_38') #11
paths.append(r'../../../../../strain_measurements/200801/device1/eval_2020_08_17_21_23') #12
paths.append(r'../../../../../strain_measurements/200801/device2/eval_2020_08_17_23_02') #13 9
paths.append(r'../../../../../strain_measurements/200802/device1/eval_2020_08_18_00_07') #14 10
#paths.append(r'../../../../../strain_measurements/200802/device2/eval_2020_08_18_00_28') #15
paths.append(r'../../../../../strain_measurements/210122/device2/eval_2021_01_27_16_49')
paths.append('../../../../../strain_measurements/210203/device1/eval_2021_02_03_18_31')
paths.append('../../../../../strain_measurements/210204/device1/eval_2021_02_04_16_53')
paths.append('../../../../../strain_measurements/210204/device2/eval_2021_02_08_17_20')
paths.append('../../../../../strain_measurements/210204/device2m2/eval')
paths.append('../../../../../strain_measurements/210205/device1/eval_2021_02_08_17_59')
paths.append('../../../../../strain_measurements/210205/device1m2/eval_2021_02_08_18_08')
paths.append('../../../../../strain_measurements/210205/device2/eval_2021_02_09_18_20')
paths.append('../../../../../strain_measurements/210206/device1/eval_2021_02_09_10_13')
paths.append('../../../../../strain_measurements/210206/device1m2/eval_2021_02_09_10_45')
paths.append('../../../../../strain_measurements/210206/device2/eval_2021_02_09_11_18')
paths.append('../../../../../strain_measurements/210207/device1/eval_2021_02_09_11_40')
paths.append('../../../../../strain_measurements/210207/device2/eval_2021_02_09_11_50')
#karim stuff
paths.append('../../../../../strain_measurements/karim/BA_data/200509/KA032/eval_2020_06_24_11_29')
paths.append('../../../../../strain_measurements/karim/BA_data/200509/KA032/eval_2020_06_24_11_29')
#paths.append('../../../../strain_measurements/karim/BA_data/200510/KA033/eval_2020_06_24_11_26')
paths.append('../../../../../strain_measurements/karim/BA_data/200510/KA034/eval_2020_06_24_11_19')
#paths.append('../../../../strain_measurements/karim/BA_data/200610/KA037/eval_2020_06_24_11_03')
paths.append('../../../../../strain_measurements/karim/BA_data/200610/KA038/eval_2020_06_24_10_56')
paths.append('../../../../../strain_measurements/karim/BA_data/200611/KA039/eval_2020_06_24_10_47')
paths.append('../../../../../strain_measurements/karim/BA_data/200611/KA040/eval_2020_06_24_10_35')
paths.append('../../../../../strain_measurements/karim/BA_data/200612/KA041/eval_2020_06_24_10_28')
paths.append('../../../../../strain_measurements/karim/BA_data/200612/KA042/eval_2020_06_24_10_22')
#paths.append('../../../../strain_measurements/karim/BA_data/200613/KA043/eval_2020_06_24_10_17')
paths.append('../../../../../strain_measurements/karim/BA_data/200613/KA044/eval_2020_06_24_10_10')
paths.append('../../../../../strain_measurements/karim/BA_data/200614/KA045/eval_2020_06_24_09_51')
# recorrected
paths.append('../../../../../strain_measurements/karim/BA_data/200510/KA033/eval_2020_10_28_12_05')
paths.append('../../../../../strain_measurements/karim/BA_data/200610/KA037/eval_2020_10_27_16_17')
paths.append('../../../../../strain_measurements/karim/BA_data/200613/KA043/eval_2020_10_27_12_06')

# -------------------------------------------


index = 3
print(index)
path = paths[index]
print(path)
parm = get_measurement_parameter(path)
img_path = get_img_path(path,parm)
fnames, _ = get_files(img_path,'tif')
fnames = sort_images(fnames,path,parm)

test = image_viewer(fnames,img_path,parm)
test.run()
