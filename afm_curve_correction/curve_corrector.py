import glob
import matplotlib.pyplot as plt
import numpy as np
import os
import datetime
import json
from matplotlib.widgets import RectangleSelector
from SDF.file_io import load_from_force_sdf
import h5py
import sys
import jpkfile

"""
v1.0
curve corrector adjusted to be used with the newest SDF
- looks like everything is saved correctly should be double checked
23.7.21 - Jonathan Bodenschatz


v1.0.1
Semi automatic approach for the baseline and contact point finding.
"""




# get files function
def get_files(path,type='force.sdf',show=False):
    '''
    :param path: path
    :param type: file ending
    :param show: show the list
    :return: list of file names
    :return: dictionary so that user use it to find single files
    and key_names
    '''
    fnames = glob.glob(path+'/*.'+type)
    fnames.sort()
    keys_fnames = {}
    for i in range(len(fnames)):
        keys_fnames[os.path.basename(fnames[i])] = i
        if show:
            print(os.path.basename(fnames[i]))
    return fnames, keys_fnames
# create eval folder
def eval_folder(path,name = None):
    '''
    makes directory for the evaluation data
    :param path:
    :return:
    '''
    if name == None:
        directory = path + '/eval_' + datetime.datetime.today().strftime('%Y_%m_%d_%H_%M')
        os.makedirs(directory)
    else:
        directory = path + '/'+name
        if os.path.isdir(directory):
            pass
        else:
            os.makedirs(directory)
    return directory

def fit_output_load(path):
    fnames, _ = get_files(path,'txt')
    for fname in fnames:
        if 'fit_output' in fname:
            with open(fname) as json_file:
                fdic= json.load(json_file)
                if 'baseline' in fdic.values():
                    flist = list(fdic.keys())
        else:
            continue
    return flist


class curve_correction():
    '''
    used to correct the curves
    fnames : list of file names to correct
    path : path to those file names
    data_type : which data type do these file names have 'jpkforce','hdf5','sdf.force'
    select_contact_pont : boolean if contact point should be selected by hand
    select_fit_range : boolean if the fit range should be selected by hand
    '''
    def __init__(self,
                 fnames,
                 path,
                 eval_folder,
                 data_type,
                 select_contact_point = True,
                 select_fit_range = True):
 
        self.data_type = data_type # jpk.force or sdf.force or hdf5
        # error message about data_type
        if data_type not in ['jpk.force' , 'sdf.force', 'hdf5']:
            print('Error: please choose one of these file types: "jpk.force","sdf.force","hdf5"')
            sys.exit()
        self.user_fit_contact_point_select = select_contact_point
        self.user_fit_range_select = select_fit_range

        self.fnames = fnames
        self.index = 0
        self.eval_folder = eval_folder
        self.path = path
        self.p = None # parameters from the box
        # Setup the figure
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)

        self.line_fc, = self.ax.plot([], [], 'k')
        self.line_hl, = self.ax.plot([], [], 'g-', lw=2)
        self.line_bl, = self.ax.plot([], [], 'm--', lw=2)
        self.line_cp, = self.ax.plot([], [], 'mo', lw=4)
        self.line_est_cp, = self.ax.plot([], [], 'mx', lw=5)

        self.ax.set_xlabel("Time / s", size=16)
        self.ax.set_ylabel("Force / nN", size=16)
        self.ax.tick_params("both", labelsize=14)
        self.ax.grid(True)
    # -----------------------------------------------------------------
    # auto contact point functions
    # -----------------------------------------------------------------
    def contact_point_estimator(self,x,y):

        def sigmoid(x,c):
            return -1/(1+np.exp(-x*c)) + 1

        def _rotate(time,force,angle):
            angle = np.radians(angle)
            c = np.cos(angle)
            s= np.sin(angle)
            rot_time = time * c  - force * s
            rot_force = time * s + force * c

            return rot_time, rot_force

        def _cp_rotate(time,force,angles):
            data = np.asarray([_rotate(time,force,angle) for angle in angles])
            midx = np.argmin(data[:,1,:], axis=(1))
            ratios = force[midx] / (force.max() - force.min())
            weights = sigmoid(ratios,c=15)
            weights = weights / np.sum(weights)
            f_angles = -1 * angles
            mina = np.asarray([_rotate(data[n,0,midx[n]],data[n,1,midx[n]],f_angles[n]) for n in range(len(angles))])
            return mina, weights

        def cp_rotate(time,force,rate=200,baseline_limiter=0.3):
            # angles for rotation
            angles = np.linspace(-180,0,1000)
            # only take first segment
            time = time[:np.argmax(force)]
            force = force[:np.argmax(force)]
            #rate
            time = time[::rate]
            force = force[::rate]
            # limit baseline
            force = force[time.max() * baseline_limiter < time]
            time = time[time.max() * baseline_limiter < time]
            # minimums plus weights
            mina, weights = _cp_rotate(time,force,angles)
            mina_rounded = np.around(mina[:,0],4)
            # find all unique values
            uniq, idx = np.unique(mina_rounded,return_index = True)
            # adjust the weights accordorin to uniq number lengths
            adj_weights = np.asarray([np.sum(weights[mina_rounded==u]) for u in uniq])  / len(force)
            # contact point
            cp = uniq[np.argmax(adj_weights)]
            return cp, adj_weights, uniq, idx

        self.est_cp, weights, uniq, idx = cp_rotate(x,y)
    # -----------------------------------------------------------------
    # helper functions
    # -----------------------------------------------------------------
    def close_curve_next_curve(self, message, trace_ok = 'yes',correction_done = True):
        if correction_done:
            self.file.attrs['corr_type'] = 'done'
        print(message)
        self.p = None
        self.corr_type = 'baseline'
        self.file.attrs['trace_ok'] = trace_ok
        self.file.close()
        self.reset_all_lines()
        self.index += 1
        self.ax.set_ylim([None, None])
        self.ax.relim(visible_only=True)
        self.ax.autoscale_view()
        self.draw_force_curve()
        self.ax.autoscale(True)

    def save_standard_data(self, curve, data, relvant_segments):

        for index, rel_segment in enumerate(relvant_segments):
            self.file.create_dataset(f's{index}_height',data=data[index,0])
            self.file.create_dataset(f's{index}_force',data=data[index,1])
            #self.file.attrs[f's{index}_duration'] = curve.segments[rel_segment].duration
            #self.file.attrs[f's{index}_velocity'] = float(curve.segments[rel_segment].velocity) * self.unit_height
            self.file.attrs[f's{index}_duration'] = float(curve.workspaces[rel_segment].instruments['segment-parameters']['duration'].value)
            self.file.attrs[f's{index}_velocity'] = float(curve.workspaces[rel_segment].instruments['segment-parameters']['velocity'].value) * self.unit_height

        self.file.create_dataset('time', data=self.time)
        self.file.create_dataset('force', data=self.force)

    def find_correct_data(self, curve):
        '''
        This functions sorts through all the segments and than finds a range of indices spanning from
        extend to retract, taking only the relvant data
        '''
        #segments = np.arange(curve.GetSegmentNumber())
        # now need keys
        segments = list(curve.workspaces.keys())

        #direction = [curve.segments[i].direction for i in segments]
        # list of directions
        direction = [curve.workspaces[seg].instruments['segment-parameters']['direction'].value for seg in segments]

        relvant_segments = segments[direction.index('z-extend-force'):direction.index('z-retract-height')+1]

        #data = [(curve.segments[i].measuredTipSample * self.unit_height,
        #         curve.segments[i].vDeflection * self.unit_vDeflection) for i in relvant_segments]
        # get data
        data = [(curve.workspaces[seg].datasets['measuredTipSample'].data * self.unit_height,
                 curve.workspaces[seg].datasets['vDeflection'].data * self.unit_vDeflection) for seg in relvant_segments]

        data = np.asarray(data)

        merged_force = np.concatenate((data[0,1],data[1,1]),axis=None)
        #merged_duration = curve.segments[relvant_segments[0]].duration + curve.segments[relvant_segments[1]].duration
        merged_duration = float(curve.workspaces[relvant_segments[0]].instruments['segment-parameters']['duration'].value) + float(curve.workspaces[relvant_segments[1]].instruments['segment-parameters']['duration'].value)

        self.time = np.linspace(0,merged_duration,len(merged_force))
        self.force = merged_force

        self.time_corrected = self.time
        self.force_corrected = self.force


        return data, relvant_segments



    def toggle_selector(self, event):
        if event.key in ['a', 'A']:
            if self.p is not None:

                # baseline correction mode
                if self.corr_type == 'baseline':
                    self.force_corrected = self.force_corrected - np.poly1d(self.p)(self.time_corrected)
                    self.reset_all_lines()
                    self.line_fc.set_data(self.time_corrected, self.force_corrected)
                    # rescale the view and draw
                    self.ax.relim(visible_only=True)
                    self.ax.autoscale_view()
                    #self.ax.set_ylim([-0.5, None])
                    self.ax.set_ylim([np.min(self.force_corrected)-0.5, np.max(self.force_corrected) + 0.1 * np.max(self.force_corrected)])
                    self.file.attrs['baseline_parameter'] = self.p
                    self.file.attrs['corr_type'] = 'baseline_parameter'
                    #self.p = None
                    self.p=None
                    print('baseline')
                    if self.user_fit_contact_point_select:
                        self.corr_type = 'contact_point'
                        self.text = self.fig.text(0.05,
                                                  0.75,
                                                  "contact point",
                                                  transform=self.ax.transAxes,
                                                  size=12)
                    else:
                        self.close_curve_next_curve('baseline saved, next curve')
                    
                    x = self.time_corrected
                    y = self.force_corrected
                    self.contact_point_estimator(x,y)
                    self.line_est_cp.set_data(self.est_cp,0)
                    self.p=self.est_cp
                    self.line_cp.set_data(self.p,0)
                    plt.draw()

                # contact point mode
                elif self.corr_type == 'contact_point':
                    self.reset_all_lines()
                    self.line_fc.set_data(self.time_corrected, self.force_corrected)
                    if type(self.p) in (tuple, list):
                        self.line_cp.set_data(self.p[0],0)
                        self.file.attrs['contact_point'] = self.p[0]
                    else:
                        self.line_cp.set_data(self.p,0)
                        self.file.attrs['contact_point'] = self.p
                    self.file.attrs['corr_type'] = 'contact_point'
                    self.p = None
                    print('contact point')
                    if self.user_fit_range_select:
                        self.corr_type = 'fit_range'
                        self.text.set_visible(False)
                        self.text = self.fig.text(0.05,
                                                  0.75,
                                                  "fit range",
                                                  transform=self.ax.transAxes,
                                                  size=12)
                    else:
                        self.file.attrs['corr_type'] = 'done'
                        self.corr_type = 'baseline'
                        self.file.attrs['trace_ok'] = 'yes'
                        self.text.set_visible(False)
                        self.text = self.fig.text(0.05,
                                                  0.75,
                                                  "baseline",
                                                  transform=self.ax.transAxes,
                                                  size=12)
                        self.p = None
                        print('next curve')
                        self.file.close()
                        self.reset_all_lines()
                        self.index += 1
                        self.ax.set_ylim([None, None])
                        self.ax.relim(visible_only=True)
                        self.ax.autoscale_view()
                        self.draw_force_curve()
                        self.ax.autoscale(True)
                elif self.corr_type == 'fit_range':
                    self.file.attrs['fit_range'] = self.p
                    self .text.set_visible(False)
                    self.text = self.fig.text(0.05,
                                 0.75,
                                 "baseline",
                                 transform=self.ax.transAxes,
                                 size=12)
                    self.close_curve_next_curve(message = 'fit range / next curve',
                                                trace_ok = 'yes',
                                                correction_done = True)
        elif event.key in ['c','C']:
            self.p = None
            self.corr_type = 'baseline'
        elif event.key in ['x','X']:
            # this function also closes the file
            self.close_curve_next_curve(message = 'curve removed / next curve',
                                        trace_ok = 'no',
                                        correction_done = False)

    def rectangle_selector_callback(self, ec, er):
        """
        Callback that is called when a rectangle is drawn.

        Get bounding box of rectangle and plot thingies.
        """
        # bounding box
        x1, y1 = ec.xdata, ec.ydata
        x2, y2 = er.xdata, er.ydata

        # get a slice of the data
        x = self.time_corrected
        y = self.force_corrected

        xslice = x[(x > x1) & (x < x2)]
        yslice = y[(x > x1) & (x < x2)]

        # plot the highlight
        self.line_hl.set_data(xslice, yslice)
        if self.corr_type == 'baseline':
            # perform the baseline regression
            self.p = np.polyfit(xslice, yslice, 1)
            self.line_bl.set_data(x, np.poly1d(self.p)(x))

        elif self.corr_type == 'contact_point':
            # plot cp
            self.line_cp.set_data(xslice.mean(), 0)
            self.p = [xslice.mean(), yslice.mean()]
            # estimated cp
            self.contact_point_estimator(x,y)
            self.line_est_cp.set_data(self.est_cp,0)

        elif self.corr_type == 'fit_range':
            self.p = [x1, x2, y1, y2]


        # draw everything
        plt.draw()

    def prepare_data(self):
        '''
        prepares the data from jpk and sdf-force
        '''
        if self.index == len(self.fnames):
            print('it is done')
            sys.exit()
        if self.data_type == 'hdf5':
            self.file = h5py.File(self.fnames[self.index],'a')
            self.time_corrected = self.file['time'][:]
            self.force_corrected = self.file['force'][:]
            self.time = self.time_corrected
            self.force = self.force_corrected
        else:
            temp_file_name = self.eval_folder + self.fnames[self.index][len(self.path):] +'_corrected.hdf5'
            if os.path.exists(temp_file_name):
                os.remove(temp_file_name)
            self.file = h5py.File(self.eval_folder + self.fnames[self.index][len(self.path):] +'_corrected.hdf5', 'a')
            self.unit_vDeflection = 1e9
            self.unit_height = 1e6
            if (self.data_type == 'sdf-force') or (self.data_type == 'sdf.force'):
                curve = load_from_force_sdf(self.fnames[self.index])
                #curve = sdf_force(self.fnames[self.index])
                data, relvant_segments = self.find_correct_data(curve)
                # if i need it
                self.file.attrs['segment_number'] = len(data)
                # saving the stuff that is nice to have
                self.save_standard_data(curve,data,relvant_segments)

            else:
                curve = jpkfile.JPKFile(self.fnames[self.index])
                #stuff that is nice to have everything in SI units
                if len(curve.segments) == 2:
                    s0_time = curve.segments[0].get_array(['t'])[0]
                    s0 = curve.segments[0].get_array(['vDeflection',
                                                      'strainGaugeHeight'])[0]
                    s1_time = curve.segments[1].get_array(['t'])[0]
                    s1 = curve.segments[1].get_array(['vDeflection',
                                                      'strainGaugeHeight'])[0]
                elif len(curve.segments) == 3:
                    s0_time = curve.segments[0].get_array(['t'])[0]
                    s0 = curve.segments[0].get_array(['vDeflection',
                                                      'strainGaugeHeight'])[0]
                    s1_time = curve.segments[1].get_array(['t'])[0]
                    s1 = curve.segments[1].get_array(['vDeflection',
                                                      'strainGaugeHeight'])[0]
                    s2_time = curve.segments[2].get_array(['t'])[0]
                    s2 = curve.segments[2].get_array(['vDeflection',
                                                      'strainGaugeHeight'])[0]
                    self.file.create_dataset('s2_force',
                                             data = s2['vDeflection'] * self.unit_vDeflection) # in nN
                    self.file.create_dataset('s2_height',
                                             data = s2['strainGaugeHeight'] * 1e6) # in µm
                    self.file.attrs['s2_duration'] = s2_time['t'].max() # in s
                merged_force = np.concatenate((s0['vDeflection'],
                                               s1['vDeflection']),axis=None)
                merged_duration = s0_time['t'].max() + s1_time['t'].max()
                self.time = np.linspace(0, merged_duration, len(merged_force))  # s
                self.force = merged_force * self.unit_vDeflection  # now nN
                # save data in hdf5
                self.file.create_dataset('time', data=self.time)
                self.file.create_dataset('force', data=self.force)
                self.file.create_dataset('s0_force',
                                         data = s0['vDeflection'] * self.unit_vDeflection) # in nN
                self.file.create_dataset('s1_force',
                                         data = s1['vDeflection'] * self.unit_vDeflection) # in nN
                self.file.create_dataset('s0_height',
                                         data = s0['strainGaugeHeight'] * 1e6) # in µm
                self.file.create_dataset('s1_height',
                                         data = s1['strainGaugeHeight'] * 1e6) # in µm
                temp = s0['strainGaugeHeight'] - s0['strainGaugeHeight'].min()  # to calc. velocity
                self.file.attrs['s0_velocity'] = temp.max()/s0_time['t'].max() * 1e6 # velocity in µm/s
                self.file.attrs['s0_duration'] = s0_time['t'].max() # in s
                self.file.attrs['s1_duration'] = s1_time['t'].max() # in s
                self.time_corrected = self.time
                self.force_corrected = self.force


    def draw_force_curve(self):
        '''
        draws the force curve
        :return:
        '''
        self.prepare_data()
        self.fig.suptitle(os.path.basename(self.fnames[self.index]))
        self.line_fc.set_data(self.time,self.force)

       #---------------------------------------------------------------------------------
        # get a slice of the data
        x = self.time_corrected
        y = self.force_corrected

        xslice = x[100:1500]
        yslice = y[100:1500]

        # plot the highlight
        self.line_hl.set_data(xslice, yslice)
        self.p = np.polyfit(xslice, yslice, 1)
        self.line_bl.set_data(x, np.poly1d(self.p)(x))
        #self.ax.text(2, 6, r"an equation: $E=mc^2$", fontsize=15)

        #self.line_cp.set_data(xslice.mean(), 0)
        #self.p = [xslice.mean(), yslice.mean()]
        #self.contact_point_estimator(x,y)
        #self.line_est_cp.set_data(self.est_cp,0)

        #---------------------------------------------------------------------------------

        #rescale the view and draw
        self.ax.relim(visible_only=True)
        self.ax.autoscale_view()
        plt.draw()

    def reset_all_lines(self):
        """ 
        Reset all the lines to empty.
        """
        self.line_fc.set_data([], [])
        self.line_hl.set_data([], [])
        self.line_bl.set_data([], [])
        self.line_cp.set_data([], [])
        self.line_est_cp.set_data([],[])


    def run(self):
        self.corr_type = 'baseline'  # starts with baseline
        self.draw_force_curve()
        # event manager
        plt.connect("key_press_event", self.toggle_selector)
        # plt.connect("scroll_event", self.zoom_fun) This doesnt work that well yet
        # Create the rectangle selector
        RS = RectangleSelector(self.ax,
                               self.rectangle_selector_callback,
                               drawtype="box",
                               useblit=True,
                               minspanx=5,
                               minspany=5,
                               spancoords='pixels',
                               button=[1, 3],
                               interactive=True)
        # Show everything
        plt.tight_layout()
        plt.show()


