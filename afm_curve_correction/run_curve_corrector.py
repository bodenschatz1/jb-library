from curve_corrector import eval_folder,get_files,curve_correction,fit_output_load
import os
import numpy as np
import json
"""
script to run force curve correction from either sdf force or hdf5
Warning!! jpk.force might be a special case for very hold force curve check before use!
"""

# paths to the folders
paths = []
paths.append('./sachen zum fitten')

path = paths[1]

#which file type shoud be loaded
ftype = 'force.sdf'

curves_adjusted_with_jpk = False

# some choices
choosen_curves = False
from_fit_output = False

if from_fit_output:
    new_eval_folder = os.path.basename(path)
    ftxt, _ = get_files(path,'txt')
    for ft in ftxt:
        if 'fit_output' in ft:
            fname_txt = ft
    with open(fname_txt) as json_file:
        text = json.load(json_file)
    ftype = 'hdf5'
    fnames, _ = get_files(path,'hdf5')
    fit_output_fnames = []
    for fname in fnames:
        for txt_name in list(text.keys()):
            if os.path.basename(txt_name) in fname:
                fit_output_fnames.append(fname)
                break
    #np.save(os.path.join(path,'index_list.npy'),index_list)
    test = curve_correction(fit_output_fnames,
                            path,
                            new_eval_folder,
                            ftype,
                            select_contact_point=True,
                            select_fit_range = False)
    test.run()
    np.save(os.path.join(path,'index_list.npy'),index_list)

elif choosen_curves:
    curve_file = os.path.join(path,'cap_curves.npy')
    cap_curves = np.load(curve_file)
    cap_curves = cap_curves[:,:,1].flatten()
    cap_curves = cap_curves.astype(int)
    fnames, _ = get_files(path)
    cap_fnames = [fnames[curve] for curve in cap_curves]
    ftype = 'sdf.force'
    new_eval_folder = eval_folder(path)
    test = curve_correction(cap_fnames,
                            path,
                            new_eval_folder,
                            ftype,
                            select_contact_point=True,
                            select_fit_range = False)
    test.run()

else:
    fnames, _ = get_files(path)
    if fnames == []: # checking if there are sdf.force files if not use jpk.force
        print('no sdf-force found using jpkfile on jpk.force files')
        fnames,_ = get_files(path,'jpk-force')
        # check if its hdf5
        if fnames == []:
            fnames, _ = get_files(path,'hdf5')
            new_eval_folder = path
            ftype = 'hdf5'
        else:
            ftype = 'jpk.force'
            new_eval_folder = eval_folder(path)
    else:
        ftype = 'sdf.force'
        new_eval_folder = eval_folder(path)
    test = curve_correction(fnames,
                            path,
                            new_eval_folder,
                            ftype,
                            select_contact_point=True,
                            select_fit_range = False)
    test.run()
