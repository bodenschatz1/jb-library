import numpy as np
import h5py
import pandas as pd
from numpy import pi, sin, sqrt
from math import floor
from scipy.signal import savgol_filter

from .generics import get_generics
from . import fdc_tools



class ViscoSphereFit:
    '''
    class based on vectorize_fitting_function discher_generic_CP
    '''
    def __init__(self,fname,angle,Rp_abs,R1):
        self.fname = fname
        self.angle = angle
        self.Rp_abs = Rp_abs
        self.R1 = R1

        self.r_g, self.r_area = get_generics(R1, Rp_abs, angle)

        self.Phio = self.angle * pi / 180 # contact angle before indation in radians
        self.Ro = self.R1 / sin(self.Phio)
        self.h = self.Ro - sqrt(self.Ro ** 2 - self.R1 ** 2) # height sphere cap
        self.Vkk = (1/3) * self.h ** 2 * pi * (3 * self.Ro - self.h) # volume sphere cap
        self.A0 = 2 * pi * self.Ro * self.h # surface area sphere cap
        self.r1 = 0.0001 # startparmeter 1
        self.phi = self.Phio # startparmeter 2
        self.a = 0 # baseline stuff
        self.b = 0 # baseline stuff
        self.tm  = 0.2
        self.cont = 0.04
        self.T = 0.1e-3 # tension in N/m
        self.Ka = 0.1 # compressibility modulus in N/m


    def load_curve(self):
        file = h5py.File(self.fname,'r+')
        self.contact_point = fdc_tools.read_contact_point(file)
        if 'contact_point_fitted' in file.attrs:
            self.contact_point_fitted = file.attrs['contact_point_fitted']
        self.time, self.force = fdc_tools.read_data_2be_fitted(file)
        self.velocity = fdc_tools.read_s0_velocity(file)
        file.close()

    def prepare_curve(self):
        self.load_curve()
        self.force = self.force - self.force[0]  # will see what happens
        self.tm = self.time[self.force==max(self.force)] # time of maximum indentation
        # data reduction
        red = 1000
        data_red = floor(len(self.time) / 1000)
        self.force = savgol_filter(self.force,27,1)
        self.time = self.time[::data_red]
        self.force = self.force[::data_red]

    def vectorize_fitting_function_discher_generic_CP(T,time,Ka,beta,a,b,par):
        print('ayy')

    def visco_sphere_fit(self):
        '''
        this is where simfit will happen
        '''
        print('wow')

def area_integral():


def fit_function_skalar_discher(self):

    def func(time,force):
        y
'''
    def f(x):
            x = np.asarray(x)
            y = np.zeros(x.shape)
            y += (x<0)            * 0           #region 1
            y += ((x>=0) & (x<1)) * x**2        #region 2
            y += (x>=1)           * (0.5*x+0.5) #region 3
            return y

        x = np.linspace(-100,100)

        y = f(x)

        plt.plot(x,y,'.b')
        plt.show()
'''
