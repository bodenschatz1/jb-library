import sys
import os
import SimFit
import h5py
import json
import matplotlib
#matplotlib.use('Agg')
import time as real_time
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
from scipy.optimize import fsolve, root
from scipy.signal import savgol_filter
from scipy.integrate import quad
from numpy import pi as PI
from numpy import sin, cos, tan, sqrt, arctan
from numpy.lib import scimath
from numba import float64, njit, jit

from multiprocessing import Pool

from ftc_tools import get_files,fit_output_load, read_data_2be_fitted, read_s0_velocity, read_contact_point, read_baseline_corr, read_height, cell_load


'''
This is a translation from the matlab version as well as a script from Filip Savic

for the viscoelastic sphere
by
Jonathan Bodenschatz
'''

#------------------ data processing functions-----------------------
def apply_fit_range(ori_time, ori_height, ori_force, fit_range):
    '''
    this function applies the fit range to the data
    '''
    # slicing to fit_range
    force = ori_force[(ori_time > fit_range[0])&(ori_time < fit_range[1])]
    height = ori_height[(ori_time > fit_range[0])&(ori_time < fit_range[1])]
    time = ori_time[(ori_time > fit_range[0])&(ori_time < fit_range[1])]
    return time, height, force

def data_reduction(time_in, force_in, height_in, reduction = 500):
    '''
    reduction : data reduction /  number or points left over for fitting
    --------------------------------------------------------------------
    notes : matlab downsample seems to just be array slicing
    '''
    data_length = len(time_in)
    # from matlab downsample seems to just be array slicing of the array by the downbyhowmuch value
    downbyhowmuch = np.floor(data_length / reduction)

    # change to int
    downbyhowmuch = downbyhowmuch.astype(int)

    # downsampling with array slicing
    time = time_in[::downbyhowmuch]
    force = force_in[::downbyhowmuch]
    height = height_in[::downbyhowmuch]
    # here there is come sogolayfilt in matlab but the data is overriden
    # this method does not work, from scipy.signal leads to errors
    # time = resample(time_in,downbyhowmuch)
    # force = resample(force_in,downbyhowmuch)
    return time, force, height

# get index
def _find_contact_point_index(time,contact_point):
        return np.abs(time-contact_point).argmin()

def improved_data_reduction(time,height,force,contact_point, reduction):
    '''
    a data reduction that also includes a contact point
    '''
    data_length = len(time)
    # from matlab downsample seems to just be array slicing of the array by the downbyhowmuch value
    downbyhowmuch = np.floor(data_length / reduction)

    # change to int
    downbyhowmuch = downbyhowmuch.astype(int)

    # cp index
    index_cp = _find_contact_point_index(time,contact_point)

    time = time[index_cp::downbyhowmuch]
    height = height[index_cp::downbyhowmuch]
    force = force[index_cp::downbyhowmuch]

    return time, height, force

def get_data(file):
    '''
    get all the required data and the fit range
    from the hdf5 file
    '''
    time, force = read_baseline_corr(file)
    height = read_height(file)
    contact_point = read_contact_point(file)
    s0_velocity = read_s0_velocity(file)
    return time, height, force, contact_point, s0_velocity

# save fit data into hdf5
def save_fit(fname,
             fit_parameter,
             fitted_time,
             fitted_height,
             fitted_force,
             fit_force,
             data_reduction,
             fit_range_factor):
    # fit data
    # fit_force = discher_final_function_visco(height,par[0],par[1],par[2],r1,phi)
    # open h5df file
    file = h5py.File(fname,'a')

    # check if fitparameter are already saved and  delete them
    if 'fit_parameter' in file:
        del file['fit_parameter']
    if 'fit_force' in file:
        del file['fit_force']  # from fit
        del file['fitted_force'] # force data that is fitted
        del file['fitted_height']
        del file['fitted_time']

    # save the data to hdf5
    file.create_dataset('fit_force', data = fit_force*1e3) # now in nN
    file.create_dataset('fitted_force', data = fitted_force*1e3) # now in nN
    file.create_dataset('fitted_height', data = fitted_height)
    file.create_dataset('fitted_time', data = fitted_time)
    file.create_dataset('fit_parameter',data = fit_parameter)
    file.attrs['data_reduction_factor'] = data_reduction
    file.attrs['fit_range_factor'] = fit_range_factor
    file.attrs['fit_R1'] = R1
    file.attrs['fit_angle'] = angle
    file.close()

# ------------------ Parameters -------------------------------------
angle = 35 #contact angle before indentation
R1 = 12 # radius cell?
Rp = 6.62 / 2 # radius of the indentor
Phio = angle * PI / 180 # contact angle in radians
Ro = R1 / sin(Phio)
h = Ro - sqrt(Ro ** 2 - R1 ** 2) # height of sphere cap
Vkk = (1/3) * h ** 2 * PI * (3 * Ro - h) # volume of sphere cap
A0 = 2 * PI * Ro * h # surface area of sphere cap
r1 = 0.0001 # start parameter radius in micro meter
phi = Phio # start parameter 2

# ---------------------- Functions needed --------------------------------
@njit(float64(float64), cache = True)
def theta(r1):
    f1 = sqrt(Rp**2 - r1**2)
    return np.arctan(r1 / f1)

@njit(float64(float64, float64), cache = True)
def A(r1, phi):
    '''
    Eq. A5(1) in ref. 1
    '''
    f1 = R1 * sin(phi) + r1 * sin(theta(r1))
    f2 = R1 ** 2 - r1 ** 2
    return f1 / f2

@njit(float64(float64, float64), cache=True)
def B(r1, phi):
    """
    Eq. A5(2) in ref. 1
    """
    return -A(r1, phi) * r1**2 - r1 * sin(theta(r1))

# for arrays
@njit(float64[:](float64[:]), cache = True)
def theta_array(r1):
    f1 = sqrt(Rp**2 - r1**2)
    return np.arctan(r1 / f1)



@njit(float64[:](float64[:], float64[:]), cache = True)
def A_array(r1, phi):
    '''
    Eq. A5(1) in ref. 1
    '''
    f1 = R1 * sin(phi) + r1 * sin(theta_array(r1))
    f2 = R1 ** 2 - r1 ** 2
    return f1 / f2

# --------------- other functions -------------------
@njit(float64(float64))
def hub(r1):
    """
    some kind of height
    """
    f1 = sqrt(Rp**2 - r1**2)
    return Rp - f1


# ---------------------------------the integrads -----------------------------------
@njit(float64(float64, float64, float64), cache=True)
def cyl(r, r1, phi):
    """
    Some other function I don't see the point of right now.
    """
    _A = A(r1, phi)
    _B = B(r1, phi)
    _C = _A * r + _B / r

    return _C / sqrt(1 - _C**2)

@njit(float64(float64, float64, float64), cache=True)
def volume_integrand(r, r1, phi):
    """
    The integrand in eq. A6, Sen, Discher, 2005, Biophys. J. 89(5) 3203-3213

    matlab calls this vol
    """
    return PI * r**2 * cyl(r, r1, phi)

@njit(float64(float64, float64, float64), cache=True)
def area_integrand(r, r1, phi):
    """
    The integrand in eq. A10, ref. 1

    called surface discher in the matlab code
    """
    _A = A(r1, phi)
    _B = B(r1, phi)
    _C = _A * r + _B / r

    return (2 * PI * r) / sqrt(1 - _C**2)

# ------------ The integrals using quad from scipy --------------------------------
def volume(r1, phi):
    """
    Calculate the volume using eq. A6 in ref. 1
    """
    #breakpoint()
    _V1 = quad(volume_integrand,
              r1,
              R1,
              args=(r1, phi))
    V1 = _V1[0]

    Vk = (1/3) * hub(r1)**2 * PI * (3 * Rp - hub(r1))

    return V1 - Vk

def area(r1, phi):
    """
    Calculate the volume using eq. A10 in ref. 1
    """
    A1 = quad(area_integrand,
              r1,
              R1,
              args=(r1, phi))[0]

    Ak = 2 * PI * Rp * hub(r1)

    return A1 + Ak

def some_height(r1, phi):
    """
    One integral appearing in jonathan's thingy.
    """
    K1 = quad(cyl,
              r1,
              R1,
              args=(r1, phi),
              limit = 500)[0]

    return K1 - hub(r1)

# -------------- here comes the equation system -------------------
def equation_system(xs,z_indexed):
    r1 = xs[0]
    phi = xs[1]
    #print(xs)
    # Get volume
    _V = volume(r1, phi)

    # Get area
    #_A = area(r1, phi) # unused

    # Get this weird height
    _h = some_height(r1, phi)

    # Derived things.
    #_alpha = (_A - A0) / A0 # unused
    _delta = h - _h

    # Construct output
    ret_1 = Vkk - _V
    ret_2 = z_indexed - _delta

    return [ret_1, ret_2]

#  ---------------- here comes the other stuff / directly transfered from matlab -----------------
def discher_function_visco(r1,phi,z,p):
    T = p[0]
    Ka = p[1]
    x0 = [r1, phi]
    #xs = fsolve(equation_system, x0, z, xtol=1e-8,epsfcn=1e10)
    sol = root(equation_system, x0, z, method='lm',tol=1e-8)
    xs = sol.x

    # some adjustments
    r1 = xs[0]
    phi = xs[1]

    # get area
    Area = area(r1,phi)
    alpha = (Area - A0) / A0

    # calculate the force / force_out in matlab <= DOESNT SEEM TO DO ANYTHING
    force = ((2 * PI * R1 ** 2 * A(r1,phi) - 2 * PI * R1 * sin(phi)) * (T + Ka*alpha))

    return force, r1, phi, Area

def discher_final_function_visco(z, T, Ka, beta, r1, phi):
    '''
    z    : height
    T    : p1
    Ka   : p2
    beta : p3
    '''
    # time must be global
    p = [T, Ka]
    # stuff array [area, Tfull, r1_i, phi_i, force]
    stuff = np.full([len(z),4],np.nan)
    # setting first element
    stuff[0,0] = A0   # must be global as well
    stuff[0,1] = T
    #stuff[0,2] = r1   # must be global
    #stuff[0,3] = phi
    stuff[0,2] = 0  # now closer to matlab make no difference tho
    stuff[0,3] = 0
    # for loop that goes through elements starting at element 1
    for i in range(1,len(z)):
        force, r1, phi, Area = discher_function_visco(r1,phi,z[i],p)
        stuff[i,0] = Area
        mem_term = (time[i] - time[:i]) ** (-beta)
        v_term = np.diff(stuff[:i+1,0]) / A0
        stress_now = np.sum(Ka * mem_term * v_term)
        stuff[i,1] = T + stress_now
        stuff[i,2] = r1
        stuff[i,3] = phi

    force =  ((2 * PI * R1 ** 2 * A_array(stuff[:,2],stuff[:,3]) - 2 * PI * R1 * sin(stuff[:,3]))* stuff[:,1])
    #force = stuff[:,4]
    return force

def run_fit(height,force):
    '''
    to run the fit
    '''
    startpar = {'T': 0.0003, 'Ka': 0.01, 'beta': 0.6}
    constants = {'r1' : r1,  'phi' : phi}
    s = SimFit.SimFit(xvals = height,yvals = force,
                      func = discher_final_function_visco,
                      startpar = startpar,
                      indep = 'z',
                      constant = constants,
                      #minpar = {'T' : 0, 'Ka' : 0, 'beta' : 0},
                      #maxpar = {'beta' : 1},
                      #tol = [1e-4, 1000],
                      cntrl = -1,
                      maxiter =1000)
    s.fittype = 'levenberg'
    s()
    return s.curr_par




def plot_fit(time,height,force, par, fname,ori_force,ori_time,ori_height, contact_point,
             xaxis='time', show = False, save=True):
    # generate fitted force
    fit_force = discher_final_function_visco(height,par[0],par[1],par[2],r1,phi)
    # stuff force plots
    _time = ori_time[ori_time >= contact_point]
    _height = ori_height[ori_time >= contact_point]
    # shift ori_time
    ori_time = ori_time - _time[0]
    # plot
    fig, ax = plt.subplots()
    if xaxis == 'time':
        ln1 = ax.plot(ori_time[ori_time > - 0.2], ori_force[ori_time > -0.2] * 1e-3,
                      'k.', alpha = 0.3,label = 'data')
        ln3 = ax.plot(time, force,label = 'reduction')
        ln2 = ax.plot(time, fit_force, linewidth = 3,label = 'fit')
        # x label
        ax.set_xlabel('time / s')
    else:
        ln1 = ax.plot(_height[0] - ori_height, ori_force * 1e-3, '.', label = 'data')
        ln3 = ax.plot(height, force,label = 'reduction')
        ln2 = ax.plot(height, fit_force, linewidth = 3,label = 'fit')
        # x label
        ax.set_xlabel('indentation depth / µm')

    # y label
    ax.set_ylabel('force / µN')

    # fit parameter as text
    ax.text(0.1, 0.4, 'T = '+str(np.around(par[0]*1e3,5))+' mN/m \nK$_A$ = '+str(np.around(par[1],5)) + ' N/m\n$\\beta$ = ' + str(np.around(par[2],5)), fontsize=12, transform = ax.transAxes)

    # legend
    lns = ln1 + ln2 + ln3
    labs = [l.get_label() for l in lns]
    ax.legend(lns,labs)
    plt.title(os.path.basename(fname)[:-25])
    figure_name = fname[:-25]+'.png'
    if save:
        plt.savefig(figure_name,dpi=150)
    if show:
        plt.show()
    else:
        plt.close()

    return fit_force

# ----------------------------------- multiprocessing
def main_process(fname):
    print(fname)
    # get hdf5 file and data
    file = h5py.File(fname,'r')
    # check if curve was considered
    #if 'trace_ok' in file.attrs:   # prehaps for later
    #   if file.attrs['trace_ok'] == 'yes':
    global time
    time, height, force, contact_point, s0_velocity = get_data(file)
    # -----------------check if baseline was correctly corrected
    if force.max() < 0:
        file.close()
        return (fname, 'baseline')
    # ----------------------------------
    if (fit_range_type == 'from file') & ('fit_range' in file.attrs):
        fit_range = file.attrs['fit_range']
    else:
        fit_range = [0,0]
        index_max = np.argmax(force)
        fit_range[0] = 0
        fit_range[1] = time[(force > force[index_max] * (1-value)) & (time > time[index_max])].max()

    ori_time, ori_height, ori_force = apply_fit_range(time, height, force, fit_range)
    # data is now adjusted
    time, height, force = apply_fit_range(time, height, force, fit_range)

    # smoothing with Savitzky-Golay filter
    sogolay_window = 27
    force = savgol_filter(force,sogolay_window,1)

    # reduce data points
    data_reduction_factor = 250
    time, force, height = improved_data_reduction(time, force, height,
                                                          contact_point,
                                                          data_reduction_factor)
    # all data has been extracted
    time = time - time[0]
    force = force * 1e-3 # now in microNewton
    # remaking the height
    maxi = np.argmax(force)
    piezo_app = s0_velocity * time[:maxi+1]
    if dwell:
        piezo_back = s0_velocity * time[maxi] * np.ones(len(time[maxi+1:]))
    else:
        piezo_back = 2 * s0_velocity * time[maxi] - s0_velocity * time[maxi+1:]
    height = np.append(piezo_app,piezo_back)

    # ===================== Parameters ====================================
    global angle, R1, theta, h, Vkk, A0  # global variables
    if para_from_file:
        angle = 35 #contact angle before indentation
        R1 = file.attrs['R1']
    else:
        angle = 35
        R1 = 12 # radius cell?
    file.close()
    Rp = 6.62 / 2 # radius of the indentor
    Phio = angle * PI / 180 # contact angle in radians
    Ro = R1 / sin(Phio)
    h = Ro - sqrt(Ro ** 2 - R1 ** 2) # height of sphere cap
    Vkk = (1/3) * h ** 2 * PI * (3 * Ro - h) # volume of sphere cap
    A0 = 2 * PI * Ro * h # surface area of sphere cap
    r1 = 0.0001 # start parameter radius in micro meter
    phi = Phio # start parameter 2

    height_check = height[np.argmax(force)]

    if h < height_check:
        #print(f'{h} < {height_check}')
        return (fname,'height')
    elif Rp < height_check:
        return (fname,'Rp')

    run = True
    if run:
        parameter = run_fit(height,force)
        fit_force = plot_fit(time,
                             height,
                             force,
                             parameter,
                             fname,
                             ori_force,
                             ori_time,
                             ori_height,
                             contact_point)

        save_fit(fname,
                 parameter,
                 time,
                 height,
                 force,
                 fit_force,
                 data_reduction_factor,
                 value)

# -----------------------------some settings---------------------------------
# Dwell or no dwell
global dwell
dwell = False  # if the curves have a dwell or not

# for testing set to false
run_multi_processing = True

# either 'from file' or 'not from file'
fit_range_type = 'from file'

# percentage of the maximal force kept for the fitting
global value
value = 0.5

# some choices
fit_from_choices = {'all curves' : 0,
                    'fit output list': 1,
                    'cell wise': 2}
# choose it
fit_from = fit_from_choices['all curves']

# choose where the parameters come from
global para_from_file
para_from_file = False

# number of cores
cores = 3
#============================run part here ==============================
paths = []
#paths.append('../../../../ownCloud/strain_measurements/test/err')
paths.append('../../../../ownCloud/strain_measurements/191009/device1/eval_2019_11_06_17_12')
#paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110310/map-data-2011.03.10-12.56.03/eval_2020_03_23_11_23')
#paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110310/map-data-2011.03.10-14.32.08/eval_2020_03_26_14_30')

# iterate through paths
if run_multi_processing:
    for path in paths:
        # use a shorter list of file names
        if fit_from == 1:
            fnames = fit_output_load(path)
        # list of all file names in folder
        elif fit_from == 0:
            fnames, _ = get_files(path)
        elif fit_from == 2:
            fnames = cell_load(path)
        p = Pool(cores)
        # returns file names of junk data
        stuff = list(tqdm(p.imap(main_process,fnames),total = len(fnames)))
        p.close()
        # waits for everything to finish
        p.join()

        # fit output saved as json
        file_name = path + '/fit_output.txt'
        stuff = list(filter(None, stuff))
        output = {}
        dic_stuff = dict(stuff)
        baselines = []
        heights = []
        Rps = []
        for (key,value) in dic_stuff.items():
            if value == 'baseline':
                baselines.append(key)
            elif value == 'height':
                heights.append(key)
            elif value == 'Rp':
                Rps.append(key)

        output['false_baselines'] = baselines
        output['wrong_height'] = heights
        output['error Rp'] = Rps

        with open(file_name, 'w+') as outfile:
            json.dump(output, outfile)

else:
    index = 0
    fname_i = 0
    path = paths[index]
    fnames, _ = get_files(path)
    fname = fnames[fname_i]

    fit = False

    t0 = real_time.time()
    # get hdf5 file and data
    file = h5py.File(fname,'r')
    global time
    time, height, force, contact_point, s0_velocity = get_data(file)
    # -----------------check if baseline was correctly corrected
    if force.max() < 0:
        file.close()
    # ----------------------------------
    if (fit_range_type == 'from file') & ('fit_range' in file.attrs):
        fit_range = file.attrs['fit_range']
    else:
        fit_range = [0,0]
        index_max = np.argmax(force)
        fit_range[0] = 0
        fit_range[1] = time[(force > force[index_max] * (1-value)) & (time > time[index_max])].max()
    # save
    ori_time, ori_height, ori_force = apply_fit_range(time, height, force, fit_range)
    # data is now adjusted
    time, height, force = apply_fit_range(time, height, force, fit_range)
    # smoothing with Savitzky-Golay filter
    sogolay_window = 27
    force = savgol_filter(force,sogolay_window,1)
    # reduce data points
    data_reduction_factor = 250
    time, force, height = improved_data_reduction(time, force, height,
                                                      contact_point, data_reduction_factor)
    # all data has been extracted
    time = time - time[0]
    force = force * 1e-3 # now in microNewton
    # remaking the height
    maxi = np.argmax(force)
    piezo_app = s0_velocity * time[:maxi+1]
    if dwell:
        piezo_back = s0_velocity * time[maxi] * np.ones(len(time[maxi+1:]))
    else:
        piezo_back = 2 * s0_velocity * time[maxi] - s0_velocity * time[maxi+1:]
        height = np.append(piezo_app,piezo_back)
    # ----------------------------------------------------------------
    # Parameters
    # ----------------------------------------------------------------
    if para_from_file:
        angle = 35 #contact angle before indentation
        R1 = file.attrs['R1']
    else:
        angle = 35
        R1 = 12 # radius cell?
    file.close()
    Rp = 6.62 / 2 # radius of the indentor
    Phio = angle * PI / 180 # contact angle in radians
    Ro = R1 / sin(Phio)
    h = Ro - sqrt(Ro ** 2 - R1 ** 2) # height of sphere cap
    Vkk = (1/3) * h ** 2 * PI * (3 * Ro - h) # volume of sphere cap
    A0 = 2 * PI * Ro * h # surface area of sphere cap
    r1 = 0.0001 # start parameter radius in micro meter
    phi = Phio # start parameter 2
    height_check = height[np.argmax(force)]
    if fit:
        #parameter = run_fit(height,force)
        '''
        to run the fit
        '''
        startpar = {'T': 0.0003, 'Ka': 0.01, 'beta': 0.6}
        constants = {'r1' : r1,  'phi' : phi}
        s = SimFit.SimFit(xvals = height,yvals = force,
                          func = discher_final_function_visco,
                          startpar = startpar,
                          indep = 'z',
                          constant = constants,
                          #minpar = {'T' : 0, 'Ka' : 0, 'beta' : 0},
                          #maxpar = {'beta' : 1},
                          #tol = [1e-4, 1e-4],
                          cntrl = 1,
                          maxiter =500)
        s.fittype = 'levenberg'
        s()
        parameter = s.curr_par
        t1 = real_time.time()
        fit_force = plot_fit(time,
                             height,
                             force,
                             parameter,
                             fname,
                             ori_force,
                             ori_time,
                             ori_height,
                             contact_point,
                             'time',
                             True,
                             False)
        print(f'Time spent: {(t1-t0) / 60} minutes')
        from scipy.io import savemat
        file_name = '../sandbox/matlab/aj_new_fits/sphere/test_data.mat'
        mat_dic = {'time':time,'z':height,'force':force,'vo':s0_velocity}
        savemat(file_name,mat_dic)

    else:
        from_mat = False

        para = [0.0001,0.016,0.4]
        vo = 2
        x_fit = time
        maxi = np.argmax(force)
        piezo_app = s0_velocity * x_fit[:maxi+1]
        piezo_back = 2 * vo * x_fit[maxi] - vo * x_fit[maxi+1:]
        y_fit = np.append(piezo_app,piezo_back)
        if from_mat:
            # load data for test
            f = np.loadtxt('../sandbox/matlab/aj_new_fits/sphere/test_f.txt',delimiter = ',')

            startpar = {'T': 0.0003, 'Ka': 0.01, 'beta': 0.6}
            constants = {'r1' : r1,  'phi' : phi}
            s = SimFit.SimFit(xvals = y_fit,yvals = f,
                              func = discher_final_function_visco,
                              startpar = startpar,
                              indep = 'z',
                              constant = constants,
                              #minpar = {'T' : 0, 'Ka' : 0, 'beta' : 0},
                              #maxpar = {'beta' : 1},
                              #tol = [1e-4, 1e-4],
                              cntrl = 1,
                              maxiter =500)
            s.fittype = 'levenberg'
            s()
            parameter = s.curr_par
            fit_force = plot_fit(x_fit,
                                 y_fit,
                                 f,
                                 parameter,
                                 fname,
                                 f,
                                 time,
                                 height,
                                 contact_point,
                                 'time',
                                 True,
                                 False)
            print(parameter)
            f3 = discher_final_function_visco(y_fit,para[0],para[1],para[2],r1,phi)
        else:
            f = discher_final_function_visco(y_fit,para[0],para[1],para[2],r1,phi)
            from scipy.io import savemat
            file_name = '../sandbox/matlab/aj_new_fits/sphere/test_data.mat'
            mat_dic = {'time':time,'z':y_fit,'force':f,'vo':vo}
            savemat(file_name,mat_dic)
            m_fit = np.loadtxt('../sandbox/matlab/aj_new_fits/sphere/test_for.txt',delimiter = ',')
        plt.ion()
