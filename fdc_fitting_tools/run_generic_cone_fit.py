import numpy as np

from generic_cone_fit import run_processes,run_processes_no_multi

import warnings
warnings.filterwarnings("ignore")


paths = []
#paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110310/map-data-2011.03.10-11.16.05/eval_2020_02_04_20_00')
paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110310/map-data-2011.03.10-12.56.03/eval_2020_03_23_11_23')
paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110310/map-data-2011.03.10-14.32.08/eval_2020_03_26_14_30')
paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110901/map-data-2011.09.01-12.29.56/eval_2020_10_21_15_16')
paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110901/map-data-2011.09.01-13.41.29/eval_2020_10_21_16_14')
#paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110908/konfluent/map-data-2011.09.08-11.50.48/eval_2020_10_20_14_03')
#paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110908/konfluent/map-data-2011.09.08-12.41.16/eval_2020_10_21_11_44')
paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110908/konfluent/map-data-2011.09.08-13.01.08/eval_2020_10_21_13_07')
paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110908/konfluent/map-data-2011.09.08-13.34.34/eval_2020_10_21_14_34')

#paths = []
#paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110901/map-data-2011.09.01-12.29.56/eval_2020_10_21_15_16')

paths = []
#paths.append('../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 99/Zelle 2/map-data-2017.06.21-17.18.54.512/eval_2021_10_25_23_57')
#paths.append('../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 78/Zelle 1/map-data-2017.01.20-13.51.50.222/eval_2021_10_25_14_26/')
#paths.append('../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG033/Stelle2/map-data-2016.06.06-14.44.16.386/eval_2021_10_26_09_53')

laptop = False
if laptop:
    paths = [
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG033/Stelle2/map-data-2016.06.06-14.44.16.386/eval_2021_10_26_09_53',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG033/Stelle3/map-data-2016.06.06-15.28.26.664/eval_2021_11_06_17_05',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG033/Stelle4/map-data-2016.06.06-16.16.57.120/eval_2021_11_06_17_10',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG033/Stelle5/map-data-2016.06.06-16.54.51.944/eval_2021_11_06_17_12',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG033/Stelle6/map-data-2016.06.06-17.43.55.035/eval_2021_11_06_17_32',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG035/Stelle1/map-data-2016.06.10-13.40.41.881/eval_2021_11_06_17_33',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG035/Stelle2/map-data-2016.06.10-14.39.14.663/eval_2021_11_06_17_34',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG035/Stelle3/map-data-2016.06.10-15.26.24.465/eval_2021_11_06_17_36',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG035/Stelle4/map-data-2016.06.10-16.23.19.962/eval_2021_11_06_17_38',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG035/Stelle5/map-data-2016.06.10-17.14.35.755/eval_2021_11_06_17_38',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG036/Stelle1/map-data-2016.06.13-13.36.21.802/eval_2021_11_06_17_40',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG036/Stelle2/map-data-2016.06.13-14.10.33.638/eval_2021_11_06_17_40',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG036/Stelle3/map-data-2016.06.13-14.54.45.172/eval_2021_11_06_17_42',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG036/Stelle4/map-data-2016.06.13-15.36.24.332/eval_2021_11_06_17_44',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG044/Stelle1/map-data-2016.08.08-13.09.44.191/eval_2021_11_06_17_45',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG044/Stelle3/map-data-2016.08.08-15.19.59.750/eval_2021_11_06_17_45',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG045/Stelle2/map-data-2016.08.10-14.27.42.408/eval_2021_11_06_17_46',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG045/Stelle3/map-data-2016.08.10-15.50.53.830/eval_2021_11_06_17_48',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG045/Stelle4/map-data-2016.08.10-17.10.04.245/eval_2021_11_14_18_46',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 78/Zelle 1/map-data-2017.01.20-13.51.50.222/eval_2021_11_14_18_46', ## 2 of them <<
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 78/Zelle 2/map-data-2017.01.20-15.00.47.927/eval_2021_11_14_18_51',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 78/Zelle 3/map-data-2017.01.20-15.41.26.031/eval_2021_11_14_18_54',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 78/Zelle 4/map-data-2017.01.20-16.29.52.770/eval_2021_11_14_18_55',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 78/Zelle 5/map-data-2017.01.20-17.06.00.528/eval_2021_11_14_19_01',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 78/Zelle 6/map-data-2017.01.20-17.48.38.664/eval_2021_11_14_19_02',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 78/Zelle 7/map-data-2017.01.20-18.28.21.226-1/eval_2021_11_15_17_48',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 80/Zelle 5/map-data-2017.01.24-15.26.42.492/eval_2021_11_15_15_27',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 84/Zelle 4/map-data-2017.04.05-17.25.56.511/eval_2021_11_15_15_24',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 84/Zelle 5/map-data-2017.04.05-18.06.01.062/eval_2021_11_15_15_23',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 86/Zelle 1/map-data-2017.04.21-16.04.56.103/eval_2021_11_15_15_35',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 86/Zelle 2/map-data-2017.04.21-17.04.47.401/eval_2021_11_15_15_20',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 86/Zelle 4/map-data-2017.04.21-18.32.10.172/eval_2021_11_14_22_41',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 87/Zelle 1/map-data-2017.04.25-15.47.30.972/eval_2021_11_14_22_40',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 87/Zelle 2/map-data-2017.04.25-16.39.16.601/eval_2021_11_14_22_39',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 87/Zelle 5/map-data-2017.04.25-18.50.47.069/eval_2021_11_14_22_36',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 87/Zelle 6/map-data-2017.04.25-19.27.59.208/eval_2021_11_14_22_35',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 1/map-data-2017.04.28-11.38.50.202/eval_2021_11_14_22_31',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 2/map-data-2017.04.28-12.19.05.729/eval_2021_11_14_22_30',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 4/map-data-2017.04.28-13.48.34.238/eval_2021_11_14_22_29',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 5/map-data-2017.04.28-14.17.57.040/eval_2021_11_14_22_15',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 6/map-data-2017.04.28-15.09.20.187/eval_2021_11_14_22_13',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 7/map-data-2017.04.28-15.52.24.029/eval_2021_11_14_22_10',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 9/map-data-2017.04.28-17.20.39.115/eval_2021_11_14_22_09',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 10/map-data-2017.04.28-17.57.05.759/eval_2021_11_14_22_08',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 99/Zelle 2/map-data-2017.06.21-17.18.54.512/eval_2021_10_25_23_57',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 99/Zelle 3/map-data-2017.06.21-17.55.23.561/eval_2021_11_14_22_01',
        #'../../../../../../media/jbodes/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 99/Zelle 4/map-data-2017.06.21-18.54.33.992/eval_2021_11_14_21_55'
    ]
else:
    paths = [
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG033/Stelle2/map-data-2016.06.06-14.44.16.386/eval_2021_10_26_09_53',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG033/Stelle3/map-data-2016.06.06-15.28.26.664/eval_2021_11_06_17_05',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG033/Stelle4/map-data-2016.06.06-16.16.57.120/eval_2021_11_06_17_10',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG033/Stelle5/map-data-2016.06.06-16.54.51.944/eval_2021_11_06_17_12',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG033/Stelle6/map-data-2016.06.06-17.43.55.035/eval_2021_11_06_17_32',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG035/Stelle1/map-data-2016.06.10-13.40.41.881/eval_2021_11_06_17_33',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG035/Stelle2/map-data-2016.06.10-14.39.14.663/eval_2021_11_06_17_34',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG035/Stelle3/map-data-2016.06.10-15.26.24.465/eval_2021_11_06_17_36',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG035/Stelle4/map-data-2016.06.10-16.23.19.962/eval_2021_11_06_17_38',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG035/Stelle5/map-data-2016.06.10-17.14.35.755/eval_2021_11_06_17_38',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG036/Stelle1/map-data-2016.06.13-13.36.21.802/eval_2021_11_06_17_40',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG036/Stelle2/map-data-2016.06.13-14.10.33.638/eval_2021_11_06_17_40',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG036/Stelle3/map-data-2016.06.13-14.54.45.172/eval_2021_11_06_17_42',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG036/Stelle4/map-data-2016.06.13-15.36.24.332/eval_2021_11_06_17_44',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG044/Stelle1/map-data-2016.08.08-13.09.44.191/eval_2021_11_06_17_45',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG044/Stelle3/map-data-2016.08.08-15.19.59.750/eval_2021_11_06_17_45',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG045/Stelle2/map-data-2016.08.10-14.27.42.408/eval_2021_11_06_17_46',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG045/Stelle3/map-data-2016.08.10-15.50.53.830/eval_2021_11_06_17_48',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/AG045/Stelle4/map-data-2016.08.10-17.10.04.245/eval_2021_11_14_18_46',
        #'../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 78/Zelle 1/map-data-2017.01.20-13.51.50.222/eval_2021_11_14_18_46', ## 2 of them
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 78/Zelle 2/map-data-2017.01.20-15.00.47.927/eval_2021_11_14_18_51',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 78/Zelle 3/map-data-2017.01.20-15.41.26.031/eval_2021_11_14_18_54',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 78/Zelle 4/map-data-2017.01.20-16.29.52.770/eval_2021_11_14_18_55',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 78/Zelle 5/map-data-2017.01.20-17.06.00.528/eval_2021_11_14_19_01',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 78/Zelle 6/map-data-2017.01.20-17.48.38.664/eval_2021_11_14_19_02',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 78/Zelle 7/map-data-2017.01.20-18.28.21.226-1/eval_2021_11_15_17_48',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 80/Zelle 5/map-data-2017.01.24-15.26.42.492/eval_2021_11_15_15_27',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 84/Zelle 4/map-data-2017.04.05-17.25.56.511/eval_2021_11_15_15_24',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 84/Zelle 5/map-data-2017.04.05-18.06.01.062/eval_2021_11_15_15_23',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 86/Zelle 1/map-data-2017.04.21-16.04.56.103/eval_2021_11_15_15_35',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 86/Zelle 2/map-data-2017.04.21-17.04.47.401/eval_2021_11_15_15_20',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 86/Zelle 4/map-data-2017.04.21-18.32.10.172/eval_2021_11_14_22_41',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 87/Zelle 1/map-data-2017.04.25-15.47.30.972/eval_2021_11_14_22_40',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 87/Zelle 2/map-data-2017.04.25-16.39.16.601/eval_2021_11_14_22_39',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 87/Zelle 5/map-data-2017.04.25-18.50.47.069/eval_2021_11_14_22_36',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 87/Zelle 6/map-data-2017.04.25-19.27.59.208/eval_2021_11_14_22_35',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 1/map-data-2017.04.28-11.38.50.202/eval_2021_11_14_22_31',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 2/map-data-2017.04.28-12.19.05.729/eval_2021_11_14_22_30',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 4/map-data-2017.04.28-13.48.34.238/eval_2021_11_14_22_29',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 5/map-data-2017.04.28-14.17.57.040/eval_2021_11_14_22_15',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 6/map-data-2017.04.28-15.09.20.187/eval_2021_11_14_22_13',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 7/map-data-2017.04.28-15.52.24.029/eval_2021_11_14_22_10',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 9/map-data-2017.04.28-17.20.39.115/eval_2021_11_14_22_09',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 88/Zelle 10/map-data-2017.04.28-17.57.05.759/eval_2021_11_14_22_08',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 99/Zelle 2/map-data-2017.06.21-17.18.54.512/eval_2021_10_25_23_57',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 99/Zelle 3/map-data-2017.06.21-17.55.23.561/eval_2021_11_14_22_01',
        '../../../../../../media/jbodens/Max/data_for_evaluation/stefan_nehls/cell_patterning/Exp 99/Zelle 4/map-data-2017.06.21-18.54.33.992/eval_2021_11_14_21_55'
    ]


#### settings

# Dwell or no Dwell?
dwell = False

# either 'from file' or 'not from file'
fit_range_type = 'not from file'

# percentage of the maximal force kept for the fitting
value = 0.4

# some choices
fit_from_choices = {'all curves' : 0,
                    'fit output list': 1,
                    'cell wise': 2,
                    'from index list' : 3}
# choose it
fit_from = fit_from_choices['all curves']

# choose where the parameters come from file or not
para_from_file = True

# choose in the input parameters are used
para_from_input = False

# adjust parameters for cell stretcher
adjust_para_CS = False

# parameters describing the shape of the cell cap
input_parameters = {'angle' : 35,
                    'radius' : 12,
                    'theta_cone' : 17.5}

# number of cores to be used for multiprocessing
cores = 13

# use Solgov window
sogolay = False

# Solgov window
sogolay_window = 107

# data reduction factor
data_reduction_factor = 10000

##### run it

multi_on = True
if multi_on:
    run_processes(paths,cores,
                  fit_range_type,
                  fit_from,
                  input_parameters,
                  sogolay,
                  sogolay_window,
                  data_reduction_factor,
                  para_from_file,
                  para_from_input,
                  adjust_para_CS,
                  dwell,
                  value)
else:
    run_processes_no_multi(paths,cores,
                           fit_range_type,
                           fit_from,
                           input_parameters,
                           sogolay,
                           sogolay_window,
                           data_reduction_factor,
                           para_from_file,
                           para_from_input,
                           adjust_para_CS,
                           dwell,
                           value)

