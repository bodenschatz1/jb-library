import numpy as np

from generic_fit import run_processes,run_processes_no_multi



paths = []
#paths.append('../../../../ownCloud/texts/stretcher_paper/eval/setpoint_dependence/data_jannis/200902/sample2/all_data_dwell/eval_2020_09_04_12_22')
#paths.append('../../../../ownCloud/texts/stretcher_paper/eval/setpoint_dependence/data_jannis/200902/sample2/all_data_loop/eval_2020_09_04_12_49')

# older data only 10 mm strain
paths.append('../../../../ownCloud/strain_measurements/191004/device1/eval_2019_10_06_16_45')
paths.append('../../../../ownCloud/strain_measurements/191004/device1/eval_2019_10_06_16_45/strained')
paths.append('../../../../ownCloud/strain_measurements/191009/device1/eval_2019_11_06_17_12')
paths.append('../../../../ownCloud/strain_measurements/191009/device1/eval_2019_11_06_17_12/strain')
paths.append('../../../../ownCloud/strain_measurements/191010/device1/eval_2019_11_06_17_45')
paths.append('../../../../ownCloud/strain_measurements/191010/device1/eval_2019_11_06_17_45/strain')
paths.append('../../../../ownCloud/strain_measurements/191010/device2/eval_2019_11_06_17_55')
paths.append('../../../../ownCloud/strain_measurements/191010/device2/eval_2019_11_06_17_55/strain')
paths.append('../../../../ownCloud/strain_measurements/191016/device2/eval_2019_11_05_10_28')
paths.append('../../../../ownCloud/strain_measurements/191016/device2/eval_2019_11_05_10_28/strain')
paths.append('../../../../ownCloud/strain_measurements/191018/device1/eval_2019_11_06_12_19')
paths.append('../../../../ownCloud/strain_measurements/191018/device1/eval_2019_11_06_12_19/strain')
# new measurements only 10mm strain
paths.append('../../../../ownCloud/strain_measurements/200704/device1/eval_2020_07_21_14_41')
paths.append('../../../../ownCloud/strain_measurements/200704/device1/eval_2020_07_21_14_41/strained')
paths.append('../../../../ownCloud/strain_measurements/200704/device2/eval_2020_07_21_15_14')
paths.append('../../../../ownCloud/strain_measurements/200704/device3/eval_2020_07_21_15_45')
paths.append('../../../../ownCloud/strain_measurements/200704/device3/eval_2020_07_21_15_45/strained')
paths.append('../../../../ownCloud/strain_measurements/200801/device1/eval_2020_08_17_21_23')
paths.append('../../../../ownCloud/strain_measurements/200801/device2/eval_2020_08_17_23_02')
paths.append('../../../../ownCloud/strain_measurements/200801/device2/eval_2020_08_17_23_02/strain')
paths.append('../../../../ownCloud/strain_measurements/200802/device1/eval_2020_08_18_00_07')
paths.append('../../../../ownCloud/strain_measurements/200802/device1/eval_2020_08_18_00_07/strain')
paths.append('../../../../ownCloud/strain_measurements/200802/device2/eval_2020_08_18_00_28')
paths.append('../../../../ownCloud/strain_measurements/200802/device2/eval_2020_08_18_00_28/strain')
# newe  10mm
paths.append('../../../../ownCloud/strain_measurements/210204/device1/eval_2021_02_04_16_53')
paths.append('../../../../ownCloud/strain_measurements/210204/device1/eval_2021_02_04_16_53/strain')
paths.append('../../../../ownCloud/strain_measurements/210204/device2/eval_2021_02_08_17_20')
paths.append('../../../../ownCloud/strain_measurements/210204/device2/eval_2021_02_08_17_20/strain')
paths.append('../../../../ownCloud/strain_measurements/210204/device2m2/eval')
paths.append('../../../../ownCloud/strain_measurements/210204/device2m2/eval/strain')
paths.append('../../../../ownCloud/strain_measurements/210206/device1/eval_2021_02_09_10_13')
paths.append('../../../../ownCloud/strain_measurements/210206/device1/eval_2021_02_09_10_13/strain')
paths.append('../../../../ownCloud/strain_measurements/210206/device1m2/eval_2021_02_09_10_45')
paths.append('../../../../ownCloud/strain_measurements/210206/device1m2/eval_2021_02_09_10_45/strain')
paths.append('../../../../ownCloud/strain_measurements/210206/device2/eval_2021_02_09_11_18')
paths.append('../../../../ownCloud/strain_measurements/210206/device2/eval_2021_02_09_11_18/strain')
paths.append('../../../../ownCloud/strain_measurements/210207/device1/eval_2021_02_09_11_40')
paths.append('../../../../ownCloud/strain_measurements/210207/device1/eval_2021_02_09_11_40/strain')
paths.append('../../../../ownCloud/strain_measurements/210207/device2/eval_2021_02_09_11_50')
paths.append('../../../../ownCloud/strain_measurements/210207/device2/eval_2021_02_09_11_50/strain')
#paths.append('../../../../ownCloud/strain_measurements')
# data from Karim
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200614/KA045/eval_2020_06_24_09_51')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200613/KA043/eval_2020_10_27_12_06')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200613/KA043/eval_2020_10_27_12_06/strained')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200613/KA044/eval_2020_06_24_10_10')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200612/KA041/eval_2020_06_24_10_28')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200612/KA042/eval_2020_06_24_10_22')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200611/KA039/eval_2020_06_24_10_47')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200611/KA040/eval_2020_06_24_10_35')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200611/KA040/eval_2020_06_24_10_35/strained')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200610/KA037/eval_2020_10_27_16_17')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200610/KA037/eval_2020_10_27_16_17/strained')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200610/KA038/eval_2020_06_24_10_50')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200610/KA038/eval_2020_06_24_10_50/strained')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200510/KA033/eval_2020_06_24_11_26')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200510/KA033/eval_2020_06_24_11_26/strained')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200510/KA034/eval_2020_06_24_11_19')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200509/KA032/eval_2020_06_24_11_29')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200509/KA032/eval_2020_06_24_11_29/strained')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200508/KA029/eval_2020_06_24_11_35')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200508/KA029/eval_2020_06_24_11_35/strained')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200508/KA030/eval_2020_06_24_11_33')
#paths.append('../../../../ownCloud/strain_measurements/karim/BA_data')
#paths.append('../../../../ownCloud/strain_measurements/karim/BA_data')
#paths.append('../../../../ownCloud/strain_measurements/karim/BA_data')



### 5 mm
paths = []
paths.append('../../../../ownCloud/strain_measurements/200729/device1/eval_2020_08_17_16_31')
paths.append('../../../../ownCloud/strain_measurements/200729/device1/eval_2020_08_17_16_31/strain')
paths.append('../../../../ownCloud/strain_measurements/200730/device2/eval_2020_08_17_17_06')
paths.append('../../../../ownCloud/strain_measurements/200730/device2/eval_2020_08_17_17_06/strain')
paths.append('../../../../ownCloud/strain_measurements/210203/device1/eval_2021_02_03_18_31')
paths.append('../../../../ownCloud/strain_measurements/210203/device1/eval_2021_02_03_18_31/strain')
paths.append('../../../../ownCloud/strain_measurements/210205/device1/eval_2021_02_08_17_59')
paths.append('../../../../ownCloud/strain_measurements/210205/device1/eval_2021_02_08_17_59/strain')
paths.append('../../../../ownCloud/strain_measurements/210205/device1m2/eval_2021_02_08_18_08')
paths.append('../../../../ownCloud/strain_measurements/210205/device1m2/eval_2021_02_08_18_08/strain')
paths.append('../../../../ownCloud/strain_measurements/210205/device2/eval_2021_02_09_18_20')
paths.append('../../../../ownCloud/strain_measurements/210205/device2/eval_2021_02_09_18_20/strain')

#### 15 mm
paths = []
paths.append('../../../../ownCloud/strain_measurements/200731/device1/eval_2020_08_17_17_38')
paths.append('../../../../ownCloud/strain_measurements/200731/device1/eval_2020_08_17_17_38/strain')

####new
#paths = []
#paths.append('../../../../ownCloud/strain_measurements/210122/device2/eval_2021_01_27_16_49')
#paths.append('../../../../ownCloud/strain_measurements/210122/device2/eval_2021_01_27_16_49/strain')







"""
### test stuff cp stuff
paths = []
#paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200613/KA043/eval_2020_10_27_12_06')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200613/KA043/eval_2020_10_27_12_06/strained')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200610/KA037/eval_2020_10_27_16_17')
paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200610/KA037/eval_2020_10_27_16_17/strained')
"""
#paths = []
#paths.append('../../../../ownCloud/strain_measurements/191010/device2/eval_2019_11_06_17_55/strain')
####### setpoint stuff
#paths = []
#paths.append('../../../../ownCloud/texts/stretcher_paper/eval/setpoint_dependence/data_jannis/200902/sample2/all_data_dwell/eval_2020_09_04_12_22')
#paths = []
#paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200510/KA033/eval_2020_10_28_12_05')
#paths.append('../../../../ownCloud/strain_measurements/karim/BA_data/200510/KA033/eval_2020_10_28_12_05/strained')
#### settings

# Dwell or no Dwell?
dwell = False

# either 'from file' or 'not from file'
fit_range_type = 'not from file'

# percentage of the maximal force kept for the fitting
value = 0.3

# some choices
fit_from_choices = {'all curves' : 0,
                    'fit output list': 1,
                    'cell wise': 2}
# choose it
fit_from = fit_from_choices['all curves']

# choose where the parameters come from
para_from_file = False

# number of cores
cores = 4

# use Solgov window
sogolay = False

# Solgov window
sogolay_window = 107

# data reduction factor
data_reduction_factor = 1000

theo_strain = 15

##### run it
run_processes(paths,cores,
              fit_range_type,
              fit_from,
              sogolay,
              sogolay_window,
              data_reduction_factor,
              para_from_file,
              dwell,
              value,
              theo_strain)

