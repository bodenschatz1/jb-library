#! python

##################################################################
#
# SimFit: Performs a Downhill-Simplex / Levenberg-Marquardt fit of
#         a data set
#
# o Based on the scipy.optimize.fmin Simplex Fit, found in
#   /usr/lib/python2.7/dist-packages/scipy/optimize/optimize.py
#   But contains a modified copy of this source code in the 
#   function SimFit._minimize_neldermead(...).
#
# bg 12.10.2014 : concepts of class SimFit
##################################################################

import numpy
import scipy.optimize

import matplotlib as mpl
#mpl.use('TkAgg') # I do not know why..
# import matplotlib.pylab as plt
# plt.ion()

class SimFit(object):

    def __init__(self, xvals=[], yvals=[], func=None, startpar={}, indep='x',
                 minpar={}, maxpar={}, yweight=None, constant={}, maxiter=100,
                 tol=[1e-6, 1e-6], callback=None, cntrl=-1):
        self.xvals    = xvals
        self.yvals    = yvals
        self.func     = func
        self.startpar = startpar.copy()
        self.indep    = indep
        self.minpar   = minpar.copy()
        self.maxpar   = maxpar.copy()
        self.yweight  = yweight
        self.constant = constant.copy()
        self.maxiter  = maxiter
        self.tol      = tol # tuple with (parameter-tolerance, chi^2-tolerance)
        if callback == None:
            self.callback = SimFitStandardCallBack
        self.cntrl    = cntrl
        self.convhist = []
        self.fittype  = 'simplex' # use one of 'simplex','levenberg','both'

        # some private member data

        self.curr_iterations = 0
        self.curr_par        = []
        self.curr_chisqr     = numpy.inf
        self.curr_ytheovals  = []
        

        self._FlatParValue   = [] # a flat list with the values of the 
                                  # the fit parameters
        self._FlatParNames   = [] # a flat list with the names of the
                                  # fit parameters (in the same order as
                                  # _FlatParValues!)
        self._FlatMaxPar     = [] # dto., list of upper parameter boundaries
        self._FlatMinPar     = [] # dto., list of lower parameter boundaries

        self._FuncCalls      = 0  # counts the number of function evaluations

        # the following member data is filled after the optimization
        # (instead of return values. use 'print <SimFit-Object> to inspect)

        self.optpar_Simplex   = None
        self.optpar_Levenberg = None
        self.par_stddev       = None
        self.optfunc_Simplex  = None
        self.ret_criterion    = None
        
        
        
    def __call__(self,verbose=False, xvals=None, yvals=None, 
                 func=None, startpar=None, indep=None, 
                 minpar=None, maxpar=None, yweight=None, constant=None, 
                 maxiter=None, tol=None, callback=None, cntrl=None):

        if verbose:
            print('----------------------+------------------------')
            print('SimFit: Simplex- and/or Levenberg-Marquardt Fit')
            print()
            # get function name and argument list by python's 
            # introspection mechanisms
            funcdef = self.func.__name__
            funcdef = funcdef + '('
            for argname in self.func.__code__.co_varnames[:self.func.__code__.co_argcount]:
                funcdef = funcdef + argname +', '
            funcdef = funcdef[:-2] + ')'
            print('Function:', funcdef)

        #---------------------------------------------------------------
        # Check for last changes in the SimFit-parameters, given here as 
        # arguments to the __call__() function
        #---------------------------------------------------------------

        rerun_PrepareFit = False

        if xvals != None:
            self.xvals = xvals
            rerun_PrepareFit = True

        if yvals != None:
            self.yvals = yvals
            rerun_PrepareFit = True

        if func != None:
            self.func = func
            rerun_PrepareFit = True

        if startpar != None:
            self.startpar = startpar.copy()
            rerun_PrepareFit = True

        if indep != None:
            self.indep = indep
            rerun_PrepareFit = True

        if minpar != None:
            self.minpar = minpar.copy()
            rerun_PrepareFit = True

        if maxpar != None:
            self.maxpar = maxpar.copy()
            rerun_PrepareFit = True
            
        if yweight != None:
            self.yweight = yweight
            rerun_PrepareFit = True

        if constant != None:
            self.constant = constant.copy()
            rerun_PrepareFit = True
             
        if maxiter != None:
            self.maxiter = maxiter
            rerun_PrepareFit = True

        if tol != None:
            self.tol = tol
            rerun_PrepareFit = True

        if callback != None:
            self.callback = callback
            rerun_PrepareFit = True
                        
        if cntrl != None:
            self.cntrl = cntrl
            rerun_PrepareFit = True

        if self._PrepareFit() != 0:
            return

        self._minimize_neldermead()
                                  
        if self.fittype != 'simplex':
            self._minimize_levenberg_marquardt(self.curr_par)

        if verbose:
            print()
            print('Performed Simplex Iterations ..:', self.curr_iterations)
            print('Final Chi-square ..............:', self.curr_chisqr)
            print('Convergence Criterion:')
            for line in self.ret_criterion.split('\n'):
                print('  -', line)
            print()
            print('Optimized Parameters:')
            
            cpar = list(zip(self._FlatParNames, self.curr_par))
            if self.fittype != 'simplex':
                cstddev = dict(zip(self._FlatParNames, self.par_stddev))
            pardict = dict(cpar + list(self.constant.items()))

            for k in pardict:
                print('  ', k, '.'*(27-len(k))+':', pardict[k], end='')
                if k in self.constant:
                    print('(const)')
                else:
                    if self.fittype != 'simplex':
                        print('+-', cstddev[k])
                    else:
                        print('+- (?)')


    def _PrepareFit(self):
        #---------------------------------
        # Perform some consistancy checks:
        #---------------------------------
        have_errors = False
        if len(self.xvals) == 0:
            print('   Error: missing an array with x-values')
            have_errors = True
        if len(self.yvals) == 0:
            print('   Error: missing an array with y-values')
            have_errors = True
        if len(self.xvals) != len(self.yvals):
            print('   Error: mismatch in size of x- and y-values')
            have_errors = True
        if self.func == None:
            print('   Error: missing a fit function')
            have_errors = True
        if len(self.startpar) == 0:
            print('Error: missing the dictionary with start parameters')
            have_errors = True

        if have_errors:
            return -1

        #---------------------------
        # Prepare the internal data:
        #---------------------------

        # flatten the fitparameter dictionary into two lists (with the
        # same order of elements!): one contains the names, the other 
        # contains the corresponding values:
        # {'a':1, 'b':2, 'c':3} -> ['a','b','c'] and [1, 2, 3]
        #
        # o this is necessary because the _minimize_neldermead(...) 
        #   works with parameter-arrays and would hardly accept 
        #   dictionaries.
        # o the _chi_square function re-assembles a dictionary from
        #   these two lists

        self._FlatParNames = []
        self._FlatParValue = []
        self._FlatMaxPar   = []
        self._FlatMinPar   = []

        for e in self.startpar.items():
            self._FlatParNames.append(e[0])
            self._FlatParValue.append(e[1])
            if e[0] in self.maxpar:#.has_key(e[0]):
                self._FlatMaxPar.append(self.maxpar[e[0]])
            else:
                self._FlatMaxPar.append(numpy.inf)
            if e[0] in self.minpar:#.has_key(e[0]):
                self._FlatMinPar.append(self.minpar[e[0]])
            else:
                self._FlatMinPar.append(-numpy.inf)

        # convert the fit parameter values into a numpy array and
        # force a deep copy:

        self.curr_par = numpy.array(self._FlatParValue).copy() 

        if self.yweight == None:
            self.yweight = numpy.ones_like(self.yvals)

        return 0


    def _chi_function(self, current_parameters):

        # re-assemble a parameter dictionary from the self._FlatParNames
        # array and the current parameters. Recombine this dictionary 
        # with the independent variable and the dictionary containing
        # the constant parameters:

        cpar = list(zip(self._FlatParNames, current_parameters))

        pardict = dict(list({self.indep:self.xvals}.items()) +
                        cpar +
                        list(self.constant.items())
                   )

        self.curr_ytheovals = self.func(**pardict)
        delta_y = self.yweight*(self.yvals-self.curr_ytheovals);
        self._FuncCalls += 1
        return delta_y

    def _chi_square(self, current_parameters):

        delta_y = self._chi_function(current_parameters)
        return numpy.sum(delta_y**2)


    def _minimize_neldermead(self):
        """
        Minimization of scalar function of one or more variables 
        using the Nelder-Mead algorithm.

        Options for the Nelder-Mead algorithm are:
            disp : bool
                Set to True to print convergence messages.
            xtol : float
                Relative error in solution `xopt` acceptable for 
                convergence.
            ftol : float
                Relative error in ``fun(xopt)`` acceptable for 
                convergence.
            maxiter : int
                Maximum number of iterations to perform.
            maxfev : int
                Maximum number of function evaluations to make.

        """

        x0 = numpy.asfarray(self.curr_par).flatten()
        N = len(x0)
        rank = len(x0.shape)
        if not -1 < rank < 2:
            raise ValueError("Initial guess must be a scalar or rank-1 sequence.")


        rho = 1
        chi = 2
        psi = 0.5
        sigma = 0.5
        one2np1 = list(range(1, N + 1))

        if rank == 0:
            sim = numpy.zeros((N + 1,), dtype=x0.dtype)
        else:
            sim = numpy.zeros((N + 1, N), dtype=x0.dtype)
        fsim = numpy.zeros((N + 1,), float)
        sim[0] = x0
        self.curr_iterations = 0


        self.convhist.append([self.curr_iterations, self.curr_chisqr, sim[0]])
            
        fsim[0] = self._chi_square(x0)
        nonzdelt = 0.05
        zdelt = 0.00025
        for k in range(0, N):
            y = numpy.array(x0, copy=True)
            if y[k] != 0:
                y[k] = (1 + nonzdelt)*y[k]
            else:
                y[k] = zdelt

            y = numpy.minimum(self._FlatMaxPar, y)
            y = numpy.maximum(self._FlatMinPar, y)

            sim[k + 1] = y
            f = self._chi_square(y)
            fsim[k + 1] = f

        ind = numpy.argsort(fsim)
        fsim = numpy.take(fsim, ind, 0)
        # sort so sim[0,:] has the lowest function value
        sim = numpy.take(sim, ind, 0)

        # self.curr_iterations = 1

        while (self.curr_iterations < self.maxiter):
            if (numpy.max(numpy.ravel(numpy.abs(sim[1:] - sim[0]))) <= self.tol[0] and
                    numpy.max(numpy.abs(fsim[0] - fsim[1:])) <= self.tol[1]):
                break

            xbar = numpy.add.reduce(sim[:-1], 0) / N
            xr = (1 + rho) * xbar - rho * sim[-1]
            xr = numpy.minimum(self._FlatMaxPar, xr)
            xr = numpy.maximum(self._FlatMinPar, xr)

            fxr = self._chi_square(xr)
            doshrink = 0

            if fxr < fsim[0]:
                xe = (1 + rho * chi) * xbar - rho * chi * sim[-1]
                xe = numpy.minimum(self._FlatMaxPar, xe)
                xe = numpy.maximum(self._FlatMinPar, xe)

                fxe = self._chi_square(xe)

                if fxe < fxr:
                    sim[-1] = xe
                    fsim[-1] = fxe
                else:
                    sim[-1] = xr
                    fsim[-1] = fxr
            else:  # fsim[0] <= fxr
                if fxr < fsim[-2]:
                    sim[-1] = xr
                    fsim[-1] = fxr
                else:  # fxr >= fsim[-2]
                    # Perform contraction
                    if fxr < fsim[-1]:
                        xc = (1 + psi * rho) * xbar - psi * rho * sim[-1]
                        xc = numpy.minimum(self._FlatMaxPar, xc)
                        xc = numpy.maximum(self._FlatMinPar, xc)

                        fxc = self._chi_square(xc)

                        if fxc <= fxr:
                            sim[-1] = xc
                            fsim[-1] = fxc
                        else:
                            doshrink = 1
                    else:
                        # Perform an inside contraction
                        xcc = (1 - psi) * xbar + psi * sim[-1]
                        xcc = numpy.minimum(self._FlatMaxPar, xcc)
                        xcc = numpy.maximum(self._FlatMinPar, xcc)

                        fxcc = self._chi_square(xcc)

                        if fxcc < fsim[-1]:
                            sim[-1] = xcc
                            fsim[-1] = fxcc
                        else:
                            doshrink = 1

                    if doshrink:
                        for j in one2np1:
                            sim[j] = sim[0] + sigma * (sim[j] - sim[0])
                            sim[j] = numpy.minimum(self._FlatMaxPar, sim[j])
                            sim[j] = numpy.maximum(self._FlatMinPar, sim[j])

                            fsim[j] = self._chi_square(sim[j])

            ind = numpy.argsort(fsim)
            sim = numpy.take(sim, ind, 0)
            fsim = numpy.take(fsim, ind, 0)
            
            self.curr_par = sim[0]
            self.curr_chisqr = numpy.min(fsim)
            self.curr_iterations += 1
            self.convhist.append([self.curr_iterations, self.curr_chisqr, sim[0]])
            
            if self.cntrl > 0 and (self.curr_iterations % self.cntrl) == 0:
                self.callback(self)
            
        self.optpar_Simplex = sim[0]
        self.optfunc_Simplex = numpy.min(fsim)
        
        self.ret_criterion = 'Simplex: '

        if (numpy.max(numpy.ravel(numpy.abs(sim[1:] - sim[0]))) <= self.tol[0]):
            self.ret_criterion += 'Reached tolerance level in parameter values. '

        if (numpy.max(numpy.abs(fsim[0] - fsim[1:])) <= self.tol[1]):
            self.ret_criterion += 'Reached tolerance level in chi-square values. '

        if self.curr_iterations >= self.maxiter:
            self.ret_criterion += 'Reached maximum number of iterations. '



    def _minimize_levenberg_marquardt(self, p0=None):
        """
        Use non-linear least squares to fit a function, f, to data.

        Parameters
        ----------
        p0 : None, scalar, or N-length sequence
            Initial guess for the parameters. If None, then the initial
            values will all be 1 (if the number of parameters for the function
            can be determined using introspection, otherwise a ValueError
            is raised).

        Returns
        -------
        popt : array
            Optimal values for the parameters so that the sum of the squared error
            of ``f(xdata, *popt) - ydata`` is minimized
        pcov : 2d array
            The estimated covariance of popt. The diagonals provide the variance
            of the parameter estimate. To compute one standard deviation errors
            on the parameters use ``perr = np.sqrt(np.diag(pcov))``.

        Notes
        -----
        The algorithm uses the Levenberg-Marquardt algorithm through `leastsq`.
        Additional keyword arguments are passed directly to that algorithm.

        This code is take (copy, paste and modify) from scipy's curve_fit 
        function source code. bg. 11/2014.

        Examples
        --------
        >>> import numpy as np
        >>>
        >>> def func(x, a, b, c):
        ...     return a * np.exp(-b * x) + c
        >>> xdata = np.linspace(0, 4, 50)
        >>> y = func(xdata, 2.5, 1.3, 0.5)
        >>> ydata = y + 0.2 * np.random.normal(size=len(xdata))

        >>> popt, pcov = _minimize_levenberg_marquardt(func, xdata, ydata)

        """

        res = scipy.optimize.leastsq(self._chi_function, p0, full_output=1, maxfev=self.maxiter)
        (popt, pcov, infodict, errmsg, ier) = res

        #if ier not in [1, 2, 3, 4]:
        #    msg = "Optimal parameters not found: " + errmsg
        #    raise RuntimeError(msg)

        if pcov is None:
            # indeterminate covariance
            pcov = numpy.zeros((len(popt), len(popt)), dtype=float)
            pcov.fill(numpy.inf)

        if len(self.yvals) > len(p0):
            s_sq = (numpy.asarray(self._chi_function(popt)**2).sum() / (len(self.yvals) - len(p0)))
            pcov = pcov * s_sq
        else:
            pcov.fill(numpy.inf)

        self.optpar_Levenberg = popt
        self.par_stddev       = numpy.sqrt(numpy.diag(pcov))
        self.ret_criterion   += '\nLM:      '

        if ier not in [1, 2, 3, 4]:
            self.ret_criterion += 'Optimal parameters not found: ' + errmsg
            return

        # check that Levenberg-Marquardt didn't change the Simplex parameters
        # too much

        diff = numpy.abs(self.optpar_Simplex - self.optpar_Levenberg)
        if numpy.min(self.par_stddev - diff) < 0.0:
            self.ret_criterion = 'Warning: LM changed Simplex parameters too much. '
            return

        self.ret_criterion += 'Successful Levenberg-Marquardt refinement. '


# end of class simfit


def SimFitStandardCallBack(SimFitInstance):
    """
    This is the default callback function, called whenever SimFit wants to
    show its progress.
    You can use this function as a template to create your own callback
    functions.

    """        
    maxNameLen = numpy.max([len(e) for e in SimFitInstance._FlatParNames])
    format = '     %' + str(maxNameLen) + 's: ' + '%g'
    print('Iteration:', SimFitInstance.curr_iterations,end='')
    for i in range(len(SimFitInstance.curr_par)):
        print(format % (SimFitInstance._FlatParNames[i], SimFitInstance.curr_par[i]),end='')
    print()
    




####################################################################

if __name__ == '__main__':
    
    # Define the fit function:

    def kww(a,b,t,tau,beta):
        y = a*numpy.exp(-((t/tau)**beta))+b
        return y

    # Prepare some test data:

    numpoints = 50
    x = numpy.logspace(-2.0,3.0,num=numpoints)
    y = kww(1.0, 0.0, x, 10.0, 0.8) + 0.05*numpy.random.normal(size=(numpoints))

    # plt.semilogx(x,y,'or')
    
    # Prepare a SimFit object:

    s = SimFit()
    s.func = kww
    s.startpar = {'tau':1.0, 'beta':1.0}
    s.constant = {'a':1.0, 'b':0.0}
    s.indep    = 't'
    s.xvals    = x
    s.yvals    = y


    s(True,cntrl=1)



    
