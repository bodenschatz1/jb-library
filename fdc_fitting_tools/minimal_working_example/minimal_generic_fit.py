import sys
import os
import SimFit
import h5py
import json
import matplotlib
#matplotlib.use('Agg')
import time as real_time
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
from scipy.optimize import fsolve, root
from scipy.signal import savgol_filter
from scipy.integrate import quad
from numpy import pi as PI
from numpy import sin, cos, tan, sqrt, arctan
from numpy.lib import scimath
from numba import float64, njit, jit

from scipy.io import loadmat

from multiprocessing import Pool

from generics import run_generate_generics

from ftc_tools import get_files,fit_output_load, read_data_2be_fitted, read_s0_velocity, read_contact_point, read_baseline_corr, read_height, cell_load

def apply_fit_range(ori_time, ori_height, ori_force, fit_range):
    '''
    this function applies the fit range to the data
    '''
    # slicing to fit_range
    force = ori_force[(ori_time > fit_range[0])&(ori_time < fit_range[1])]
    height = ori_height[(ori_time > fit_range[0])&(ori_time < fit_range[1])]
    time = ori_time[(ori_time > fit_range[0])&(ori_time < fit_range[1])]
    return time, height, force


# get index
def _find_contact_point_index(time,contact_point):
        return np.abs(time-contact_point).argmin()

def improved_data_reduction(time,height,force,contact_point, reduction):
    '''
    a data reduction that also includes a contact point
    '''
    data_length = len(time)
    # from matlab downsample seems to just be array slicing of the array by the downbyhowmuch value
    downbyhowmuch = np.floor(data_length / reduction)

    # change to int
    downbyhowmuch = downbyhowmuch.astype(int)

    # cp index
    index_cp = _find_contact_point_index(time,contact_point)
    if downbyhowmuch == 0:
        time = time[index_cp:]
        height = height[index_cp:]
        force = force[index_cp:]
    else:
        time = time[index_cp::downbyhowmuch]
        height = height[index_cp::downbyhowmuch]
        force = force[index_cp::downbyhowmuch]

    return time, height, force

def get_data(file):
    '''
    get all the required data and the fit range
    from the hdf5 file
    '''
    time, force = read_baseline_corr(file)
    height = read_height(file)
    contact_point = read_contact_point(file)
    s0_velocity = read_s0_velocity(file)
    return time, height, force, contact_point, s0_velocity

# save fit data into hdf5
def save_fit(fname,
             fit_parameter,
             fitted_time,
             fitted_height,
             fitted_force,
             fit_force,
             data_reduction,
             fit_range_factor,
             a,
             g,
             R1,
             angle,
             zabs):
    # fit data
    # fit_force = discher_final_function_visco(height,par[0],par[1],par[2],r1,phi)
    # open h5df file
    file = h5py.File(fname,'a')

    # check if fitparameter are already saved and  delete them
    if 'fit_parameter' in file:
        del file['fit_parameter']
    if 'fit_force' in file:
        del file['fit_force']  # from fit
        del file['fitted_force'] # force data that is fitted
        del file['fitted_height']
        del file['fitted_time']
    if 'generic parameter g'  in file:
        del file['generic parameter g']
        del file['generic parameter a']

    # save the data to hdf5
    file.create_dataset('fit_force', data = fit_force) # now in nN
    file.create_dataset('fitted_force', data = fitted_force) # now in nN
    file.create_dataset('fitted_height', data = fitted_height)
    file.create_dataset('fitted_time', data = fitted_time)
    file.create_dataset('fit_parameter',data = fit_parameter)
    file.create_dataset('generic parameter g', data = g)
    file.create_dataset('generic parameter a', data = a)
    file.attrs['data_reduction_factor'] = data_reduction
    file.attrs['fit_range_factor'] = fit_range_factor
    file.attrs['fit_R1'] = R1
    file.attrs['fit_angle'] = angle
    file.attrs['fit_zabs'] = zabs
    file.close()


def _region_1(t,g,a,R1,vo,A0,T,Ka,beta):
    Rs = R1
    t = t.astype('complex')
    xi = (vo * t) / R1
    generic_func =g[0] * xi + g[1] * xi**2 + g[2] * xi**3 + g[3] * xi**4
    generic_area_func_conv = (1 + beta)**(-1) * (2 + beta)**(-1) * (3 + beta)**(-1) * (4+beta)**(-1) * Rs**(-4) * t**(1 + beta) * vo * ((2+beta) * (3+beta) * (4+beta) * a[0] * Rs**3 + 2 * t * vo * ((3 + beta) * (4 + beta) * a[1] * Rs**2 + 3 * t * vo * ((4 + beta) * a[2] * Rs + 4 * a[3] * t * vo)))
    force = generic_func * R1 * (T + Ka * generic_area_func_conv / A0)
    return force

def _region_2_cycle(t,g,a,tm,R1,vo,A0,T,Ka,beta):
    Rs=R1
    t = t.astype('complex')
    xi_r = vo*(2*tm - t)/Rs
    generic_func_retract = g[0] * xi_r + g[1] * xi_r**2+g[2] * xi_r**3 + g[3] * xi_r**4
    f1 = ((24+26*beta+9*beta**2+beta**3)*a[0]* Rs**3+2*t*vo*((12+7*beta+beta**2)*a[1]*Rs**2+3*t*vo*((4+beta)*a[2]*Rs+4*a[3]*t*vo)))
    f_2 = ((2+beta)*(3+beta)*(4+beta)*a[0]*Rs**3+vo*(2*(3+beta)*(4+beta)*a[1]*Rs**2*((-1)*t+(3+beta)*tm)+vo*(3*(4+beta)*a[2]*Rs*(2*t**2+(-2)*(5+beta)*t*tm+(14+beta*(7+beta))*tm**2)+4*a[3]*((-6)*t**3+6*(7+beta)*t**2*tm+(-3)*(34+beta*(11+beta))* t*tm**2+(5+beta)*(18+beta*(7+beta))*tm**3)*vo)))
    f_3 = (4*(4+beta)**(-1)*a[3]*(t+(-1)*tm)**3*vo**3+(-3)*(3+beta)**(-1)*(t+(-1)*tm)**2*vo**2*(a[2]*Rs+4*a[3]*t*vo)+2*(2+beta)**(-1)*(t+(-1)*tm)*vo*(a[1]*Rs**2+3*t*vo*(a[2]*Rs+2*a[3]*t*vo))+(-1)*(1+beta)**(-1)*(a[0]*Rs**3+t*vo*(2*a[1]*Rs**2+t*vo*(3*a[2]*Rs+4*a[3]*t*vo))))


    area_integral = (1+beta)**(-1)*(2+beta)**(-1)*(3+beta)**(-1)*(4+beta)**(-1)*Rs**(-4)*t**(1+beta)*vo*f1+(-1)*(1+beta)**(-1)*(2+beta)**(-1)*(3+beta)**(-1)*(4+beta)**(-1)*Rs**(-4)*(t+(-1)*tm)**(1+beta) *vo*f_2+Rs**(-4)*(t+(-1)*tm)**(1+beta)*vo*f_3

    force = generic_func_retract * R1 * (T + Ka * area_integral/A0)
    return force

def _region_2_dwell(t,g,a,tm,R1,vo,A0,T,Ka,beta):
    Rs = R1
    t = t.astype('complex')
    xi_r = (vo * tm)/Rs
    generic_func_retract = g[0]*xi_r+g[1]*xi_r**2+g[2]*xi_r**3+g[3]*xi_r**4
    area_integral = (1+beta)**(-1)*(2+beta)**(-1)*(3+beta)**(-1)*(4+beta)**(-1)*                                   Rs**(-4)*t**(1+beta)*vo*((24+26*beta+9*beta**2+beta**3)*a[0]* Rs**3+2*t*vo*((12+7*beta+beta**2)*a[1]*Rs**2+3*t*vo*((4+beta)*a[2]*Rs+4*a[3]*t*vo)))+Rs**(-4)*(t+(-1)*tm)**(1+beta)*vo*(4*(4+beta)**(-1)*a[3]*(t+(-1)*tm)**3*vo**3+(-3)*(3+beta)**(-1)*(t+(-1)*tm)**2*vo**2*(a[2]*Rs+4*a[3]*t*vo)+2*(2+beta)**(-1)*(t+(-1)*tm)*vo*(a[1]*Rs**2+3*t*vo*(a[2]*Rs+2*a[3]*t*vo))+(-1)*(1+beta)**(-1)*(a[0]*Rs**3+t*vo*(2*a[1]*Rs**2+t*vo*(3*a[2]*Rs+4*a[3]*t*vo))))

    force = generic_func_retract * R1 * (T + Ka * area_integral/A0)
    return force

def vec_fit_function_skalar_discher(time,g,a,R1,vo,A0,tm,T,Ka,beta,dwell):
    """
    the force curves with be already completely corrected when they come to this part
    """
    force = np.zeros(time.shape,dtype='complex')
    # region 1
    force+= (time<=tm) * _region_1(time,g,a,R1,vo,A0,T,Ka,beta)
    # region 2
    if dwell:
        force+= (time>tm) * _region_2_dwell(time,g,a,tm,R1,vo,A0,T,Ka,beta)
    else:
        force+= (time>tm) * _region_2_cycle(time,g,a,tm,R1,vo,A0,T,Ka,beta)
    # force is now complex, test with np.sum(np.abs(np.imag(force))) = 0, will just take real part
    return np.real(force) * 1000 # no in nN


def plot_fit(time,height,force,par, fit_force, fname,ori_force,ori_time,ori_height, contact_point,
             xaxis='time', show = False, save=True):
    # stuff force plots
    _time = ori_time[ori_time >= contact_point]
    _height = ori_height[ori_time >= contact_point]
    _force = ori_force[ori_time >= contact_point]
    # shift ori_time
    ori_time = ori_time - _time[0]
    #ori_force = ori_force - _force[0]
    # plot
    fig = plt.figure()
    ax = plt.gca()
    if xaxis == 'time':
        ln1 = ax.plot(ori_time[ori_time > - 0.2], ori_force[ori_time > -0.2],
                      'k.', alpha = 0.3,label = 'data')
        ln3 = ax.plot(time, force,label = 'reduction')
        ln2 = ax.plot(time, fit_force, linewidth = 2,label = 'fit')
        # x label
        ax.set_xlabel('time / s')
    else:
        ln1 = ax.plot(_height[0] - ori_height, ori_force, '.', label = 'data')
        ln3 = ax.plot(height, force,label = 'reduction')
        ln2 = ax.plot(height, fit_force, linewidth = 2,label = 'fit')
        # x label
        ax.set_xlabel('indentation depth / µm')

    # y label
    ax.set_ylabel('force / nN')

    # fit parameter as text
    ax.text(0.1, 0.4, 'T = '+str(np.around(par[0]*1e3,5))+' mN/m \nK$_A$ = '+str(np.around(par[1],5)) + ' N/m\n$\\beta$ = ' + str(np.around(par[2],5)), fontsize=12, transform = ax.transAxes)

    # legend
    lns = ln1 + ln2 + ln3
    labs = [l.get_label() for l in lns]
    ax.legend(lns,labs)
    plt.title(os.path.basename(fname)[:-25])
    figure_name = fname[:-25]+'.png'
    plt.tight_layout()
    if save:
        plt.savefig(figure_name,dpi=150)
    if show:
        plt.show()
    else:
        plt.close()

def process(fname_variables):
    fname = fname_variables[0]
    variables = fname_variables[1]
    fit_range_type = variables[0]
    fit_from = variables[1]
    sogolay_window = variables[2]
    data_reduction_factor = variables[3]
    para_from_file = variables[4]
    dwell = variables[5]
    value = variables[6]
    sogolay = variables[7]
    # get hdf5 file and data
    file = h5py.File(fname,'r')
    # check if curve was considered if trace ok doesnt exist will still continue
    if 'trace_ok' in file.attrs:   # prehaps for later
       if file.attrs['trace_ok'] == 'no':
           print('aazz')
           return None
    #global time
    time, height, force, contact_point, s0_velocity = get_data(file)
    # -----------------check if baseline was correctly corrected
    if force.max() < 0:
        file.close()
        print('argh')
        return (fname, 'baseline')
    #------------------------------------
    ori_time = np.copy(time)
    ori_height = np.copy(height)
    ori_force = np.copy(force)
    # -----------------------------------
    # smoothing with Savitzky-Golay filter for the fit range
    window = 101
    force = savgol_filter(force,window,1)
    # ----------------------------------
    fit_range = [0,0]
    index_max = np.argmax(force)
    if (fit_range_type == 'from file') & ('fit_range' in file.attrs):
        fit_range = file.attrs['fit_range']
    elif dwell:
        x = time[index_max] - contact_point
        fit_range[1] = 0.3 * x + x + contact_point
    else:
        fit_range[0] = 0
        fit_range[1] = time[(force > force[index_max] * (1-value)) & (time > time[index_max])].max()
    # smoothing with Savitzky-Golay filter
    if sogolay:
        # sogolay_window = 27
        force = savgol_filter(force,sogolay_window,1)
    else:
        force = ori_force
    ori_time, ori_height, ori_force = apply_fit_range(time, height, force, fit_range)
    # data is now adjusted
    time, height, force = apply_fit_range(time, height, force, fit_range)

    # reduce data points
    time, force, height = improved_data_reduction(time, force, height,
                                                          contact_point,
                                                          data_reduction_factor)
    # all data has been extracted
    time = time - time[0]
    #force = force - force[0]
    # remaking the height
    maxi = np.argmax(force)
    piezo_app = s0_velocity * time[:maxi+1]
    if dwell:
        piezo_back = s0_velocity * time[maxi] * np.ones(len(time[maxi+1:]))
    else:
        piezo_back = 2 * s0_velocity * time[maxi] - s0_velocity * time[maxi+1:]
    height = np.append(piezo_app,piezo_back)

    # ===================== Parameters ====================================
    if para_from_file:
        angle = 35 #contact angle before indentation
        R1 = file.attrs['R1']
    else:
        angle = 35 # opening angle of the cell cap
        Rp = 6.62 / 2 # radius of indentor
        R1 = 12 # radius cell?
    file.close()
    z_abs = height[np.argmax(force)]

    # ====================== Calc Generics / (later calc or load?)
    g, a, A0 = run_generate_generics(angle,Rp,R1,z_abs)

    tm = time[np.argmax(force)]
    # for testing
    run = True
    if run:
        '''
        to run the fit
        '''
        startpar = {'T': 0.0001, 'Ka': 0.015, 'beta': -0.6}
        constants = {'g' : g,  'a' : a, 'R1' : R1, 'vo' : s0_velocity,
                     'A0' : A0, 'tm' : tm,'dwell' : dwell}
        s = SimFit.SimFit(xvals = time, yvals = force,
                          func = vec_fit_function_skalar_discher,
                          startpar = startpar,
                          indep = 'time',
                          constant = constants,
                          #minpar = {'beta' : -1},
                          #maxpar = {'beta' : 0},
                          #tol = [1e-4, 1000],
                          cntrl = -1,
                          maxiter =1000)
        s.fittype = 'levenberg'
        s()
        par = s.curr_par
        fit_force = vec_fit_function_skalar_discher(time,g,a,R1,s0_velocity,
                                                    A0,tm,par[0],par[1],par[2],dwell)
        plot_fit(time,
                 height,
                 force,
                 par,
                 fit_force,
                 fname,
                 ori_force,
                 ori_time,
                 ori_height,
                 contact_point)

        save_fit(fname,
                 par,
                 time,
                 height,
                 force,
                 fit_force,
                 data_reduction_factor,
                 value,
                 a,g,R1,angle,z_abs)

def run_processes(paths,cores,fit_range_type,fit_from,sogolay=True,
                  sogolay_window = 27,
                  data_reduction_factor = 250,
                  para_from_file = False,
                  dwell = False,
                  value = 0.4,):
    variables = [fit_range_type,fit_from,sogolay_window,
                 data_reduction_factor,para_from_file,dwell,value,sogolay]
    #global fit_range_type,fit_from,para_from_file,dwell,value,data_reduction_factor,sogolay_window
    for path in paths:
        print(path)
        # use a shorter list of file names
        if fit_from == 1:
            fnames = fit_output_load(path)
        # list of all file names in folder
        elif fit_from == 0:
            fnames, _ = get_files(path)
        elif fit_from == 2:
            fnames = cell_load(path)
        fnames_variables = [[fname,variables] for fname in fnames]
        p = Pool(cores)
        # returns file names of junk data
        stuff = list(tqdm(p.imap(process,fnames_variables),total = len(fnames)))
        p.close()
        # waits for everything to finish
        p.join()
        # fit output saved as json
        # collection of the returns from the processes
        file_name = path + '/fit_output.txt'
        stuff = list(filter(None, stuff))
        output = {}
        dic_stuff = dict(stuff)

        with open(file_name,'w+') as outfile:
            json.dump(dic_stuff, outfile)

