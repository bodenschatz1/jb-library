import os
import SimFit
import time as real_time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from numpy import pi as PI
from numpy import sin, sqrt, arctan, tan, cos
from scipy.optimize import fsolve, root, least_squares
from scipy.integrate import quad
from numpy.lib import scimath
from tqdm import tqdm
from numba import float64, njit, jit

import time as real_time
#matlab stuff
from scipy.io import loadmat

from multiprocessing import pool

"""
This is the script that should generate a generic parameter database file
and check that database if new parameters should be calculated.
----
14.09.2020 - Jonathan Bodenschatz
"""
# add environ label
# in terminal:export GENERICS_FILE='/home/jbodes/ownCloud/strain_measurements/generics.csv'

#generics_file = os.environ["GENERICS_FILE"]

def get_generics(R1, Rp, angle, z_abs):
    generics_table = pd.read_csv(generics_file, index_col=0)
    where = (np.isclose(generics_table["R1"], R1)) & (np.isclose(generics_table["Rp"], Rp)) & (np.isclose(generics_table["angle"], angle))&(np.isclose(generics_table["z_abs"],z_abs))
    if where.sum() == 0:
        r_g, r_area = generate_generics(angle,Rp_abs/R1,1/R1)
        generics_table.loc[len(generics_table)] = [R1, Rp, angle, z_abs] + list(r_g) + list(r_area)
        generics_table.to_csv(generics_file)
        return r_g, r_area
    if where.sum() == 1:
        r_g = generics_table[where][["r_g_x", "r_g_y", "r_g_z"]]
        r_area = generics_table[where][["r_area_x", "r_area_y", "r_area_z"]]
        return r_g, r_area
    msg = "generics table messed up, multiple entries for "
    msg += "R1 = {}, Rp_abs = {}, angle = {}".format(R1, Rp_abs, angle)
    raise RuntimeError(msg)

def init_generics_file():
    df = pd.DataFrame(columns=["R1","Rp","angle","z_abs","r_g_0", "r_g_1", "r_g_2","r_g_3",
                               "r_area_0", "r_area_1", "r_area_2","r_area_3"])
    df.to_csv(generics_file,index=False)

# above is old code

# ---------------------- Functions needed --------------------------------
@njit(float64(float64,float64), cache = True)
def theta(r1,Rp):
    f1 = sqrt(Rp**2 - r1**2)
    return np.arctan(r1 / f1)

@njit(float64(float64, float64, float64, float64), cache = True)
def A(r1, phi, Rp, R1):
    """
    Eq. A5(1) in ref. 1
    """
    f1 = R1 * sin(phi) + r1 * sin(theta(r1,Rp))
    f2 = R1 ** 2 - r1 ** 2
    return f1 / f2

@njit(float64(float64, float64, float64, float64), cache=True)
def B(r1, phi, Rp, R1):
    """
    Eq. A5(2) in ref. 1
    """
    return -A(r1,phi,Rp,R1) * r1**2 - r1 * sin(theta(r1,Rp))

# for arrays
@njit(float64[:](float64[:], float64), cache = True)
def theta_array(r1,Rp):
    f1 = sqrt(Rp**2 - r1**2)
    return np.arctan(r1 / f1)



@njit(float64[:](float64[:], float64[:], float64, float64), cache = True)
def A_array(r1, phi, Rp, R1):
    """
    Eq. A5(1) in ref. 1
    """
    f1 = R1 * sin(phi) + r1 * sin(theta_array(r1,Rp))
    f2 = R1 ** 2 - r1 ** 2
    return f1 / f2

# --------------- other functions -------------------
@njit(float64(float64, float64))
def hub(r1, Rp):
    """
    some kind of height
    """
    f1 = sqrt(Rp**2 - r1**2)
    return Rp - f1


# ---------------------------------the integrads -----------------------------------
@njit(float64(float64, float64, float64, float64, float64), cache=True)
def cyl(r, r1, phi, Rp, R1):
    """
    Some other function I don't see the point of right now.
    """
    _A = A(r1,phi,Rp,R1)
    _B = B(r1,phi,Rp,R1)
    _C = _A * r + _B / r

    return _C / sqrt(1 - _C**2)

@njit(float64(float64, float64, float64, float64, float64), cache=True)
def volume_integrand(r, r1, phi, Rp, R1):
    """
    The integrand in eq. A6, Sen, Discher, 2005, Biophys. J. 89(5) 3203-3213

    matlab calls this vol
    """
    return PI * r**2 * cyl(r,r1,phi,Rp,R1)

@njit(float64(float64, float64, float64, float64, float64), cache=True)
def area_integrand(r, r1, phi, Rp, R1):
    """
    The integrand in eq. A10, ref. 1

    called surface discher in the matlab code
    """
    _A = A(r1,phi,Rp,R1)
    _B = B(r1,phi,Rp,R1)
    _C = _A * r + _B / r

    return (2 * PI * r) / sqrt(1 - _C**2)

# ------------ The integrals using quad from scipy --------------------------------
def volume(r1, phi, Rp, R1):
    """
    Calculate the volume using eq. A6 in ref. 1
    """
    #breakpoint()
    _V1 = quad(volume_integrand,
              r1,
              R1,
              args=(r1, phi, Rp, R1))
    V1 = _V1[0]

    Vk = (1/3) * hub(r1,Rp)**2 * PI * (3 * Rp - hub(r1,Rp))

    return V1 - Vk

def area(r1, phi, Rp, R1):
    """
    Calculate the volume using eq. A10 in ref. 1
    """
    A1 = quad(area_integrand,
              r1,
              R1,
              args=(r1, phi, Rp, R1))[0]

    Ak = 2 * PI * Rp * hub(r1,Rp)

    return A1 + Ak

def some_height(r1, phi, Rp, R1):
    """
    One integral appearing in jonathan's thingy.
    """
    K1 = quad(cyl,
              r1,
              R1,
              args=(r1, phi, Rp, R1),
              limit = 500)[0]

    return K1 - hub(r1,Rp)

# -------------- here comes the equation system -------------------
def equation_system(xs,z_indexed, Rp, R1, Vkk, h):
    r1 = xs[0]
    phi = xs[1]
    #print(xs)
    # Get volume
    _V = volume(r1,phi,Rp,R1)

    # Get area
    #_A = area(r1, phi,Rp) # unused

    # Get this weird height
    _h = some_height(r1,phi,Rp,R1)

    # Derived things.
    #_alpha = (_A - A0) / A0 # unused
    _delta = h - _h

    # Construct output
    ret_1 = Vkk - _V
    ret_2 = z_indexed - _delta

    return [ret_1, ret_2]

#  ---------------- here comes the other stuff / directly transfered from matlab -----------------
def discher_function_visco(r1, phi, z, Rp, R1, Vkk, h):
    x0 = [r1, phi]

    #xs = fsolve(equation_system, x0, z, xtol=1e-8,epsfcn=1e10)
    sol = root(equation_system, x0, args=(z,Rp,R1,Vkk,h), method='lm',tol=1e-8)
    xs = sol.x

    # some adjustments
    r1 = xs[0]
    phi = xs[1]

    # get area
    Area = area(r1,phi,Rp,R1)

    # calc generic_func
    generic_func = (2 * PI * R1 ** 2 * A(r1,phi,Rp,R1) - 2 * PI * R1 * sin(phi))

    return generic_func, r1, phi, Area

def discher_final_function_visco(z, r1, phi, A0, Rp, R1, Vkk, h):
    """
    z    : height
    r1   : starting value for r1
    phi  : starting value for phi
    A0   : constant could be global but lets not do that
    """
    stuff = np.full([len(z),4],np.nan)
    # setting first element
    stuff[0,0] = A0
    stuff[0,1] = 0
    # for loop that goes through elements starting at element 1
    for i in range(1,len(z)):
        generic_func, r1, phi, Area = discher_function_visco(r1,phi,z[i],Rp,R1,Vkk,h)
        stuff[i,0] = Area
        stuff[i,1] = generic_func

    return stuff[:,1], stuff[:,0]


def generic4p(z,p4,p3,p2,p1):
    return p4*z**4 + p2*z**2 + p3*z**3 + p1*z

def generic4p_res(z,generic_f,p4,p3,p2,p1):
    return generic4p(z,p4,p3,p2,p1) - generic_f

def generic4p_area(z,A0,p4,p3,p2,p1):
    return generic4p(z,p4,p3,p2,p1) + A0
def generate_generics(angle,Rp,zend):
    # parameters needed for the calculation might need to make the global :<
    Phio = angle * PI / 180
    R1 = 1
    Ro = R1 / sin(Phio)
    h = Ro - sqrt(Ro ** 2 - R1 ** 2) # height of sphere cap
    Vkk = (1/3) * h ** 2 * PI * (3 * Ro - h) # volume of sphere cap
    A0 = 2 * PI * Ro * h # surface area of sphere cap
    r1 = 0.0001 # start parameter radius in micro meter
    phi = Phio # start parameter 2

    n = 1500 # number of data points

    z = np.linspace(0,zend,n)
    time = np.linspace(0,zend,n) # <=== time is never used

    # [generic_f, area] = discher_final_function_sphere_generic(z,par)
    generic_func, Area = discher_final_function_visco(z,r1,phi,A0,Rp,R1,Vkk,h)

    # fit starts here
    pol = np.polyfit(z,generic_func,4)
    # simfit try
    startpar = {'p4': pol[0], 'p3': pol[1], 'p2': pol[2],'p1' : pol[3]}
    #constants = {'r1' : r1,  'phi' : phi, 'Rp' : Rp, 'Vkk' : Vkk, 'h' : h}
    s = SimFit.SimFit(xvals = z,yvals = generic_func,
                      func = generic4p,
                      startpar = startpar,
                      indep = 'z',
                      #constant = constants,
                      #minpar = {'T' : 0, 'Ka' : 0, 'beta' : 0},
                      #maxpar = {'beta' : 1},
                      #tol = [1e-4, 1000],
                      cntrl = -1,
                      maxiter =1000)
    s.fittype = 'levenberg'
    s()
    fit_generic= s.curr_par

    pol = np.polyfit(z,Area,4)
    # fit area
    startpar = {'p4': pol[0], 'p3': pol[1], 'p2': pol[2],'p1' : pol[3]}
    constants = {'A0' : A0}
    s = SimFit.SimFit(xvals = z,yvals = Area,
                      func = generic4p_area,
                      startpar = startpar,
                      indep = 'z',
                      constant = constants,
                      #minpar = {'T' : 0, 'Ka' : 0, 'beta' : 0},
                      #maxpar = {'beta' : 1},
                      #tol = [1e-4, 1000],
                      cntrl = -1,
                      maxiter =1000)
    s.fittype = 'levenberg'
    s()
    fit_area= s.curr_par

    return z, generic_func, Area, fit_generic, fit_area, A0

def test_run_generate_generics(angle, Rp, R1,z_abs):
    Rp = Rp / R1
    zend = z_abs / R1
    z, generic_func, Area,fit_generic, fit_area,A0 = generate_generics(angle,Rp,zend)
    return z, generic_func, Area, fit_generic, fit_area, A0

def run_generate_generics(angle, Rp, R1, z_abs):
    Rp = Rp / R1
    zend = z_abs / R1
    z, generic_func, area,fit_generic, fit_area,A0 = generate_generics(angle,Rp,zend)
    return np.flip(fit_generic), np.flip(fit_area), A0

# ----------------------------------------------------------------------------------
# ---------------------------- cone functions --------------------------------------
# ---------------------- Functions needed --------------------------------
@njit(float64(float64, float64, float64, float64), cache = True)
def A_cone(r1, phi, theta, R1):
    '''
    Eq. A5(1) in ref. 1
    '''
    f1 = R1 * sin(phi) + r1 * sin(theta)
    f2 = R1 ** 2 - r1 ** 2
    return f1 / f2

@njit(float64(float64, float64, float64, float64), cache=True)
def B_cone(r1, phi, theta, R1):
    """
    Eq. A5(2) in ref. 1
    """
    return -A_cone(r1,phi,theta,R1) * r1**2 - r1 * sin(theta)

@njit(float64[:](float64[:], float64[:], float64, float64), cache = True)
def A_cone_array(r1, phi, theta, R1):
    '''
    Eq. A5(1) in ref. 1
    '''
    f1 = R1 * sin(phi) + r1 * sin(theta)
    f2 = R1 ** 2 - r1 ** 2
    return f1 / f2

# ---------------------------------the integrads -----------------------------------
@njit(float64(float64, float64, float64, float64, float64), cache=True)
def cyl_cone(r, r1, phi, theta, R1):
    """
    Some other function I don't see the point of right now.
    """
    _A = A_cone(r1,phi,theta,R1)
    _B = B_cone(r1,phi,theta,R1)
    _C = _A * r + _B / r

    return _C / sqrt(1 - _C**2)

@njit(float64(float64, float64, float64, float64, float64), cache=True)
def volume_cone_integrand(r, r1, phi, theta, R1):
    """
    The integrand in eq. A6, Sen, Discher, 2005, Biophys. J. 89(5) 3203-3213

    matlab calls this vol
    """
    return PI * r**2 * cyl_cone(r,r1,phi,theta,R1)

@njit(float64(float64, float64, float64, float64, float64), cache=True)
def area_cone_integrand(r, r1, phi, theta, R1):
    """
    The integrand in eq. A10, ref. 1

    called surface discher in the matlab code
    """
    _A = A_cone(r1,phi,theta,R1)
    _B = B_cone(r1,phi,theta,R1)
    _C = _A * r + _B / r

    return (2 * PI * r) / sqrt(1 - _C**2)

# ------------ The integrals using quad from scipy --------------------------------
def volume_cone(r1, phi, theta, R1):
    """
    Calculate the volume using eq. A6 in ref. 1
    """
    #breakpoint()
    _V1 = quad(volume_cone_integrand,
              r1,
              R1,
              args=(r1, phi, theta, R1))
    V1 = _V1[0]

    Vk = (PI * r1**3 * tan(theta)) / 3

    return V1 - Vk

def area_cone(r1, phi, theta, R1):
    """
    Calculate the volume using eq. A10 in ref. 1
    """
    A1 = quad(area_cone_integrand,
              r1,
              R1,
              args=(r1, phi, theta, R1))[0]

    Ak = PI * r1**2 / cos(theta)

    return A1 + Ak

def some_height_cone(r1, phi, theta, R1):
    """
    One integral appearing in jonathan's thingy.
    """
    K1 = quad(cyl_cone,
              r1,
              R1,
              args=(r1, phi, theta, R1),
              limit = 500)[0]

    return K1 - r1 * tan(theta)

# -------------- here comes the equation system -------------------
def equation_system_cone(xs,z_indexed, theta, R1, Vkk, h):
    r1 = xs[0]
    phi = xs[1]
    #print(xs)
    # Get volume
    _V = volume_cone(r1,phi,theta,R1)

    # Get area
    #_A = area(r1, phi,Rp) # unused

    # Get this weird height
    _h = some_height_cone(r1,phi,theta,R1)

    # Derived things.
    #_alpha = (_A - A0) / A0 # unused
    _delta = h - _h

    # Construct output
    ret_1 = Vkk - _V
    ret_2 = z_indexed - _delta

    return [ret_1, ret_2]



def discher_function_cone(r1,phi,z,theta,R1,Vkk,h):
    x0 = [r1, phi]

    sol = root(equation_system_cone, x0, args=(z,theta,R1,Vkk,h), method='lm',tol=1e-8)
    xs = sol.x

    # some adjustments
    r1 = xs[0]
    phi = xs[1]

    # get area
    Area = area_cone(r1,phi,theta,R1)

    # calc generic_func
    generic_func = (2 * PI * R1 ** 2 * A_cone(r1,phi,theta,R1) - 2 * PI * R1 * sin(phi))

    return generic_func, r1, phi, Area

def discher_final_function_cone(z,r1,phi,A0,theta,R1,Vkk,h):
    '''
    z    : height
    r1   : starting value for r1
    phi  : starting value for phi
    A0   : constant could be global but lets not do that
    '''
    stuff = np.full([len(z),4],np.nan)
    # setting first element
    stuff[0,0] = A0
    stuff[0,1] = 0
    # for loop that goes through elements starting at element 1
    for i in range(1,len(z)):
        generic_func, r1, phi, Area = discher_function_cone(r1,phi,z[i],theta,R1,Vkk,h)
        stuff[i,0] = Area
        stuff[i,1] = generic_func

    return stuff[:,1], stuff[:,0]

def cone_generate_generics(angle,theta_cone,zend):
    Phio = angle * PI / 180
    R1 = 1
    Ro = R1 / sin(Phio)
    theta = (PI / 2) - theta_cone * PI / 180
    h = Ro - sqrt(Ro ** 2 - R1 ** 2) # height of sphere cap
    Vkk = (1/3) * h ** 2 * PI * (3 * Ro - h) # volume of sphere cap
    A0 = 2 * PI * Ro * h # surface area of sphere cap
    r1 = 0.0001 # start parameter radius in micro meter
    phi = Phio # start parameter 2

    n = 1500 # number of data points

    z = np.linspace(0,zend,n)
    time = np.linspace(0,zend,n) # <=== time is never used

    generic_func, Area = discher_final_function_cone(z,r1,phi,A0,theta,R1,Vkk,h)

    # fit starts here
    pol = np.polyfit(z,generic_func,4)
    # simfit try
    startpar = {'p4': pol[0], 'p3': pol[1], 'p2': pol[2],'p1' : pol[3]}
    #constants = {'r1' : r1,  'phi' : phi, 'Rp' : Rp, 'Vkk' : Vkk, 'h' : h}
    s = SimFit.SimFit(xvals = z,yvals = generic_func,
                      func = generic4p,
                      startpar = startpar,
                      indep = 'z',
                      #constant = constants,
                      #minpar = {'T' : 0, 'Ka' : 0, 'beta' : 0},
                      #maxpar = {'beta' : 1},
                      #tol = [1e-4, 1000],
                      cntrl = -1,
                      maxiter =1000)
    s.fittype = 'levenberg'
    s()
    fit_generic= s.curr_par

    pol = np.polyfit(z,Area,4)
    # fit area
    startpar = {'p4': pol[0], 'p3': pol[1], 'p2': pol[2],'p1' : pol[3]}
    constants = {'A0' : A0}
    s = SimFit.SimFit(xvals = z,yvals = Area,
                      func = generic4p_area,
                      startpar = startpar,
                      indep = 'z',
                      constant = constants,
                      #minpar = {'T' : 0, 'Ka' : 0, 'beta' : 0},
                      #maxpar = {'beta' : 1},
                      #tol = [1e-4, 1000],
                      cntrl = -1,
                      maxiter =1000)
    s.fittype = 'levenberg'
    s()
    fit_area= s.curr_par

    return z, generic_func, Area, fit_generic, fit_area, A0

def run_cone_generate_generics(angle, theta_cone, R1, z_abs):
    zend = z_abs / R1
    z, generic_func, area, fit_generic, fit_area, A0 = cone_generate_generics(angle, theta_cone, zend)
    return np.flip(fit_generic), np.flip(fit_area), A0


######################## testing / run part
# to check if the values are equal or very close to matlab values
test = False

if test:

    t_0 = real_time.time()
    z, generic_func, Area, fit_generic, fit_area, A0= test_run_generate_generics(angle=35,
                                                                            Rp=6.62/2,
                                                                            R1=12,
                                                                            z_abs=2.5)
    t_1 = real_time.time()
    print(f'time {t_1-t_0}')

    # matlab part
    annots = loadmat('../sandbox/matlab/generic/generics4p_test.mat')

    mat_generic_f = annots['generic_f'][0,:]
    mat_area = annots['area'][0,:]
    mat_r_area = annots['r_area'][0,:]
    mat_r_g = annots['r_g'][0,:]
    mat_z = annots['z'][0,:]
    mat_fit_g = annots['fit_g'][0,:]
    mat_fit_a = annots['fit_a'][0,:]

    fig, (ax1, ax2) = plt.subplots(1,2, sharex=True)
    ax1.plot(mat_z,mat_generic_f,'.',label = 'matlab data')
    ax1.plot(z,generic_func,'.',label='python data')
    ax1.plot(mat_z,mat_fit_g,label='matlab fit')
    ax1.plot(z,
             generic4p(z,fit_generic[0],fit_generic[1],fit_generic[2],fit_generic[3]),
             label='python fit')
    ax2.plot(mat_z,mat_area,'.',label='matlab data')
    ax2.plot(z,Area,'.',label='python data')
    ax2.plot(mat_z,mat_fit_a,label='matlab fit')
    ax2.plot(z,
             generic4p_area(z,A0,fit_area[0],fit_area[1],fit_area[2],fit_area[3]),
             label='python fit')
    ax1.set_ylabel('gen_f')
    ax2.set_ylabel('area')
    plt.legend()
    plt.tight_layout()
    plt.savefig('comparison_to_matlab.png',dpi=300)
    plt.show()

    # ------------------ Parameters --------------for testing
    angle = 35 #contact angle before indentation
    R1 = 12 # radius cell?
    Rp = 6.62 / 2 # radius of the indentor
    Rp = Rp / R1
    zend = 2.5 / R1
    R1 = 1
    Phio = angle * PI / 180 # contact angle in radians
    Ro = R1 / sin(Phio)
    h = Ro - sqrt(Ro ** 2 - R1 ** 2) # height of sphere cap
    Vkk = (1/3) * h ** 2 * PI * (3 * Ro - h) # volume of sphere cap
    A0 = 2 * PI * Ro * h # surface area of sphere cap
    r1 = 0.0001 # start parameter radius in micro meter
    phi = Phio # start parameter 2


    plt.ion()
