import numpy as np
import h5py
import glob
import os
import json

'''
collection of functions that are used everywhere and are useful
'''



def get_files(path,type='hdf5',show=False):
    '''
    :param path: path
    :param type: file ending
    :param show: show the list
    :return: list of file names
    :return: dictionary so that user use it to find single files
    '''
    fnames = glob.glob(path+'/*.'+type)
    fnames.sort()
    keys_fnames = {}
    for i in range(len(fnames)):
        keys_fnames[os.path.basename(fnames[i])] = i
        if show:
            print(os.path.basename(fnames[i]))
    return fnames, keys_fnames

# load file list from json
def fit_output_load(path):
    fnames, _ = get_files(path,'txt')
    for fname in fnames:
        if 'fit_output' in fname:
            with open(fname) as json_file:
                fdic= json.load(json_file)
                flist = fdic['false_baselines']
        else:
            continue
    return flist

def fit_output_load_2(path):
    fnames, _ = get_files(path,'txt')
    for fname in fnames:
        if 'fit_output' in fname:
            with open(fname) as json_file:
                fdic= json.load(json_file)
                if 'baseline' in fdic.values():
                    flist = list(fdic.keys())
                else:
                    flist = []
        else:
            continue
    return flist

def _adjust_flist(path,flist):
    # this is a list of paths, because the fnames paths only word on the pc that created them
    correct_fnames = []

    # fix the fnames list to work on every pc
    for f in flist:
        correct_fnames.append(path + '/' + os.path.basename(f))
    return correct_fnames

# load file list for curves marked as a cell
def cell_load(path):
    fnames, _ = get_files(path,'txt')
    for fname in fnames:
        if 'cell_info' in fname:
            flist = []
            with open(fname) as json_file:
                fdic= json.load(json_file)
                for key in fdic:
                    flist = flist + fdic[key]
            flist = _adjust_flist(path,flist)
        else:
            continue
    return flist

# simple fit
def simple_fit_function(time,force,velocity, vorfak = 1e-4, beta= -0.5,d = 0.0001):
    """

    a = 1e-4;    % kubischer Vorfaktor or vorfak
    b = -0.5;    % beta
    d = 0.0001;  % Linearer Vorfaktor

    """
    # so it doesnt get to long :)...
    def _region_1(region,power,t,vo,vorfak,beta,d):
        power_1 = d * vo * t[region]
        numerator = 2 * vorfak * vo**3 * t[region]**(3+beta)
        denominator = 2 + 3 * beta + beta**2
        return power_1 + numerator / denominator

    def _region_2(region,power,t,tm,vo,vorfak,beta,d):
        power_1 = d * vo * (2*tm - t[region])
        power_2 = vorfak * vo**3 * (2*tm - t[region])
        numerator_1 = 2*(t[region] - tm)**(1+beta)*(t[region] - (3 + beta)*tm)
        numerator_2 = 2*(t[region]**(2+beta) - (t[region]-tm)**(1+beta)*(t[region] + tm + beta*tm))
        denominator = (1+beta)*(2+beta)

        return power_1 + power_2 * (numerator_1 + numerator_2) / denominator

    # define some parameters and find end of trace
    tm = time[force.argmax()]
    vo = velocity
    t = time
    # the regions
    region_1 = t < tm
    region_2 = t >= tm

    # new fit from AJ
    power = np.zeros(time.shape)

    power[region_1] = _region_1(region_1,power,t,vo,vorfak,beta,d)

    power[region_2] = _region_2(region_2,power,t,tm,vo,vorfak,beta,d)

    return power




# hdf5 reading functions

def read_raw_data(file):
    time = file['time']
    force = file['force']
    return np.array(time), np.array(force)

def read_height(file):
    '''
    function to extract height of first two segments
    requires opened hdf5 file with h5py
    '''
    # first measured height of the segments
    s0_height = file['s0_height'][:]
    s1_height = file['s1_height'][:]
    # since some files have arrays that are 2d
    if s0_height.ndim == 2:
        # merge the two segments
        height = np.concatenate([s0_height[:,0],s1_height[:,0]])
    else:
        height = np.concatenate([s0_height,s1_height])
    return height

def read_baseline_corr(file):
    time = file['time'][:]
    force = file['force'] - np.poly1d(file.attrs['baseline_parameter'])(time)
    return np.array(time), np.array(force)

def read_contact_point(file):
    return file.attrs['contact_point']

def read_contact_point_fitted(file):
    return file.attrs['contact_point_fitted']

def read_data_2be_fitted(file):
    time = file['time'][:]
    force = file['force'] - np.poly1d(file.attrs['baseline_parameter'])(time)
    fit_range = file.attrs['fit_range']

    xslice = time[(time > fit_range[0]) & (time < fit_range[1])]
    yslice = force[(time > fit_range[0]) & (time < fit_range[1])]

    return np.array(xslice), np.array(yslice)

def read_s0_velocity(file):
    return file.attrs['s0_velocity']
