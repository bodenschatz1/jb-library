import sys
import os
import SimFit
import h5py
import json
import matplotlib
#matplotlib.use('Agg')
import time as real_time
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
from scipy.optimize import fsolve, root
from scipy.signal import savgol_filter
from scipy.integrate import quad
from numpy import pi as PI
from numpy import sin, cos, tan, sqrt, arctan
from numpy.lib import scimath
from numba import float64, njit, jit

from scipy.io import loadmat

from multiprocessing import Pool

from generics import run_generate_generics

from ftc_tools import get_files,fit_output_load, read_data_2be_fitted, read_s0_velocity, read_contact_point, read_baseline_corr, read_height, cell_load


def _region_1(t,g,a,R1,vo,A0,T,Ka,beta):
    Rs = R1
    t = t.astype('complex')
    xi = (vo * t) / R1
    generic_func =g[0] * xi + g[1] * xi**2 + g[2] * xi**3 + g[3] * xi**4
    generic_area_func_conv = (1 + beta)**(-1) * (2 + beta)**(-1) * (3 + beta)**(-1) * (4+beta)**(-1) * Rs**(-4) * t**(1 + beta) * vo * ((2+beta) * (3+beta) * (4+beta) * a[0] * Rs**3 + 2 * t * vo * ((3 + beta) * (4 + beta) * a[1] * Rs**2 + 3 * t * vo * ((4 + beta) * a[2] * Rs + 4 * a[3] * t * vo)))
    force = generic_func * R1 * (T + Ka * generic_area_func_conv / A0)
    return force

def _region_2_cycle(t,g,a,tm,R1,vo,A0,T,Ka,beta):
    Rs=R1
    t = t.astype('complex')
    xi_r = vo*(2*tm - t)/Rs
    generic_func_retract = g[0] * xi_r + g[1] * xi_r**2+g[2] * xi_r**3 + g[3] * xi_r**4
    f1 = ((24+26*beta+9*beta**2+beta**3)*a[0]* Rs**3+2*t*vo*((12+7*beta+beta**2)*a[1]*Rs**2+3*t*vo*((4+beta)*a[2]*Rs+4*a[3]*t*vo)))
    f_2 = ((2+beta)*(3+beta)*(4+beta)*a[0]*Rs**3+vo*(2*(3+beta)*(4+beta)*a[1]*Rs**2*((-1)*t+(3+beta)*tm)+vo*(3*(4+beta)*a[2]*Rs*(2*t**2+(-2)*(5+beta)*t*tm+(14+beta*(7+beta))*tm**2)+4*a[3]*((-6)*t**3+6*(7+beta)*t**2*tm+(-3)*(34+beta*(11+beta))* t*tm**2+(5+beta)*(18+beta*(7+beta))*tm**3)*vo)))
    f_3 = (4*(4+beta)**(-1)*a[3]*(t+(-1)*tm)**3*vo**3+(-3)*(3+beta)**(-1)*(t+(-1)*tm)**2*vo**2*(a[2]*Rs+4*a[3]*t*vo)+2*(2+beta)**(-1)*(t+(-1)*tm)*vo*(a[1]*Rs**2+3*t*vo*(a[2]*Rs+2*a[3]*t*vo))+(-1)*(1+beta)**(-1)*(a[0]*Rs**3+t*vo*(2*a[1]*Rs**2+t*vo*(3*a[2]*Rs+4*a[3]*t*vo))))


    area_integral = (1+beta)**(-1)*(2+beta)**(-1)*(3+beta)**(-1)*(4+beta)**(-1)*Rs**(-4)*t**(1+beta)*vo*f1+(-1)*(1+beta)**(-1)*(2+beta)**(-1)*(3+beta)**(-1)*(4+beta)**(-1)*Rs**(-4)*(t+(-1)*tm)**(1+beta) *vo*f_2+Rs**(-4)*(t+(-1)*tm)**(1+beta)*vo*f_3

    force = generic_func_retract * R1 * (T + Ka * area_integral/A0)
    return force

def _region_2_dwell(t,g,a,tm,R1,vo,A0,T,Ka,beta):
    Rs = R1
    t = t.astype('complex')
    xi_r = (vo * tm)/Rs
    generic_func_retract = g[0]*xi_r+g[1]*xi_r**2+g[2]*xi_r**3+g[3]*xi_r**4
    area_integral = (1+beta)**(-1)*(2+beta)**(-1)*(3+beta)**(-1)*(4+beta)**(-1)*                                   Rs**(-4)*t**(1+beta)*vo*((24+26*beta+9*beta**2+beta**3)*a[0]* Rs**3+2*t*vo*((12+7*beta+beta**2)*a[1]*Rs**2+3*t*vo*((4+beta)*a[2]*Rs+4*a[3]*t*vo)))+Rs**(-4)*(t+(-1)*tm)**(1+beta)*vo*(4*(4+beta)**(-1)*a[3]*(t+(-1)*tm)**3*vo**3+(-3)*(3+beta)**(-1)*(t+(-1)*tm)**2*vo**2*(a[2]*Rs+4*a[3]*t*vo)+2*(2+beta)**(-1)*(t+(-1)*tm)*vo*(a[1]*Rs**2+3*t*vo*(a[2]*Rs+2*a[3]*t*vo))+(-1)*(1+beta)**(-1)*(a[0]*Rs**3+t*vo*(2*a[1]*Rs**2+t*vo*(3*a[2]*Rs+4*a[3]*t*vo))))

    force = generic_func_retract * R1 * (T + Ka * area_integral/A0)
    return force

def vec_fit_function_skalar_discher(time,g,a,R1,vo,A0,tm,T,Ka,beta,dwell):
    """
    the force curves with be already completely corrected when they come to this part
    """
    force = np.zeros(time.shape,dtype='complex')
    # region 1
    force+= (time<=tm) * _region_1(time,g,a,R1,vo,A0,T,Ka,beta)
    # region 2
    if dwell:
        force+= (time>tm) * _region_2_dwell(time,g,a,tm,R1,vo,A0,T,Ka,beta)
    else:
        force+= (time>tm) * _region_2_cycle(time,g,a,tm,R1,vo,A0,T,Ka,beta)
    # force is now complex, test with np.sum(np.abs(np.imag(force))) = 0, will just take real part
    return np.real(force) * 1000 # no in nN

def process(fname_variables):
    fname = fname_variables[0]
    dwell = variables[1]
    # load stuff
    # time, height, force

    # all data has been extracted
    time = time - time[0]
    #force = force - force[0]
    # remaking the height
    maxi = np.argmax(force)
    piezo_app = s0_velocity * time[:maxi+1]
    if dwell:
        piezo_back = s0_velocity * time[maxi] * np.ones(len(time[maxi+1:]))
    else:
        piezo_back = 2 * s0_velocity * time[maxi] - s0_velocity * time[maxi+1:]
    height = np.append(piezo_app,piezo_back)

    angle = 35
    Rp = 6.62
    R1 = 12 # radius cell?

    z_abs = height[np.argmax(force)]

    # ====================== Calc Generics / (later calc or load?)
    g, a, A0 = run_generate_generics(angle,Rp,R1,z_abs)

    tm = time[np.argmax(force)]
    run = True
    if run:
        '''
        to run the fit
        '''
        startpar = {'T': 0.0001, 'Ka': 0.015, 'beta': -0.6}
        constants = {'g' : g,  'a' : a, 'R1' : R1, 'vo' : s0_velocity,
                     'A0' : A0, 'tm' : tm,'dwell' : dwell}
        s = SimFit.SimFit(xvals = time, yvals = force,
                          func = vec_fit_function_skalar_discher,
                          startpar = startpar,
                          indep = 'time',
                          constant = constants,
                          #minpar = {'beta' : -1},
                          #maxpar = {'beta' : 0},
                          #tol = [1e-4, 1000],
                          cntrl = -1,
                          maxiter =1000)
        s.fittype = 'levenberg'
        s()
        par = s.curr_par
        fit_force = vec_fit_function_skalar_discher(time,g,a,R1,s0_velocity,
                                                    A0,tm,par[0],par[1],par[2],dwell)

def run_processes(paths,cores,
                  dwell = False):
    variables = [dwell,'important']
    for path in paths:
        print(path)
        fnames, _ = get_files(path)
        fnames_variables = [[fname,variables] for fname in fnames]
        p = Pool(cores)
        # returns file names of junk data
        stuff = list(tqdm(p.imap(process,fnames_variables),total = len(fnames)))
        p.close()
        # waits for everything to finish
        p.join()

