import sys
import os
import SimFit
import h5py
import json
import numpy as np
import time as real_time
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from tqdm import tqdm
from scipy.optimize import fsolve
from scipy.signal import resample
from scipy.integrate import quad
from numpy import pi as PI
from numpy import sin, cos, tan, sqrt
from numpy.lib import scimath
from numba import float64, njit, jit

from multiprocessing import Pool

from ftc_tools import get_files,fit_output_load, read_data_2be_fitted, read_s0_velocity, read_contact_point, read_baseline_corr, read_height, cell_load, simple_fit_function


'''
This is a translation from the matlab version as well as a script from Filip Savic

for the viscoelastic cone
'''

#======================================================================
# Since I do not know if numba will work in a class will have to be without one :(
#======================================================================
# The ViscoConeFit..
# =====================================================
# data processing functions
# =====================================================

def apply_fit_range(ori_time, ori_height, ori_force, fit_range):
    '''
    this function applies the fit range to the data
    '''
    # slicing to fit_range
    force = ori_force[(ori_time > fit_range[0])&(ori_time < fit_range[1])]
    height = ori_height[(ori_time > fit_range[0])&(ori_time < fit_range[1])]
    time = ori_time[(ori_time > fit_range[0])&(ori_time < fit_range[1])]
    return time, height, force

def data_reduction(time_in, force_in, height_in, reduction = 500):
    '''
    reduction : data reduction /  number or points left over for fitting
    --------------------------------------------------------------------
    notes : matlab downsample seems to just be array slicing
    '''
    data_length = len(time_in)
    # from matlab downsample seems to just be array slicing of the array by the downbyhowmuch value
    downbyhowmuch = np.floor(data_length / reduction)

    # change to int
    downbyhowmuch = downbyhowmuch.astype(int)
    #print(downbyhowmuch)

    # downsampling with array slicing
    time = time_in[::downbyhowmuch]
    force = force_in[::downbyhowmuch]
    height = height_in[::downbyhowmuch]
    # here there is come sogolayfilt in matlab but the data is overriden
    # this method does not work, from scipy.signal leads to errors
    # time = resample(time_in,downbyhowmuch)
    # force = resample(force_in,downbyhowmuch)
    return time, force, height

def get_data(file):
    '''
    get all the required data and the fit range
    from the hdf5 file
    '''
    time, force = read_baseline_corr(file)
    height = read_height(file)
    contact_point = read_contact_point(file)
    s0_velocity = read_s0_velocity(file)
    return time, height, force, contact_point, s0_velocity

# save fit data into hdf5
def save_fit(fname,
             fit_parameter,
             fitted_time,
             fitted_height,
             fitted_force,
             fit_force,
             fit_range_factor):
    # fit data
    # fit_force = discher_final_function_visco(height,par[0],par[1],par[2],r1,phi)
    # open h5df file
    file = h5py.File(fname,'r+')

    # check if fitparameter are already saved and  delete them
    if 'simple_fit_parameter' in file:
        del file['simple_fit_parameter']
    if 'simple_fit_force' in file:
        del file['simple_fit_force']  # from fit
        del file['simple_fitted_force'] # force data that is fitted
        del file['simple_fitted_height']
        del file['simple_fitted_time']

    # save the data to hdf5
    file.create_dataset('simple_fit_force', data = fit_force*1e3) # now in nN
    file.create_dataset('simple_fitted_force', data = fitted_force*1e3) # now in nN
    file.create_dataset('simple_fitted_height', data = fitted_height)
    file.create_dataset('simple_fitted_time', data = fitted_time)
    file.create_dataset('simple_fit_parameter',data = fit_parameter)
    #file.attrs['simple_data_reduction_factor'] = data_reduction
    file.attrs['simple_fit_range_factor'] = fit_range_factor

    file.close()

def run_fit(time,force,velocity):
    '''
    run the fit
    '''
    startpar = {'vorfak': 1e-4, 'beta': -0.5, 'd' : 0.0001}
    constants = {'velocity' : velocity,  'force' : force}
    s = SimFit.SimFit(xvals = time,yvals = force,
                      func = simple_fit_function,
                      startpar = startpar,
                      indep = 'time',
                      constant = constants,
                      #minpar = {'d':0},
                      #maxpar = {'beta' : 1},
                      #tol = [1e-4, 1e-4],
                      cntrl = -1,
                      maxiter =500)
    s.fittype = 'levenberg'
    s()
    par =  s.curr_par
    return par

def calculate_parameters(d,vorfak,theta,A0=452,calc_area = False):
    T = d / (2 * PI * cos(theta)) # microNewton / m
    if calc_area:
        Ka0 = vorfak * tan(theta)**2 * A0 /(2 * PI**2 * (1 - cos(theta))) # microNewton / m
    else:
        Ka0 = vorfak * tan(theta)**2 /(2 * PI**2 * (1 - cos(theta))) # microNewton / m

    return T * 1e-3, Ka0 * 1e-6 # mN/m, N/m

def plot_fit(time,height,force, par, fname,ori_force,ori_time,ori_height, contact_point, velocity):
    '''
    just to check the fits
    '''
    #plt.ion() # interactive
    # generate fitted force
    fit_force = simple_fit_function(time,force,velocity,*par)
    # stuff force plots
    _height = ori_height[ori_time >= contact_point]
    # plot
    fig, ax = plt.subplots()
    ln1 = ax.plot(_height[0] - ori_height, ori_force * 1e-3, '.', label = 'data')
    ln2 = ax.plot(height, fit_force, linewidth = 3,label = 'fit')

    # labels
    ax.set_xlabel('indentation depth / µm')
    ax.set_ylabel('force / µN')

    # fit parameter as text
    ax.text(0.1, 0.4, f'Vorfaktor = {str(par[0])}\nbeta = {str(par[1])}\nd= {str(par[2])}', fontsize=12, transform = ax.transAxes)

    # legend
    lns = ln1 + ln2
    labs = [l.get_label() for l in lns]
    ax.legend(lns,labs)
    plt.title(f'simple_{os.path.basename(fname)[:-25]}')
    figure_name = f'simple_{os.path.basename(fname)[:-25]}.png'
    plt.savefig(os.path.join(os.path.dirname(fname),figure_name),dpi=150)
    #plt.savefig
    plt.tight_layout()
    #plt.show()
    plt.close()

    return fit_force

def main_process(fname):
    # get hdf5 file and data
    file = h5py.File(fname,'r')
    #print(fname)
    # check if curve was considered
    if file.attrs['trace_ok'] == 'yes':
        # get required data from the file
        time, height, force, contact_point, s0_velocity = get_data(file)
        # ---------------------------check if baseline is correct
        if force.max() < maximum_force*0.5:
            file.close()
            return fname
        # -----------------------------
        if (fit_range_type == 'from file') & ('fit_range' in file.attrs):
            fit_range = file.attrs['fit_range']
        else:
            fit_range = [0,0]
            index_max = np.argmax(force)
            fit_range[0] = 0
            fit_range[1] = time[(force > force[index_max] * (1-value)) & (time > time[index_max])].max()
        # data is now adjusted
        file.close()
        time, height, force = apply_fit_range(time, height, force, fit_range)

        # -------------------------saving copy------------
        ori_force = force.copy()
        ori_height = height.copy()
        ori_time = time.copy()
        # the fit only fits the indentation
        force = force[time >= contact_point]
        height = height[time >= contact_point]
        time = time[time >= contact_point]
        # shift height to 0 and change
        time = time - time[0]
        height = height[0] - height

        # reduce data points
        #data_reduction_factor = 300
        #time, force, height = data_reduction(time, force, height,data_reduction_factor)
        force = force * 1e-3
        # ------------------------ fit part-------------------
        par = run_fit(time,force,s0_velocity)

        fit_force = plot_fit(time,
                             height,
                             force,
                             par,
                             fname,
                             ori_force,
                             ori_time,
                             ori_height,
                             contact_point,
                             s0_velocity)

        save_fit(fname,par,time,height,force,fit_force,value)

# ------------------------------- for testing----------------------

def calculate_fit_parameters(d,vorfak,theta,A0=452,calc_area = False):
    T = d / (2 * PI * cos(theta)) # microNewton / m
    if calc_area:
        Ka0 = vorfak * tan(theta)**2 * A0 /(2 * PI**2 * (1 - cos(theta))) # microNewton / m
    else:
        Ka0 = vorfak * tan(theta)**2 /(2 * PI**2 * (1 - cos(theta))) # microNewton / m

    return T * 1e-3, Ka0 * 1e-6 # mN/m, N/m


# -----------------------------some settings---------------------------------
run_multi_processing = True

show_time = True  # value to show the time it takes
# either 'from file' or 'not from file'
fit_range_type = 'not from file'
# percentage of the maximal force kept for the fitting
global value
value = 0.3
# some choices
fit_from_choices = {'all curves' : 0,
                    'fit output list': 1,
                    'cell_wise': 2}
# choose it
fit_from = fit_from_choices['all curves']

# choose R1
global para_from_file
para_from_file = False

# maximum force
global maximum_force
maximum_force = 1
# general parameters
canti_angle = 17.5
theta = 120 - 90 - canti_angle
#============================run part here ==============================
paths = []
paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110310/map-data-2011.03.10-11.16.05/eval_2020_02_04_20_00')
paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110310/map-data-2011.03.10-12.56.03/eval_2020_03_23_11_23')
#paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110310/map-data-2011.03.10-14.32.08/eval_2020_03_26_14_30')

if run_multi_processing:
    for path in paths:
        # use a shorter list of file names
        if fit_from == 1:
            fnames = fit_output_load(path)
        # list of all file names in folder
        elif fit_from == 0:
            fnames, _ = get_files(path)
        elif fit_from == 2:
            fnames = cell_load(path)
        t0 = real_time.time()
        p = Pool(3)
        stuff = list(tqdm(p.imap(main_process,fnames),total = len(fnames)))
        p.close()
        # waits for everything to finish
        p.join()
        t1 = real_time.time()
        t = t1 - t0
        print(t)

        file_name = path + '/simple_fit_output.txt'
        stuff = list(filter(None, stuff))
        output = {}
        output['false_baselines'] = stuff

        with open(file_name, 'w+') as outfile:
            json.dump(output, outfile)
else:
    index = 0
    fname_i = 4
    path = paths[index]
    fnames, _ = get_files(path)
    fname = fnames[fname_i]

    #main_process(fname)

    A0 = PI * 12**2  # micrometer **2
    expected_Ka = 0.0127   # microN / micr meter

    expected_T0 = 0.203 * 1e-3 # micro T0 / micro meter

    file = h5py.File(fname,'r')
    #print(fname)
    # check if curve was considered
    if file.attrs['trace_ok'] == 'yes':
        # get required data from the file
        time, height, force, contact_point, s0_velocity = get_data(file)
        # ---------------------------check if baseline is correct
        if force.max() < maximum_force*0.5:
            file.close()
        # -----------------------------
        if (fit_range_type == 'from file') & ('fit_range' in file.attrs):
            fit_range = file.attrs['fit_range']
        else:
            fit_range = [0,0]
            index_max = np.argmax(force)
            fit_range[0] = 0
            fit_range[1] = time[(force > force[index_max] * (1-value)) & (time > time[index_max])].max()
        # data is now adjusted
        file.close()
        time, height, force = apply_fit_range(time, height, force, fit_range)

        # -------------------------saving copy------------
        ori_force = force.copy()
        ori_height = height.copy()
        ori_time = time.copy()
        # the fit only fits the indentation
        force = force[time >= contact_point]
        height = height[time >= contact_point]
        time = time[time >= contact_point]
        # shift height to 0 and change
        time = time - time[0]
        height = height[0] - height

        # reduce data points
        #data_reduction_factor = 300
        #time, force, height = data_reduction(time, force, height,data_reduction_factor)
        force = force * 1e-3

        plt.ion()

