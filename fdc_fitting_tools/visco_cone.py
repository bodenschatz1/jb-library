import numpy as np
import os
import SimFit
import h5py
import time as real_time
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from tqdm import tqdm
from scipy.optimize import fsolve
from scipy.signal import resample
from scipy.integrate import quad
from numpy import pi as PI
from numpy import sin, cos, tan, sqrt
from numpy.lib import scimath
from numba import float64, njit, jit

from multiprocessing import Pool

from ftc_tools import get_files, read_data_2be_fitted, read_s0_velocity, read_contact_point, read_baseline_corr, read_height


'''
This is a translation from the matlab version as well as a script from Filip Savic

for the viscoelastic cone
'''

#======================================================================
# Since I do not know if numba will work in a class will have to be without one :(
#======================================================================
# The ViscoConeFit..

# ---------------------------------------------------------------------
# function to run everything
# ---------------------------------------------------------------------
def run_everything(height,
                   force,
                   fnames,
                   index,
                   ori_force,
                   oir_time,
                   ori_height,
                   contact_point,
                   data_reduction_factor,
                   value):

    parameter = run_fit(height,force)

    fit_force = plot_to_check(height,
                                  force,
                                  parameter,
                                  fnames[index],
                                  ori_force,
                                  ori_time,
                                  ori_height,
                                  contact_point)

    save_fit(fnames[index],
             parameter,
             time,
             height,
             force,
             fit_force,
             data_reduction_factor,
             value)


# =====================================================
# data processing functions
# =====================================================

def apply_fit_range(ori_time, ori_height, ori_force, fit_range):
    '''
    this function applies the fit range to the data
    '''
    # slicing to fit_range
    force = ori_force[(ori_time > fit_range[0])&(ori_time < fit_range[1])]
    height = ori_height[(ori_time > fit_range[0])&(ori_time < fit_range[1])]
    time = ori_time[(ori_time > fit_range[0])&(ori_time < fit_range[1])]
    return time, height, force

def data_reduction(time_in, force_in, height_in, reduction = 500):
    '''
    reduction : data reduction /  number or points left over for fitting
    --------------------------------------------------------------------
    notes : matlab downsample seems to just be array slicing
    '''
    data_length = len(time_in)
    # from matlab downsample seems to just be array slicing of the array by the downbyhowmuch value
    downbyhowmuch = np.floor(data_length / reduction)

    # change to int
    downbyhowmuch = downbyhowmuch.astype(int)
    #print(downbyhowmuch)

    # downsampling with array slicing
    time = time_in[::downbyhowmuch]
    force = force_in[::downbyhowmuch]
    height = height_in[::downbyhowmuch]
    # here there is come sogolayfilt in matlab but the data is overriden
    # this method does not work, from scipy.signal leads to errors
    # time = resample(time_in,downbyhowmuch)
    # force = resample(force_in,downbyhowmuch)
    return time, force, height

def get_data(file):
    '''
    get all the required data and the fit range
    from the hdf5 file
    '''
    time, force = read_baseline_corr(file)
    height = read_height(file)
    contact_point = read_contact_point(file)
    s0_velocity = read_s0_velocity(file)
    #file.close()
    return time, height, force, contact_point, s0_velocity

# save fit data into hdf5
def save_fit(fname,
             fit_parameter,
             fitted_time,
             fitted_height,
             fitted_force,
             fit_force,
             data_reduction,
             fit_range_factor):
    # fit data
    # fit_force = discher_final_function_visco(height,par[0],par[1],par[2],r1,phi)
    # open h5df file
    file = h5py.File(fname,'a')

    # check if fitparameter are already saved and  delete them
    if 'fit_parameter' in file:
        del file['fit_parameter']
    if 'fit_force' in file:
        del file['fit_force']  # from fit
        del file['fitted_force'] # force data that is fitted
        del file['fitted_height']
        del file['fitted_time']

    # save the data to hdf5
    file.create_dataset('fit_force', data = fit_force*1e3) # now in nN
    file.create_dataset('fitted_force', data = fitted_force*1e3) # now in nN
    file.create_dataset('fitted_height', data = fitted_height)
    file.create_dataset('fitted_time', data = fitted_time)
    file.create_dataset('fit_parameter',data = fit_parameter)
    file.attrs['data_reduction_factor'] = data_reduction
    file.attrs['fit_range_factor'] = fit_range_factor

    file.close()

# ----------------------------------------------------------------
# Parameters
# ----------------------------------------------------------------
#vo = 2 # velocity in micrometer pro sec FILLER
angle = 35 #contact angle before indentation
R1 = 12 # radius cell?
theta_cone = 17.5 # opening angle for Indenter
Phio = angle * PI / 180 # contact angle in radians
theta = (PI/2) - theta_cone * PI / 180 # correction Discher paper
Ro = R1 / sin(Phio)
h = Ro - sqrt(Ro ** 2 - R1 ** 2) # height of sphere cap
Vkk = (1/3) * h ** 2 * PI * (3 * Ro - h) # volume of sphere cap
A0 = 2 * PI * Ro * h # surface area of sphere cap
r1 = 0.0001 # start parameter radius in micro meter
phi = Phio # start parameter 2
# -------------------------------------------------------------------------
# Functions needed
# -------------------------------------------------------------------------
@njit(float64(float64, float64), cache = True)
def A(r1, phi):
    '''
    Eq. A5(1) in ref. 1
    '''
    f1 = R1 * sin(phi) + r1 * sin(theta)
    f2 = R1 ** 2 - r1 ** 2
    return f1 / f2

@njit(float64(float64, float64), cache=True)
def B(r1, phi):
    """
    Eq. A5(2) in ref. 1
    """
    return -A(r1, phi) * r1**2 - r1 * sin(theta)
# for arrays
@njit(float64[:](float64[:], float64[:]), cache = True)
def A_array(r1, phi):
    '''
    Eq. A5(1) in ref. 1
    '''
    f1 = R1 * sin(phi) + r1 * sin(theta)
    f2 = R1 ** 2 - r1 ** 2
    return f1 / f2

# -------------------------------------------------------------------------
# the integrads
# -------------------------------------------------------------------------

@njit(float64(float64, float64, float64), cache=True)
def cyl(r, r1, phi):
    """
    Some other function I don't see the point of right now.
    """
    _A = A(r1, phi)
    _B = B(r1, phi)
    _C = _A * r + _B / r

    return _C / sqrt(1 - _C**2)

@njit(float64(float64, float64, float64), cache=True)
def volume_integrand(r, r1, phi):
    """
    The integrand in eq. A6, Sen, Discher, 2005, Biophys. J. 89(5) 3203-3213

    matlab calls this vol
    """
    return PI * r**2 * cyl(r, r1, phi)

@njit(float64(float64, float64, float64), cache=True)
def area_integrand(r, r1, phi):
    """
    The integrand in eq. A10, ref. 1

    called surface discher in the matlab code
    """
    _A = A(r1, phi)
    _B = B(r1, phi)
    _C = _A * r + _B / r

    return (2 * PI * r) / sqrt(1 - _C**2)

# ------------------------------------------------------------------------------
# The integrals using quad from scipy
# ------------------------------------------------------------------------------
def volume(r1, phi):
    """
    Calculate the volume using eq. A6 in ref. 1
    """
    V1 = quad(volume_integrand,
              r1,
              R1,
              args=(r1, phi))[0]

    return V1 - (PI * r1**3 * tan(theta)) / 3

def area(r1, phi):
    """
    Calculate the volume using eq. A10 in ref. 1
    """
    A1 = quad(area_integrand,
              r1,
              R1,
              args=(r1, phi))[0]

    return A1 + (PI * r1**2) / cos(theta)

def some_height(r1, phi):
    """
    One integral appearing in jonathan's thingy.
    """
    K1 = quad(cyl,
              r1,
              R1,
              args=(r1, phi))[0]

    return K1 - r1 * tan(theta)

# --------------------------------------------------------------
# here comes the equation system
# -------------------------------------------------------------
def equation_system(xs,z_indexed):
    r1 = xs[0]
    phi = xs[1]

    # Get volume
    _V = volume(r1, phi)

    # Get area
    #_A = area(r1, phi) # unused

    # Get this weird height
    _h = some_height(r1, phi)

    # Derived things.
    #_alpha = (_A - A0) / A0 # unused
    _delta = h - _h

    # Construct output
    ret_1 = Vkk - _V
    ret_2 = z_indexed - _delta

    return [ret_1, ret_2]

# ---------------------------------------------------------------------
# here comes the other stuff / directly transfered from matlab
# ---------------------------------------------------------------------

def discher_function_visco(r1,phi,z,p):
        T = p[0]
        Ka = p[1]

        #beta = p[2] # unused
        x0 = [r1, phi]
        xs = fsolve(equation_system, x0, z, xtol=1e-8)

        # some adjustments
        r1 = xs[0]
        phi = xs[1]

        # get area
        Area = area(r1,phi)
        alpha = (Area - A0) / A0

        # calculate the force / force_out in matlab <= DOESNT SEEM TO DO ANYTHING
        force = ((2 * PI * R1 ** 2 * A(r1,phi) - 2 * PI * R1 * sin(phi)) * (T + Ka*alpha))

        return force, r1, phi, Area

def discher_final_function_visco(z, T, Ka, beta, r1, phi):
    '''
    z    : height
    T    : p1
    Ka   : p2
    beta : p3
    '''
    # time must be global
    p = [T, Ka, beta]
    # stuff array [area, Tfull, r1_i, phi_i]
    stuff = np.full([len(z),5],np.nan)
    # setting first element
    stuff[0,0] = A0   # must be global as well
    stuff[0,1] = T
    stuff[0,2] = r1   # must be global
    stuff[0,3] = phi
    # for loop that goes through elements starting at element 1
    for i in range(1,len(z)):
        force, r1, phi, Area = discher_function_visco(r1,phi,z[i],p)
        stuff[i,0] = Area
        mem_term = (time[i] - time[:i-1]) ** (-beta)
        v_term = np.diff(stuff[:i,0]) / A0
        stress_now = np.sum(Ka * mem_term * v_term)
        stuff[i,1] = T + stress_now
        stuff[i,2] = r1
        stuff[i,3] = phi
        stuff[i,4] = force

    force =  ((2 * PI * R1 ** 2 * A_array(stuff[:,2],stuff[:,3]) - 2 * PI * R1 * sin(stuff[:,3]))* stuff[:,1])
    #force = stuff[:,4]
    return force

def run_fit(height,force):
    '''
    to run the fit
    '''
    startpar = {'T': 0.0003, 'Ka': 0.01, 'beta': 0.6}
    constants = {'r1' : r1,  'phi' : phi}
    s = SimFit.SimFit(xvals = height,yvals = force,
                      func = discher_final_function_visco,
                      startpar = startpar,
                      indep = 'z',
                      constant = constants,
                      #minpar = {'T' : 0, 'Ka' : 0, 'beta' : 0},
                      #maxpar = {'beta' : 1},
                      #tol = [1e-4, 1e-4],
                      cntrl = -1,
                      maxiter =50)
    s.fittype = 'levenberg'
    s()
    return s.curr_par




def plot_to_check(height,force, par, fname,ori_force,ori_time,ori_height, contact_point):
    '''
    just to check the fits
    '''
    #plt.ion() # interactive
    # generate fitted force
    fit_force = discher_final_function_visco(height,par[0],par[1],par[2],r1,phi)
    # stuff force plots
    _height = ori_height[ori_time >= contact_point]
    # plot
    fig, ax = plt.subplots()
    ln1 = ax.plot(_height[0] - ori_height, ori_force * 1e-3, '.', label = 'data')
    ln2 = ax.plot(height, fit_force, linewidth = 3,label = 'fit')

    # labels
    ax.set_xlabel('indentation depth / µm')
    ax.set_ylabel('force / µN')

    # fit parameter as text
    ax.text(0.1, 0.4, 'T = '+str(par[0]*1e3)+' mN/m \nK$_A$ = '+str(par[1]) + ' N/m\n$\\beta$ = ' + str(par[2]), fontsize=12, transform = ax.transAxes)

    # legend
    lns = ln1 + ln2
    labs = [l.get_label() for l in lns]
    ax.legend(lns,labs)
    plt.title(os.path.basename(fname)[:-25])
    figure_name = fname[:-25]+'.png'
    plt.savefig(figure_name,dpi=150)
    #plt.savefig
    plt.tight_layout()
    #plt.show()

    return fit_force

# ----------------------------------------------------------
# functions to test stuff till it works
# ----------------------------------------------------------
#plt.ion()
p = [0.0003,0.01,0.5]

def run_fit_testing(height,force,
                    startpar = {'T' : 0.0003, 'Ka' : 0.01, 'beta' : 0.6},
                    fittype='levenberg',
                    maxiter=5):
    '''
    to run the fit
    '''
    import time
    t0 = time.time()
    constants = {'r1' : r1,  'phi' : phi}
    s = SimFit.SimFit(xvals = height,yvals = force,
                      func = discher_final_function_visco,
                      startpar = startpar,
                      indep = 'z',
                      constant = constants,
                      #minpar = {'T' : 0, 'Ka' : 0, 'beta' : 0},
                      #maxpar = {'beta' : 1},
                      #tol = [1e-4, 1e-4],
                      cntrl = -1,
                      maxiter = maxiter)
    s.fittype = fittype
    s()
    t1 = time.time()
    t = t1 - t0
    print(t)
    #plot_to_check(time,height,force,s.curr_par)

    return s.curr_par, t

def plot_fit(height,force, par, fname,ori_force,ori_time,ori_height, contact_point):
    '''
    just to check the fits
    '''
    #plt.ion() # interactive
    # generate fitted force
    fit_force = discher_final_function_visco(height,par[0],par[1],par[2],r1,phi)
    # stuff force plots
    _height = ori_height[ori_time >= contact_point]
    # plot
    fig, ax = plt.subplots()
    ln1 = ax.plot(_height[0] - ori_height, ori_force * 1e-3, '.', label = 'data')
    ln2 = ax.plot(height, fit_force, linewidth = 3,label = 'fit')

    # labels
    ax.set_xlabel('indentation depth / µm')
    ax.set_ylabel('force / µN')

    # fit parameter as text
    ax.text(0.1, 0.4, 'T = '+str(par[0]*1e3)+' mN/m \nK$_A$ = '+str(par[1]) + ' N/m\n$\\beta$ = ' + str(par[2]), fontsize=12, transform = ax.transAxes)

    # legend
    lns = ln1 + ln2
    labs = [l.get_label() for l in lns]
    ax.legend(lns,labs)
    plt.title(os.path.basename(fname)[:-25])
    figure_name = fname[:-25]+'.png'
    #plt.savefig(figure_name,dpi=150)
    #plt.savefig
    plt.tight_layout()
    #plt.show()

    return fit_force


# -----------------------------some settings---------------------------------
show_time = True  # value to show the time it takes
# either 'from file' or 'not from file'
fit_range_type = 'not from file'
# percentage of the maximal force kept for the fitting
value = 0.3
#============================run part here ==============================
paths = []
paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110310/map-data-2011.03.10-11.16.05/eval_2020_02_04_20_00')

# iterate through paths
for path in paths:
    # list of file names
    fnames, _ = get_files(path)
    # iterate through files
    #for index in tqdm(range(len(fnames))):
    # for testing
    for index in range(4,5):
        # index = 2
        # get hdf5 file and data
        file = h5py.File(fnames[index],'r')
        # check if curve was considered
        if file.attrs['trace_ok'] == 'no':
            continue
        # get fit range from file
        #if 'fit_range' in file.attrs:    # in the future there might be files without this saved
            #fit_range = file.attrs['fit_range']
        # get required data from the file and close it
        time, height, force, contact_point, s0_velocity = get_data(file)
        # ----------------------------------
        if (fit_range_type == 'from file') & ('fit_range' in file.attrs):
            fit_range = file.attrs['fit_range']
            #time, height, force = apply_fit_range(time, height, force, fit_range)
        else:
            fit_range = [0,0]
            index_max = np.argmax(force)
            fit_range[0] = 0
            fit_range[1] = time[(force > force[index_max] * (1-value)) & (time > time[index_max])].max()
        # data is now adjusted
        time, height, force = apply_fit_range(time, height, force, fit_range)
        # all data has been extracted
        file.close()
        # -------------------------saving copy------------
        ori_force = force.copy()
        ori_height = height.copy()
        ori_time = time.copy()
        # the fit only fits the indentation
        force = force[time >= contact_point]
        height = height[time >= contact_point]
        time = time[time >= contact_point]
        # shift height to 0 and change
        height = height[0] - height
        # reduce data points
        data_reduction_factor = 300
        time, force, height = data_reduction(time, force, height,data_reduction_factor)
        # according to matlab the script the force is in microNewton
        force = force * 1e-3
        if show_time:
            t0 = real_time.time()
        parameter = run_fit(height,force)
        fit_force = plot_to_check(height,
                                  force,
                                  parameter,
                                  fnames[index],
                                  ori_force,
                                  ori_time,
                                  ori_height,
                                  contact_point)

        save_fit(fnames[index],
                 parameter,
                 time,
                 height,
                 force,
                 fit_force,
                 data_reduction_factor,
                 value)
        if show_time:
            t1 = real_time.time()
            print('time spent: ' + str(t1-t0))

