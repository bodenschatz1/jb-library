import sys
import os
import SimFit
import json
import numpy as np
import time as real_time
import matplotlib.pyplot as plt
from tqdm import tqdm
from scipy.optimize import fsolve
from scipy.signal import resample
from scipy.integrate import quad
from numpy import pi as PI
from numpy import sin, cos, tan, sqrt
from numpy.lib import scimath
from numba import float64, njit, jit

from multiprocessing import Pool

'''
This is a translation from the matlab version as well as a script from Filip Savic

for the viscoelastic cone
'''

#------------------ data processing functions-----------------------

# get data function here!

# save fit data function here!

# -------------------- some helpful functions ------------------------
def apply_fit_range(ori_time,ori_height,ori_force,value=0.3):
    '''
    this function finds a fit range and then applies it to the data
    - value is a percetatge of the maximum force that is used
    '''
    fit_range = [0,0]
    index_max = np.argmax(force)
    fit_range[0] = 0
    fit_range[1] = time[(force > force[index_max] * (1-value)) & (time > time[index_max])].max()

    force = ori_force[(ori_time > fit_range[0])&(ori_time < fit_range[1])]
    height = ori_height[(ori_time > fit_range[0])&(ori_time < fit_range[1])]
    time = ori_time[(ori_time > fit_range[0])&(ori_time < fit_range[1])]
    return time, height, force

# get index
def _find_contact_point_index(time, contact_point):
        return np.abs(time-contact_point).argmin()

def data_reduction(time,height,force,contact_point,reduction):
    '''
    a data reduction that also includes a contact point
    and shifts to 0
    '''
    data_length = len(time)
    # from matlab downsample seems to just be array slicing of the array by the downbyhowmuch value
    downbyhowmuch = np.floor(data_length / reduction)

    # change to int
    downbyhowmuch = downbyhowmuch.astype(int)

    # cp index
    index_cp = _find_contact_point_index(time,contact_point)

    time = time[index_cp::downbyhowmuch]
    height = height[index_cp::downbyhowmuch]
    force = force[index_cp::downbyhowmuch]

    # shift to 0
    time = time - time[0]

    return time, height, force

def piezo(time,force,velocity,dwell=False):
    '''
    adjust height
    '''
    maxi = np.argmax(force)
    piezo_app = velocity * time[:maxi+1]
    if dwell:
        piezo_back = velocity * time[maxi] * np.ones(len(time[maxi+1:]))
    else:
        piezo_back = 2 * velocity * time[maxi] - velocity * time[maxi+1:]
    height = np.append(piezo_app,piezo_back)

    return height


# ------------------ Parameters -------------------------------------
angle = 35 #contact angle before indentation
R1 = 12 # radius cell?
theta_cone = 17.5 # opening angle for Indenter
Phio = angle * PI / 180 # contact angle in radians
theta = (PI/2) - theta_cone * PI / 180 # correction Discher paper
Ro = R1 / sin(Phio)
h = Ro - sqrt(Ro ** 2 - R1 ** 2) # height of sphere cap
Vkk = (1/3) * h ** 2 * PI * (3 * Ro - h) # volume of sphere cap
A0 = 2 * PI * Ro * h # surface area of sphere cap
r1 = 0.0001 # start parameter radius in micro meter
phi = Phio # start parameter 2

# ---------------------- Functions needed --------------------------------
@njit(float64(float64, float64), cache = True)
def A(r1, phi):
    '''
    Eq. A5(1) in ref. 1
    '''
    f1 = R1 * sin(phi) + r1 * sin(theta)
    f2 = R1 ** 2 - r1 ** 2
    return f1 / f2

@njit(float64(float64, float64), cache=True)
def B(r1, phi):
    """
    Eq. A5(2) in ref. 1
    """
    return -A(r1, phi) * r1**2 - r1 * sin(theta)
# for arrays
@njit(float64[:](float64[:], float64[:]), cache = True)
def A_array(r1, phi):
    '''
    Eq. A5(1) in ref. 1
    '''
    f1 = R1 * sin(phi) + r1 * sin(theta)
    f2 = R1 ** 2 - r1 ** 2
    return f1 / f2

# ---------------------------------the integrads -----------------------------------
@njit(float64(float64, float64, float64), cache=True)
def cyl(r, r1, phi):
    """
    Some other function I don't see the point of right now.
    """
    _A = A(r1, phi)
    _B = B(r1, phi)
    _C = _A * r + _B / r

    return _C / sqrt(1 - _C**2)

@njit(float64(float64, float64, float64), cache=True)
def volume_integrand(r, r1, phi):
    """
    The integrand in eq. A6, Sen, Discher, 2005, Biophys. J. 89(5) 3203-3213

    matlab calls this vol
    """
    return PI * r**2 * cyl(r, r1, phi)

@njit(float64(float64, float64, float64), cache=True)
def area_integrand(r, r1, phi):
    """
    The integrand in eq. A10, ref. 1

    called surface discher in the matlab code
    """
    _A = A(r1, phi)
    _B = B(r1, phi)
    _C = _A * r + _B / r

    return (2 * PI * r) / sqrt(1 - _C**2)

# ------------ The integrals using quad from scipy --------------------------------
def volume(r1, phi):
    """
    Calculate the volume using eq. A6 in ref. 1
    """
    V1 = quad(volume_integrand,
              r1,
              R1,
              args=(r1, phi))[0]

    return V1 - (PI * r1**3 * tan(theta)) / 3

def area(r1, phi):
    """
    Calculate the volume using eq. A10 in ref. 1
    """
    A1 = quad(area_integrand,
              r1,
              R1,
              args=(r1, phi))[0]

    return A1 + (PI * r1**2) / cos(theta)

def some_height(r1, phi):
    """
    One integral appearing in jonathan's thingy.
    """
    K1 = quad(cyl,
              r1,
              R1,
              args=(r1, phi))[0]

    return K1 - r1 * tan(theta)

# -------------- here comes the equation system -------------------
def equation_system(xs,z_indexed):
    r1 = xs[0]
    phi = xs[1]

    # Get volume
    _V = volume(r1, phi)

    # Get area
    #_A = area(r1, phi) # unused

    # Get this weird height
    _h = some_height(r1, phi)

    # Derived things.
    #_alpha = (_A - A0) / A0 # unused
    _delta = h - _h

    # Construct output
    ret_1 = Vkk - _V
    ret_2 = z_indexed - _delta

    return [ret_1, ret_2]

#  ---------------- here comes the other stuff / directly transfered from matlab -----------------
def discher_function_visco(r1,phi,z,p):
        T = p[0]
        Ka = p[1]

        #beta = p[2] # unused
        x0 = [r1, phi]
        xs = fsolve(equation_system, x0, z, xtol=1e-8)

        # some adjustments
        r1 = xs[0]
        phi = xs[1]

        # get area
        Area = area(r1,phi)
        alpha = (Area - A0) / A0

        # calculate the force / force_out in matlab <= DOESNT SEEM TO DO ANYTHING
        force = ((2 * PI * R1 ** 2 * A(r1,phi) - 2 * PI * R1 * sin(phi)) * (T + Ka*alpha))

        return force, r1, phi, Area

def discher_final_function_visco(z,T,Ka,beta,r1,phi):
    '''
    z    : height
    T    : p1
    Ka   : p2
    beta : p3
    '''
    # time must be global
    p = [T, Ka, beta]
    # stuff array [area, Tfull, r1_i, phi_i]
    stuff = np.full([len(z),5],np.nan)
    # setting first element
    stuff[0,0] = A0   # must be global as well
    stuff[0,1] = T
    #stuff[0,2] = r1   # must be global
    #stuff[0,3] = phi
    stuff[0,2] = 0   # this is closest to matlab
    stuff[0,3] = 0

    # for loop that goes through elements starting at element 1
    for i in range(1,len(z)):
        force, r1, phi, Area = discher_function_visco(r1,phi,z[i],p)
        stuff[i,0] = Area
        mem_term = (time[i] - time[:i]) ** (-beta)
        v_term = np.diff(stuff[:i+1,0]) / A0
        stress_now = np.sum(Ka * mem_term * v_term)
        stuff[i,1] = T + stress_now
        stuff[i,2] = r1
        stuff[i,3] = phi
        stuff[i,4] = force

    force =  ((2 * PI * R1 ** 2 * A_array(stuff[:,2],stuff[:,3]) - 2 * PI * R1 * sin(stuff[:,3]))* stuff[:,1])
    #force = stuff[:,4]
    return force

def run_fit(height,force):
    '''
    to run the fit
    '''
    startpar = {'T': 0.0003, 'Ka': 0.01, 'beta': 0.6}
    constants = {'r1' : r1,  'phi' : phi}
    s = SimFit.SimFit(xvals = height,yvals = force,
                      func = discher_final_function_visco,
                      startpar = startpar,
                      indep = 'z',
                      constant = constants,
                      #minpar = {'T' : 0, 'Ka' : 0, 'beta' : 0},
                      #maxpar = {'beta' : 1},
                      #tol = [1e-4, 1e-4],
                      cntrl = -1,
                      maxiter =500)
    s.fittype = 'levenberg'
    s()
    return s.curr_par




def plot_fit(time,height,force,par,fname,xaxis='time', show = False, save=True):
    # generate fitted force
    fit_force = discher_final_function_visco(height,par[0],par[1],par[2],r1,phi)
    # plot
    fig, ax = plt.subplots()
    if xaxis == 'time':
        ln1 = ax.plot(time, force, '.', label = 'data')
        ln2 = ax.plot(time, fit_force, linewidth = 3,label = 'fit')
        # x label
        ax.set_xlabel('time / s')
    else:
        ln1 = ax.plot(height, force, '.', label = 'data')
        ln2 = ax.plot(height, fit_force, linewidth = 3,label = 'fit')
        # x label
        ax.set_xlabel('indentation depth / µm')

    # y label
    ax.set_ylabel('force / µN')

    # fit parameter as text
    ax.text(0.1, 0.4, 'T = '+str(par[0]*1e3)+' mN/m \nK$_A$ = '+str(par[1]) + ' N/m\n$\\beta$ = ' + str(par[2]), fontsize=12, transform = ax.transAxes)

    # legend
    lns = ln1 + ln2
    labs = [l.get_label() for l in lns]
    ax.legend(lns,labs)
    # title please adjust
    plt.title(os.path.basename(fname)[:-25])
    # figure_name please adjust
    figure_name = fname + '.png'
    if save:
        plt.savefig(figure_name,dpi=150)
    if show:
        plt.show()
    else:
        plt.close()

    return fit_force

# ----------------------------------- multiprocessing
def main_process(fname):
    '''
    this is the function that multiprocessing will use, idealy it well recieve a filename
    or a data that is unchanged during the fit process

    ==================================================
    what we need
    - time / s, height / microM, force / microN
    - parameters
    if the data still needs to be adjusted (fit range,
    data reducution, shift to contact point, height change)
    then you need:
    - contact point
    - s0_velocity or indentation velocity
    - and if the curves have a dwell or not ( as a boolean)
    - for data reduction or fit range please see the functions provided for ideas
    (apply_fit_range(), data_reduction, piezo())
    '''
    # the following need to be global for process
    global time, angle, R1, theta, h, Vkk, A0  # global variables

    # ===================== Parameters ====================================
    # here the parametrs are loaded either from file or set for fit

    angle = 35  # angle cell before indentation
    R1 = 12 # radius cell?
    theta_cone = 17.5 # opening angle for Indenter
    Phio = angle * PI / 180 # contact angle in radians
    theta = (PI/2) - theta_cone * PI / 180 # correction Discher paper
    Ro = R1 / sin(Phio)
    h = Ro - sqrt(Ro ** 2 - R1 ** 2) # height of sphere cap
    Vkk = (1/3) * h ** 2 * PI * (3 * Ro - h) # volume of sphere cap
    A0 = 2 * PI * Ro * h # surface area of sphere cap
    r1 = 0.0001 # start parameter radius in micro meter
    phi = Phio # start parameter 2

    # once all data and parameters are loaded the fit can be run
    parameter = run_fit(height,force)

    # plot the fit with data and save it, please see the function before using
    fit_force = plot_fit(time,
                         height,
                         force,
                         parameter,
                         fname)

    # here the data is saved
    #save_fit(fname,parameter,time,height,force,fit_force,data_reduction_factor,value)

# ============================= some settings ===============================!
# Dwell or no dwell
global dwell
dwell = False  # if the curves have a dwell or not

# for testing set to false
run_multi_processing = False

# percentage of the maximal force kept for the fitting
global value
value = 0.3

# number of cores used
cores = 3
#============================run part here ==============================
paths = []
paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110310/map-data-2011.03.10-11.16.05/eval_2020_02_04_20_00')
paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110310/map-data-2011.03.10-12.56.03/eval_2020_03_23_11_23')
paths.append('../../../../ownCloud/texts/stretcher_paper/eval/mbcd_anna_stuff/110310/map-data-2011.03.10-14.32.08/eval_2020_03_26_14_30')

# iterate through paths
if run_multi_processing:
    for path in paths:
        p = Pool(cores)
        # returns file names of junk data
        stuff = list(tqdm(p.imap(main_process,fnames),total = len(fnames)))
        p.close()
        # waits for everything to finish
        p.join()

        # fit output saved as json
        file_name = path + '/fit_output.txt'
        stuff = list(filter(None, stuff))
        output = {}
        output['fit return'] = stuff

        with open(file_name, 'w+') as outfile:
            json.dump(output, outfile)

else:
    # to test stuff out
    # timers start
    t0 = real_time.time()
    # timers end
    t1 = real_time.time()
    # timer output
    print(f'Time spent: {(t1-t0) / 60} minutes')
